<div class="breadcrumb">
	<a href="">Home</a> 
	<a href="">Profile</a>
</div>
<div class="content">
	<div class="panel">
		<div class="content-header no-mg-top">
			<i class="fa fa-newspaper-o"></i>
			<div class="content-header-title">Profile</div>
		</div>

        <?php if ($this->session->flashdata('success_message')) { ?>
            <div class="alert alert-success alert-dismissable margintopbtm20">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong> </strong><?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php } ?>









		<div class="row">
			<div class="col-md-12">
				<div class="content-box">
					<form id="form-validate" method="post" action="<?php echo base_url('profile/save') ?>" class="padding3 paddingtop30" enctype="multipart/form-data">
                        <input type="text" name="id" class="hidden" value="<?php echo $active_user->id; ?>">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Name *</label>
                                    <input class="form-control" name="name" data-error="Please input your First Name"
                                           placeholder="First Name"
                                           required="required" type="text"  value="<?php echo $active_user->name; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address *</label>
                                    <input class="form-control" name="email" data-error="Your email address is invalid"
                                           placeholder="Enter email"
                                           required="required" readonly type="email" data-remote="/auth/emailCheck"  value="<?php echo $active_user->email; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail *</label>
                                    <input class="form-control" name="phone" id="phone" data-error="Please input your contact number"
                                           placeholder="Contact Number (i.e 971 XXXXXXXXXX) "
                                           required="required" type="text"  value="<?php echo $active_user->phone; ?> ">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name *</label>
                                    <input class="form-control" name="company_name" data-error="Please input your Company Name"
                                           placeholder="Company Name"
                                           required="required" type="text"  value="<?php echo $active_user->company_name; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Designation *</label>
                                    <input class="form-control" name="designation" data-error="Please input your Designation" placeholder="Designation"
                                           required="required" type="text" value="<?php echo $active_user->designation; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select name="gender" class="form-control" required>

                                        <option <?php if($active_user->gender == 'Male') { echo 'selected';} ?> value="Male">Male</option>
                                        <option <?php if($active_user->gender == 'Female') { echo 'selected';} ?> value="Female">Female</option>
                                    </select>
                                    <div class="validation-message" data-field="group"></div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Profile Image</label>

                                    <?php if ($active_user->photo != '') { ?>

                                        <img src="<?php echo site_url('uploads/'. $active_user->photo); ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control" value="<?php echo $active_user->photo; ?>" type="hidden" name="photo">

                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Upload Photo ID</label>

                                    <?php if ($active_user->image_id != '') { ?>

                                        <img src="<?php echo site_url('uploads/'. $active_user->image_id) ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control" value="<?php echo $active_user->image_id; ?>" type="hidden" name="image_id">

                                    <?php }  ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">


                                    <button type="button" id="profile_image"
                                            class="btn btn-primary orangebackground"><i
                                                class="fa fa-upload"></i>
                                        <span class="hidden-sm-down"> Upload Photo Update</span>
                                        <input type="file" name="photo" />
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <button type="button" id="profile_image"
                                            class="btn btn-primary orangebackground"><i
                                                class="fa fa-upload"></i>
                                        <span class="hidden-sm-down"> Upoad Photo ID Update</span>
                                        <input type="file" name="image_id"  />
                                    </button>
                                    <div class="validation-message" data-field="images"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>ID Expiry Date</label>
                                    <input class="form-control single-daterange" name="id_expiry_date" data-error="Please input your Designation" placeholder="ID Expiry Date"
                                           required="required" type="text" value="<?php echo date('d/m/Y',strtotime($active_user->id_expiry_date)); ?>" >
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box-footer">
                            <button type="submit" class="btn btn-info orangebackground whitefont">Update</button>
                        </div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>

