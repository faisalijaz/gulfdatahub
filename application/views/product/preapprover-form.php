<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Pre Approver Request Form</a>
</div>
<div class="content">


    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">

                    <form id="form-validate" method="post" action="" class="padding3 paddingtop30" enctype="multipart/form-data">
                        <div class="form-section-head">
                            <span class="pull-left">DSODC1-DAR-FRM-001</span>

                            <span>Access Request Form</span>
                        </div>


                        <input class="form-control" data-error="Please input your First Name"
                               placeholder="DSODC1-DAR-FRM-001" value="DSODC1-DAR-FRM-001" type="hidden"
                               name="refrence_number">

                        <!--                        requester information-->

                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" data-error="Your email address is invalid" value="<?php echo $this->session->userdata['active_user']->name; ?>"
                                   placeholder="ABC User" required="required" type="text" name="name">
                            <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" data-error="Your email address is invalid"  value="<?php echo $this->session->userdata['active_user']->email; ?>"
                                           placeholder="abc@gmail.com" required="required" type="email"  name="email" readonly>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" placeholder="009711234567" required="required"
                                           type="text" name="phone">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Designation</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text" value="<?php echo $this->session->userdata['active_user']->designation; ?>"
                                           name="designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                           type="text" name="company"  value="<?php echo $this->session->userdata['active_user']->company_name; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>



                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Access Start Date</label>
                                                            <input class=" form-control start_date" placeholder="Date of birth"
                                                                   type="text" name="start_date" value="">
                                                        </div>
                                                    </div>



                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Access End Date</label>
                                                            <input class=" form-control end_date" placeholder="Date of birth"
                                                                   type="text" name="end_date" value="">
                                                        </div>
                                                    </div>

                                              </div>
                        <div class="row">
                            <div class="col col-sm-6">
                                <div class="form-group">
                                    <label for=""> Profile Image</label>

                                    <?php if ($this->session->userdata['active_user']->photo != '') { ?>

                                        <img src="<?php echo site_url('uploads/' . $this->session->userdata['active_user']->photo); ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control"
                                               value="<?php echo $this->session->userdata['active_user']->photo; ?>"
                                               type="hidden" name="profile_image">

                                    <?php } else { ?>
                                        <button type="button"  id="profile_image"
                                                class="btn btn-primary orangebackground"><i
                                                    class="fa fa-upload"></i>
                                            <span class="hidden-sm-down">Upoad Photo</span>
                                            <input type="file" name="profile_image"/>
                                        </button>




                                        <div class="validation-message" data-field="profile_image"></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Upload Photo ID</label>

                                    <?php if ($this->session->userdata['active_user']->image_id != '') { ?>

                                        <img src="<?php echo site_url('uploads/' . $this->session->userdata['active_user']->image_id) ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control"
                                               value="<?php echo $this->session->userdata['active_user']->image_id ; ?>"
                                               type="hidden" name="manager_id_image">

                                    <?php } else { ?>

                                        <button type="button" class="btn btn-primary  orangebackground">
                                            <i class="fa fa-upload"> </i>
                                            <span class="hidden-sm-down">Upload Photo ID</span>
                                            <input type="file" name="manager_id_image"/>
                                        </button>


                                        <div class="validation-message" data-field="profile_id"></div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label>Card Expiry Date</label>
                                    <input class="single-daterange form-control"  placeholder="Date of birth"
                                           type="text" name="card_expiry"  value="<?php echo date('d/m/Y',strtotime($this->session->userdata['active_user']->id_expiry_date)); ?>">
                                </div>
                            </div>
                        </div>


                        <!--                        Access Type-->


                        <div class="form-section bluebackground">
                            <span>Type of Access</span>
                        </div>
                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Pre Approved User <input type="checkbox"
                                                                                         name="access_type" value="preapprover"
                                                                                         class="check_access_type preapprover" checked required></label>
                                </div>
                            </div>



                        </div>
                        <!--                        Business-->


                        <!--                        zone-->
                        <div class="zone_div">
                            <div class="form-section">
                                <span>Zone Access Required</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Data Hall </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 1 <input type="checkbox" name="hall_1"
                                                                             value="hall_1"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 2 <input type="checkbox" name="hall_2"
                                                                             value="hall_2"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 3 <input type="checkbox" name="hall_3"
                                                                             value="hall_3"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Office Area <input type="checkbox"
                                                                                       name="office_area"
                                                                                       value="office_area"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> POE Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du1 <input type="checkbox" name="poe_room_du1"
                                                                               value="poe_room_du1"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du2 <input type="checkbox" name="poe_room_du2"
                                                                               value="poe_room_du2"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET1 <input type="checkbox" name="poe_room_et1"
                                                                               value="poe_room_et1"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET2 <input type="checkbox" name="poe_room_et2"
                                                                               value="poe_room_et2"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Receiving Area <input type="checkbox"
                                                                                          name="receiving_area"
                                                                                          value="receiving_area"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="usp_room_a"
                                                                             value="usp_room_a"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="usp_room_b"
                                                                             value="usp_room_b"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="usp_room_c"
                                                                             value="usp_room_c"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="usp_room_d"
                                                                             value="usp_room_d"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Storage Area <input type="checkbox"
                                                                                        name="storage_area"
                                                                                        value="storage_area"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Battery Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="battery_room_a"
                                                                             value="battery_room_a"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="battery_room_b"
                                                                             value="battery_room_b"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="battery_room_c"
                                                                             value="battery_room_c"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="battery_room_d"
                                                                             value="battery_room_d"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> LAB Area <input type="checkbox" name="lab_area"
                                                                                    value="lab_area"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Output Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="ups_output_room_a"
                                                                             value="ups_output_room_a"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="ups_output_room_b"
                                                                             value="ups_output_room_b"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="ups_output_room_c"
                                                                             value="ups_output_room_c"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="ups_output_room_d"
                                                                             value="ups_output_room_d"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Control Room <input type="checkbox"
                                                                                        name="control_room"
                                                                                        value="control_room"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Main Switch Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="main_switch_room_a"
                                                                             value="main_switch_room_a"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="main_switch_room_b"
                                                                             value="main_switch_room_b"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="main_switch_room_c"
                                                                             value="main_switch_room_c"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="main_switch_room_d"
                                                                             value="main_switch_room_d"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>


                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Preaction Room <input type="checkbox"
                                                                                          name="preaction_room"
                                                                                          value="preaction_room"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Main Tank Area <input type="checkbox"
                                                                                               name="fuel_main_tank_area"
                                                                                               value="fuel_main_tank_area">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Pump Room <input type="checkbox"
                                                                                          name="fuel_pump_room"
                                                                                          value="fuel_pump_room"></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> AHU Room <input type="checkbox" name="ahu_room"
                                                                                    value="ahu_room"></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Water Pump Room <input type="checkbox"
                                                                                                   name="chilled_water_pump_room"
                                                                                                   value="chilled_water_pump_room">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Area Backyard <input type="checkbox"
                                                                                                 name="chilled_area_backyard"
                                                                                                 value="chilled_area_backyard"></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> External Area <input type="checkbox"
                                                                                         name="external_area"
                                                                                         value="external_area"></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Generator Room <input type="checkbox"
                                                                                          name="generator_room"
                                                                                          value="generator_room">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> First Floor Corridor <input type="checkbox"
                                                                                                name="first_floor_corridor"
                                                                                                value="first_floor_corridor"></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Ground Floor Corridor <input type="checkbox"
                                                                                                 name="ground_floor_corridor"
                                                                                                 value="ground_floor_corridor"></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                        </div>


                        <!--                        preapprove section-->
                        <div class="preapprove_section">
                            <div class="form-section">
                                <span>Main Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Name" required="required" type="text" value=""
                                               name="pre_manager_name">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Title" required="required"
                                               type="text" name="pre_manager_title">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Contact Number" required="required"
                                               type="text" name="pre_manager_phone">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Email" required="required" type="email"
                                               name="pre_manager_email">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for=""> Upload Photo ID</label>
                                        <button type="button"  class="btn btn-primary  orangebackground">
                                            <i class="fa fa-upload"> </i>
                                            <input type="file" name="pre_manager_id_image"  required/>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Card Expiry Date</label>
                                        <input class="single-daterange form-control" placeholder="Card Expiry Date"
                                               type="text" name="pre_manager_card_expiry" value="04-12-2017" required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <input class="form-control" placeholder="Signature" required="required"
                                               type="text" name="pre_manager_signature">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-section">
                                <span>Backup Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Please Enter Contact Person name"
                                               placeholder="Name" required="required" type="text"
                                               name="contact_person_name">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Title" required="required"
                                               type="text" name="contact_person_title">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Contact Number" required="required"
                                               type="text" name="contact_person_number">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Email" required="required" type="email"
                                               name="contact_person_email">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for=""> Upload Photo ID</label>
                                        <button type="button"  class="btn btn-primary  orangebackground">
                                             <i class="fa fa-upload"> </i>

                                            <input type="file" name="contact_person_image"  required/>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>ID Expiry Date</label>
                                        <input class="single-daterange form-control" placeholder="Card Expiry Date"
                                               type="text" name="contact_person_card_expiry" value="" required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <input class="form-control" placeholder="Signature" required="required"
                                               type="text" name="contact_person_signature">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-section">
                                <span>Pre Approved Personnel</span>
                            </div>
                            <div class="personnels"></div>


                            <div class="row materialitem">
                                <button class="btn btn-success" id="addPersonnel" type="button"><i
                                            class="fa fa-plus"></i> Add Personnel's</button>
                            </div>
                        </div>


                        <div class="content-box-footer">
                            <button class="btn btn-primary bluebackground">Save</button>
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back
                                To
                                Requests</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function() {
            $('.start_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "timePicker": true,
                    "timePicker24Hour": true,

                locale: {
                    format: 'MM/DD/YYYY hh:mm'
                }
                }
        );

            $('.end_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "timePicker": true,
                "timePicker24Hour": true,
                locale: {
                    format: 'MM/DD/YYYY hh:mm'
                }
            }
        );
        });

        //material check boxes
        $('.business_requirements').click(function () {
            $('.business_requirements').not(this).prop('checked', false);
        });



        // Input radio-group visual controls
        $('.radio-group label').on('click', function () {
            $(this).removeClass('not-active').siblings().addClass('not-active');
        });



        $(".preapprover").change(function () {
            if (this.checked) {
                $('.requester_acknowledge').hide(500);
                $('.business_div').hide(500);
                $('.preapprove_section').show(500);

            } else {
                $('.requester_acknowledge').show(500);
                $('.business_div').show(500);
                $('.preapprove_section').hide(500);

            }
        });

        // Mariral addmore







        $(function () {

            //workers div
            var perkDiv = $('.personnels');
            var html_personnel = '';
            html_personnel += '<div class="row addPersonnel materialitems">';
            html_personnel += '<div class="col-sm-4">';

            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Name</label>';
            html_personnel += '<input class="form-control" name="personnel_name[]" data-error="Your email address is invalid"  required="required" type="text" >';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Title</label>';
            html_personnel += '<input class="form-control"  required="required" type="text" name="personnel_title[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += ' </div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Contact Number</label>';
            html_personnel += '<input class="form-control" required="required" type="text" name="personnel_phone[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Email</label>';
            html_personnel += '<input class="form-control" data-error="Your email address is invalid"  required="required" type="text" name="personnel_email[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label for=""> Upload Photo ID</label>';
            html_personnel += '<button type="button"  class="btn btn-primary  orangebackground">';
            html_personnel += '<i class="fa fa-upload"> </i>';
            html_personnel += '<input type="file" name="personnel_image[]"  required/>';
            html_personnel += '</button>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>ID expiry date</label>';
            html_personnel += '<input class="form-control single-daterange "  required="required" type="text" name="personnel_id_expiry[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';

            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Signature</label>';
            html_personnel += '<div class="input-group">';
            html_personnel += '<input class="form-control" placeholder="Signature" required="required" type="text" name="personnel_signature[]">';
            html_personnel += '<div class="input-group-btn">';
            html_personnel += '<button class="btn btn-danger remPersonnel" id="remPersonnel" type="button"><i class="fa fa-times"></i></button>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '</div>';

            $('#addPersonnel').on('click', function () {
                $(html_personnel).insertBefore(".materialitem");

                // Date picker
                if ($('input.single-daterange').length) {
                    $('input.single-daterange').daterangepicker({ "singleDatePicker": true,
                        locale: {
                            format: 'DD/MM/YYYY'
                        }
                    })

                }

                $('.remPersonnel').on('click', function () {
                    $(this).parents('.addPersonnel').remove();
                    return false;
                });

                return false;
            });

        });


    </script>