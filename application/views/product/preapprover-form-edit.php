<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Pre Approver Request Form</a>
</div>
<div class="content">


    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>

        </div>
        <div class="row" id="printable">
            <div class="col-md-12">
                <div class="content-box">
                    <?php if(isset($request['id']) && $request['id'] <> null) { ?>

                        <input type="button" class="btn btn-info orangebackground pull-right" value="Print Form"
                               id="btnPrintmainForm"/>
                    <?php } ?>

                    <form id="form-validate" method="post" action="<?php base_url('profile/save') ?>" class="padding3 paddingtop30"
                          enctype="multipart/form-data">
                        <input class="form-control" value="<?php echo $request['id']; ?>" type="hidden" name="id">
                        <div class="form-section-head">
                            <span class="pull-left">DSODC1-DAR-FRM-001</span>
                            <span>Pre-Approve Request Form</span>
                        </div>
                        <input class="form-control" data-error="Please input your First Name"
                               placeholder="DSODC1-DAR-FRM-001" value="<?php echo $request['refrence_number']; ?>" type="hidden"
                               name="refrence_number">
                        <!--                        requester information-->
                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" data-error="Your email address is invalid"
                                   value="<?php echo $request['name']; ?>"
                                   placeholder="ABC User" required="required" type="text" name="name">
                            <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           value="<?php echo $request['email']; ?>"
                                           placeholder="abc@gmail.com" required="required" type="email" name="email"
                                           readonly>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" placeholder="009711234567" required="required" value="<?php echo $request['phone']; ?>"
                                           type="text" name="phone">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Designation</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           value="<?php  echo $request['designation']; ?>"
                                           name="designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                           type="text" name="company"
                                           value="<?php  echo $request['company']; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access Start Date</label>
                                    <input class="form-control start_date" placeholder="Access Start Date"
                                           type="text" name="start_date" value="<?php  echo date('m/d/Y h:i',strtotime($request['start_date'])); ?>">
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access End Date</label>
                                    <input class=" form-control end_date" placeholder="Access End Date"
                                           type="text" name="end_date" value="<?php  echo  date('m/d/Y h:i',strtotime($request['end_date'])); ?>">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Profile Image</label>

                                    <?php if ($request['profile_image'] != '') { ?>

                                        <img src="<?php echo site_url('uploads/'. $request['profile_image']); ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control" value="<?php echo $request['profile_image']; ?>" type="hidden" name="profile_image">

                                    <?php } else { ?>
                                        <button type="button" id="profile_image"
                                                class="btn btn-primary orangebackground"><i
                                                    class="fa fa-upload"></i>
                                            <span class="hidden-sm-down"> Upload Photo</span>
                                            <input type="file" name="profile_image"/>
                                        </button>



                                        <div class="validation-message" data-field="profile_image"></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Upload Photo ID</label>

                                    <?php if ($request['manager_id_image'] != '') { ?>

                                        <img src="<?php echo site_url('uploads/'. $request['manager_id_image']) ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control" value="<?php echo $request['manager_id_image']; ?>" type="hidden" name="manager_id_image">

                                    <?php } else { ?>

                                        <button type="button" class="btn btn-primary  orangebackground">
                                            <i class="fa fa-upload"> </i>
                                            <span class="hidden-sm-down">Upoad Photo ID</span>
                                            <input type="file" name="manager_id_image"/>
                                        </button>



                                        <div class="validation-message" data-field="profile_id"></div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label>ID Expiry Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="card_expiry"
                                           value="<?php echo date('d/m/Y', strtotime($request['card_expiry'])); ?>">
                                </div>
                            </div>
                        </div>


                        <!--                        Access Type-->


                        <div class="form-section bluebackground">
                            <span>Type of Access</span>
                        </div>
                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Pre Approved User <input type="checkbox"
                                                                                         name="access_type"
                                                                                         value="preapprover"
                                                                                         class="check_access_type preapprover"
                                                                                         checked required readonly></label>
                                </div>
                            </div>


                        </div>
                        <!--                        Business-->


                        <!--                        zone-->
                        <div class="zone_div">
                            <div class="form-section">
                                <span>Zone Access Required</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Data Hall </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 1 <input type="checkbox" name="hall_1"
                                                                             value="hall_1" <?php if(isset($request['hall_1']) && ($request['hall_1'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 2 <input type="checkbox" name="hall_2"
                                                                             value="hall_2"  <?php if(isset($request['hall_2']) && ($request['hall_2'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 3 <input type="checkbox" name="hall_3"
                                                                             value="hall_3" <?php if(isset($request['hall_3']) && ($request['hall_3'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Office Area <input type="checkbox"
                                                                                       name="office_area"
                                                                                       value="office_area"  <?php if(isset($request['office_area']) && ($request['office_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> POE Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du1 <input type="checkbox" name="poe_room_du1"
                                                                               value="poe_room_du1" <?php if(isset($request['poe_room_du1']) && ($request['poe_room_du1'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du2 <input type="checkbox" name="poe_room_du2"
                                                                               value="poe_room_du2" <?php if(isset($request['poe_room_du2']) && ($request['poe_room_du2'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET1 <input type="checkbox" name="poe_room_et1"
                                                                               value="poe_room_et1" <?php if(isset($request['poe_room_et1']) && ($request['poe_room_et1'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET2 <input type="checkbox" name="poe_room_et2"
                                                                               value="poe_room_et2" <?php if(isset($request['poe_room_et2']) && ($request['poe_room_et2'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Receiving Area <input type="checkbox"
                                                                                          name="receiving_area"
                                                                                          value="receiving_area" <?php if(isset($request['receiving_area']) && ($request['receiving_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="usp_room_a"
                                                                             value="usp_room_a" <?php if(isset($request['usp_room_a']) && ($request['usp_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="usp_room_b"
                                                                             value="usp_room_b" <?php if(isset($request['usp_room_b']) && ($request['usp_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="usp_room_c"
                                                                             value="usp_room_c" <?php if(isset($request['usp_room_c']) && ($request['usp_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="usp_room_d"
                                                                             value="usp_room_d" <?php if(isset($request['usp_room_d']) && ($request['usp_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Storage Area <input type="checkbox"
                                                                                        name="storage_area"
                                                                                        value="storage_area" <?php if(isset($request['storage_area']) && ($request['storage_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Battery Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="battery_room_a"
                                                                             value="battery_room_a" <?php if(isset($request['battery_room_a']) && ($request['battery_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="battery_room_b"
                                                                             value="battery_room_b"  <?php if(isset($request['battery_room_b']) && ($request['battery_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="battery_room_c"
                                                                             value="battery_room_c" <?php if(isset($request['battery_room_c']) && ($request['battery_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="battery_room_d"
                                                                             value="battery_room_d"  <?php if(isset($request['battery_room_d']) && ($request['battery_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> LAB Area <input type="checkbox" name="lab_area"
                                                                                    value="lab_area" <?php if(isset($request['lab_area']) && ($request['lab_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Output Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="ups_output_room_a"
                                                                             value="ups_output_room_a"  <?php if(isset($request['ups_output_room_a']) && ($request['ups_output_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="ups_output_room_b"
                                                                             value="ups_output_room_b" <?php if(isset($request['ups_output_room_b']) && ($request['ups_output_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="ups_output_room_c"
                                                                             value="ups_output_room_c" <?php if(isset($request['ups_output_room_c']) && ($request['ups_output_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="ups_output_room_d"
                                                                             value="ups_output_room_d" <?php if(isset($request['ups_output_room_d']) && ($request['ups_output_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Control Room <input type="checkbox"
                                                                                        name="control_room"
                                                                                        value="control_room" <?php if(isset($request['control_room']) && ($request['control_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Main Switch Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="main_switch_room_a"
                                                                             value="main_switch_room_a" <?php if(isset($request['main_switch_room_a']) && ($request['main_switch_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="main_switch_room_b"
                                                                             value="main_switch_room_b" <?php if(isset($request['main_switch_room_b']) && ($request['main_switch_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="main_switch_room_c"
                                                                             value="main_switch_room_c" <?php if(isset($request['main_switch_room_c']) && ($request['main_switch_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="main_switch_room_d"
                                                                             value="main_switch_room_d" <?php if(isset($request['main_switch_room_d']) && ($request['main_switch_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>


                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Preaction Room <input type="checkbox"
                                                                                          name="preaction_room"
                                                                                          value="preaction_room"  <?php if(isset($request['preaction_room']) && ($request['preaction_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Main Tank Area <input type="checkbox"
                                                                                               name="fuel_main_tank_area"
                                                                                               value="fuel_main_tank_area" <?php if(isset($request['fuel_main_tank_area']) && ($request['fuel_main_tank_area'] !='')) { echo 'checked'; } ?>>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Pump Room <input type="checkbox"
                                                                                          name="fuel_pump_room"
                                                                                          value="fuel_pump_room" <?php if(isset($request['fuel_pump_room']) && ($request['fuel_pump_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> AHU Room <input type="checkbox" name="ahu_room"
                                                                                    value="ahu_room" <?php if(isset($request['ahu_room']) && ($request['ahu_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Water Pump Room <input type="checkbox"
                                                                                                   name="chilled_water_pump_room"
                                                                                                   value="chilled_water_pump_room" <?php if(isset($request['chilled_water_pump_room']) && ($request['chilled_water_pump_room'] !='')) { echo 'checked'; } ?>>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Area Backyard <input type="checkbox"
                                                                                                 name="chilled_area_backyard"
                                                                                                 value="chilled_area_backyard" <?php if(isset($request['chilled_area_backyard']) && ($request['chilled_area_backyard'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> External Area <input type="checkbox"
                                                                                         name="external_area"
                                                                                         value="external_area" <?php if(isset($request['external_area']) && ($request['external_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Generator Room <input type="checkbox"
                                                                                          name="generator_room"
                                                                                          value="generator_room" <?php if(isset($request['generator_room']) && ($request['generator_room'] !='')) { echo 'checked'; } ?>>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> First Floor Corridor <input type="checkbox"
                                                                                                name="first_floor_corridor"
                                                                                                value="first_floor_corridor" <?php if(isset($request['first_floor_corridor']) && ($request['first_floor_corridor'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Ground Floor Corridor <input type="checkbox"
                                                                                                 name="ground_floor_corridor"
                                                                                                 value="ground_floor_corridor" <?php if(isset($request['ground_floor_corridor']) && ($request['ground_floor_corridor'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                        </div>


                        <!--                        preapprove section-->
                        <div class="preapprove_section">
                            <div class="form-section">
                                <span>Main Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text" value="<?php  echo $request['pre_manager_name']; ?>"
                                               name="pre_manager_name">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="pre_manager_title" value="<?php  echo $request['pre_manager_title']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="0971 0000000" required="required"
                                               type="text" name="pre_manager_phone" value="<?php  echo $request['pre_manager_phone']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="pre_manager_email" value="<?php  echo $request['pre_manager_email']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for=""> Upload Photo ID</label>
                                        <button type="button" class="btn btn-primary  orangebackground">
                                            <i class="fa fa-upload"> </i>
                                            <input type="file" name="pre_manager_id_image"  />
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Card Expiry Date</label>
                                        <input class="single-daterange form-control" placeholder="Date of birth"
                                               type="text" name="pre_manager_card_expiry" value="<?php  echo $request['pre_manager_card_expiry']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <input class="form-control" placeholder="Signature" required="required"
                                               type="text" name="pre_manager_signature" value="<?php  echo $request['pre_manager_signature']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Uploaded ID</label>
                                        <img src="<?php echo site_url('uploads/'. $request['pre_manager_id_image']) ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-section">
                                <span>Backup Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Please Enter Contact Person name"
                                               placeholder="Contact Person" required="required" type="text"
                                               name="contact_person_name" value="<?php  echo $request['contact_person_name']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Title" required="required"
                                               type="text" name="contact_person_title"  value="<?php  echo $request['contact_person_title']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Contact Number" required="required"
                                               type="text" name="contact_person_number"  value="<?php  echo $request['contact_person_number']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="contact_person_email"  value="<?php  echo $request['contact_person_email']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for=""> Upload Photo ID</label>
                                        <button type="button" class="btn btn-primary  orangebackground">
                                            <i class="fa fa-upload"> </i>

                                            <input type="file" name="contact_person_image" />
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Card Expiry Date</label>
                                        <input class="single-daterange form-control" placeholder="Date of birth"
                                               type="text" name="contact_person_card_expiry" value="<?php  echo $request['contact_person_card_expiry']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <input class="form-control" placeholder="Signature" required="required"
                                               type="text" name="contact_person_signature"  value="<?php  echo $request['contact_person_signature']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Uploaded ID</label>
                                        <img src="<?php echo site_url('uploads/'. $request['contact_person_image']) ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-section">
                                <span>Pre Approved Personnel</span>
                            </div>
                            <div class="personnels"></div>

                            <?php if(!empty($personnels)){

                            /*    echo "<pre>";
                                print_r($personnels);
                                exit('sdad');*/



                                ?>

                                <?php foreach($personnels as $key=>$data){  ?>


                                <div class="row newmaterial addPersonnel">
                                    <div class="col-sm-4">

                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" data-error="Required"
                                                   placeholder="Name" required="required" type="text"
                                                   name="personnel_name[]"  value="<?php  echo $data['personnel_name']; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input class="form-control" placeholder="Title" required="required"
                                                   type="text" name="personnel_title[]"  value="<?php  echo $data['personnel_title']; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Number</label>
                                            <input class="form-control" placeholder="Contact Number" required="required"
                                                   type="text" name="personnel_phone[]" value="<?php  echo $data['personnel_phone']; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="Email" required="required" type="text"
                                                   name="personnel_email[]" value="<?php  echo $data['personnel_email']; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <input class=" "
                                           placeholder="Date" required="required" type="hidden"
                                           name="personnel_image[]"  value="<?php  echo $data['personnel_image']; ?>">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>ID expiry date</label>
                                            <input class="form-control single-daterange "
                                                   placeholder="Date" required="required" type="text"
                                                   name="personnel_id_expiry[]"  value="<?php  echo $data['personnel_id_expiry']; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Signature</label>
                                            <div class="input-group">
                                                <input class="form-control" placeholder="Signature" required="required"
                                                       type="text" name="personnel_signature[]" value="<?php  echo $data['personnel_signature']; ?>">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger remPersonnel" id="remPersonnel" type="button"><i
                                                                class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Uploaded ID</label>
                                            <div class="input-group">
                                                <img src="<?php echo site_url('uploads/'. $data['personnel_image']) ?>"
                                                     class="img-responsive formimage" alt="Cinque Terre">
                                            </div>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            } ?>
                            <div class="clearfix"></div>

                            <div class="row materialitem maindiv">
                                <button class="btn btn-success" id="addPersonnel" type="button"><i
                                            class="fa fa-plus"></i> Add Personnel's</button>

                            </div>
                        </div>



                        <?php if($this->session->userdata['active_user']->group_id != 3){ ?>

                            <div class="form-section">
                                <span> Manager / Pre Approved Managers Details</span>
                            </div>

                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" data-error="Your email address is invalid"
                                       placeholder="ABC User" name="manager_name" required="required" type="text" value="<?php  echo $this->session->userdata['active_user']->name; ?>">
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="abc@gmail.com" name="manager_email" required="required" readonly type="email" value="<?php  echo $this->session->userdata['active_user']->email; ?>" >
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Contact Detail</label>
                                        <input class="form-control"  name="manager_phone" placeholder="009711234567"  value="<?php  echo $request['manager_phone']; ?>" required="required"
                                               type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Designation</label>
                                        <input class="form-control" name="manager_designation" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text" value="<?php  echo $request['manager_designation']; ?>"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input class="form-control" name="manager_company" placeholder="Wisdom IT Solution" required="required" type="text" name="requester_company" value="<?php  echo $request['manager_company']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <div class="input-group">
                                            <div class="btn-group radio-group mangerradio">
                                                <label class="btn btn-primary <?php if($request['manager_signature']=="Reject") {echo 'not-active';}else if($request['team_signature']=="") {echo 'not-active';}  ?> ">Approve <input type="radio" value="Approved" name="manager_signature" <?php if($request['manager_signature']=="Approved") {echo 'checked';} ?> required></label>
                                                <label class="btn btn-primary <?php if($request['manager_signature']=="Approved") {echo 'not-active';} else if($request['team_signature']=="") {echo 'not-active';} ?>">Reject <input type="radio" value="Reject" name="manager_signature" <?php if($request['manager_signature']=="Reject") {echo 'checked';} ?> required></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class=" form-control" name="manager_signature_date" placeholder=""
                                               type="text"  value="<?php if( $request['manager_signature_date']!=""){echo $request['manager_signature_date'];}else{ echo date('d/m/Y h:i');} ?>" readonly >
                                    </div>
                                </div>
                                <div class="col-sm-6 <?php if($request['manager_signature']=="Reject") {}else{ echo 'hidden';} ?>" id="manager_reject_reason">
                                    <div class="form-group">
                                        <label>Reason</label>
                                        <input class=" form-control" name="manager_reject_reason" placeholder=""
                                               type="text"  value="<?php if(isset($request['manager_reject_reason'])){ echo $request['manager_reject_reason'];} ?>" >
                                    </div>
                                </div>


                            </div>



                        <?php if($this->session->userdata['active_user']->group_id == 1){ ?>

                            <div class="form-section">
                                <span> For Data Centre Operations Team</span>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label >Access Number</label>
                                        <input class="form-control" name="access_number" data-error="Your Access Number"
                                               placeholder="AERT-711-34567" required="required" value="AERT-711-34567" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row paddingtop8">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Esscort Service </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label ">Optional <input type="checkbox" name="esscort_service" value="optional" <?php if($request['team_signature']=="optional") {echo 'checked';} ?> ></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Mandatory <input type="checkbox"  name="esscort_service" value="mandatory"  <?php if($request['esscort_service']=="mandatory") {echo 'checked';} ?> ></label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                            <div class="row paddingtop8">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Approver Type</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label ">Visitor <input type="checkbox" name="appprover_type" value="visitor"  <?php if($request['appprover_type']=="visitor") {echo 'checked';} ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Contracter <input type="checkbox"  name="appprover_type" value="contracter" <?php if($request['appprover_type']=="contracter") {echo 'checked';} ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Customer <input type="checkbox"  name="appprover_type" value="customer" <?php if($request['appprover_type']=="customer") {echo 'checked';} ?>></label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>




                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label >Signature</label>
                                        <div class="input-group">
                                            <div class="btn-group radio-group teamradio">

                                                <label class="btn btn-primary  <?php if($request['team_signature']=="Reject") {echo 'not-active';}else if($request['team_signature']=="") {echo 'not-active';} ?> ">Approve <input type="radio" value="Approved" name="team_signature" <?php if($request['team_signature']=="Approved") {echo 'checked';} ?> required></label>
                                                <label class="btn btn-primary  <?php if($request['team_signature']=="Approved") {echo 'not-active';}else if($request['team_signature']=="") {echo 'not-active';}  ?>">Reject <input type="radio" value="Reject" name="team_signature" <?php if($request['team_signature']=="Reject") {echo 'checked';} ?> required></label>

                                            </div>
                                        </div>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control" placeholder="Date of birth" name="team_signature_date"
                                               type="text" value="<?php if( $request['team_signature_date']!=""){echo $request['team_signature_date'];}else{ echo date('d/m/Y h:i');} ?>" readonly ">
                                    </div>
                                </div>
                                <div class="col-sm-6 <?php if($request['team_signature']=="Reject") {}else{ echo 'hidden';} ?>" id="datateam_reject_reason">
                                    <div class="form-group">
                                        <label>Reason</label>
                                        <input class=" form-control" name="datateam_reject_reason" placeholder=""
                                               type="text"  value="<?php if(isset($request['datateam_reject_reason'])){ echo $request['datateam_reject_reason'];} ?>" >
                                    </div>
                                </div>


                            </div>
                            <?php } ?>

                        <?php } ?>


                        <div class="content-box-footer">
                            <button class="btn btn-primary bluebackground">Save</button>
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back
                                To
                                Requests</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {

            $("#btnPrintmainForm").on('click', function () {
                $.print("#printable");
            });

            $('.start_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "timePicker": true,
                    "timePicker24Hour": true,
                locale: {
                    format: 'MM/DD/YYYY hh:mm'
                }


                }
            );

            $('.end_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "timePicker": true,
                    "timePicker24Hour": true,
                    locale: {
                        format: 'MM/DD/YYYY hh:mm'
                    }
                }
            );
        });

        //material check boxes
        $('.business_requirements').click(function () {
            $('.business_requirements').not(this).prop('checked', false);
        });

        $('.business_requirements').click(function () {
            $('.business_requirements').not(this).prop('checked', false);
        });


        // Input radio-group visual controls
        $('.mangerradio label').on('click', function () {

            $(this).removeClass('not-active').siblings().addClass('not-active');

        });
        $('.radio-group label').on('click', function () {

            $(this).removeClass('not-active').siblings().addClass('not-active');

        });

        $('input[type=radio][name=manager_signature]').change(function() {

            if($(this).val() =='Reject'){
                $('#manager_reject_reason').show(500);
            }else{
                $('#manager_reject_reason').hide(500);
            }
        });
        $('input[type=radio][name=team_signature]').change(function() {

            if($(this).val() =='Reject'){
                $('#datateam_reject_reason').show(500);
            }else{
                $('#datateam_reject_reason').hide(500);
            }
        });





        $(".preapprover").change(function () {
            if (this.checked) {
                $('.requester_acknowledge').hide(500);
                $('.business_div').hide(500);
                $('.preapprove_section').show(500);

            } else {
                $('.requester_acknowledge').show(500);
                $('.business_div').show(500);
                $('.preapprove_section').hide(500);

            }
        });

        // Mariral addmore


        $(function () {

            //workers div
            var perkDiv = $('.personnels');
            var html_personnel = '';
            html_personnel += '<div class="row addPersonnel materialitems">';
            html_personnel += '<div class="col-sm-4">';

            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Name</label>';
            html_personnel += '<input class="form-control" name="personnel_name[]" data-error="Your email address is invalid" required="required" type="text" >';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Title</label>';
            html_personnel += '<input class="form-control" required="required" type="text" name="personnel_title[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += ' </div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Contact Number</label>';
            html_personnel += '<input class="form-control" required="required" type="text" name="personnel_phone[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Email</label>';
            html_personnel += '<input class="form-control" data-error="Your email address is invalid" required="required" type="text" name="personnel_email[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label for=""> ID Details</label>';
            html_personnel += '<button type="button"  class="btn btn-primary  orangebackground">';
            html_personnel += '<i class="fa fa-upload"> </i>';
            html_personnel += '<input type="file" name="personnel_image[]"  required/>';
            html_personnel += '</button>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>ID expiry date</label>';
            html_personnel += '<input class="form-control single-daterange " required="required" type="text" name="personnel_id_expiry[]">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';

            html_personnel += '<div class="col-sm-3">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Signature</label>';
            html_personnel += '<div class="input-group">';
            html_personnel += '<input class="form-control" placeholder="Signature" required="required" type="text" name="personnel_signature[]">';
            html_personnel += '<div class="input-group-btn">';
            html_personnel += '<button class="btn btn-danger remPersonnel" id="remPersonnel" type="button"><i class="fa fa-times"></i></button>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '</div>';

            $('#addPersonnel').on('click', function () {
                $(html_personnel).insertBefore('.materialitem');
                // Date picker
                if ($('input.single-daterange').length) {
                    $('input.single-daterange').daterangepicker({ "singleDatePicker": true,
                        locale: {
                            format: 'DD/MM/YYYY'
                        }
                    })

                }

                $('.remPersonnel').on('click', function () {
                    $(this).parents('.addPersonnel').remove();
                    return false;
                });

                return false;
            });
            $('.remPersonnel').on('click', function () {
                $(this).parents('.addPersonnel').remove();
                return false;
            });
            $('.remPersonnelmain').on('click', function () {
                $(this).parents('.maindiv').remove();
                return false;
            });

            // Date picker
            if ($('input.single-daterange-sig').length) {
                $('input.single-daterange-sig').daterangepicker({ "singleDatePicker": true,
                    locale: {
                        format: 'DD/MM/YYYY hh:mm'
                    }
                })

            }

        });


    </script>