
<style>

    .signature-pad {

        width:400px;
        height:100px;
        background-color: white;
        border: 1px solid #333;
    }
</style>

<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Request Form</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>

        </div>

        <?php if ($this->session->flashdata('error_message')) { ?>
            <div class="alert alert-danger alert-dismissable margintopbtm20">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Sorry! </strong><?php echo $this->session->flashdata('error_message'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success_message')) { ?>
            <div class="alert alert-success alert-dismissable margintopbtm20">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong> </strong><?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php } ?>

        <div class="row" id="printable">
            <div class="col-md-12">
                <div class="content-box">
                    <?php if(isset($request['id']) && $request['id'] <> null) { ?>

                      <!--  <input type="button" class="btn btn-info orangebackground pull-right" value="Print Form"
                               id="btnPrintmainForm"/>-->
                    <?php } ?>

                    <form id="form-validate" method="post" action="" class="padding3 paddingtop30 test"
                          enctype="multipart/form-data">
                        <input class="form-control" value="<?php echo $request['id']; ?>" type="hidden" name="request_id">
                        <input class="form-control" value="<?php echo $request['access_type']; ?>" type="hidden" name="request_type">
                        <input class="form-control" value="<?php echo $request['is_active']; ?>" type="hidden" name="is_active">
                        <div class="form-section-head">
                            <span class="pull-left">DSODC1-DAR-FRM-001</span>
                            <span class="pull-right">Pre-Approve Request Form</span>
                            <div class="clearfix"></div>
                        </div>
                        <!--                        requester information-->
                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" data-error="Your email address is invalid"
                                   value="<?php echo $request['name']; ?>"
                                   placeholder="ABC User"  type="text" name="name" disabled>
                            <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           value="<?php echo $request['email']; ?>"
                                           placeholder="abc@gmail.com"  type="email" name="email"
                                           disabled>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" placeholder="009711234567" required="required" value="<?php echo $request['phone']; ?>"
                                           type="text" name="phone" disabled>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Designation</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           value="<?php  echo $request['designation']; ?>"
                                           name="designation" disabled>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution"
                                           type="text" name="company"
                                           value="<?php  echo $request['company']; ?>" disabled>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access Start Date</label>
                                    <input class="form-control start_date" placeholder="Access Start Date"
                                           type="text" name="start_date" disabled value="<?php  echo date('m/d/Y h:i',strtotime($request['start_date'])); ?>">
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access End Date</label>
                                    <input class=" form-control end_date" placeholder="Access End Date"
                                           type="text" name="end_date" disabled value="<?php  echo  date('m/d/Y h:i',strtotime($request['end_date'])); ?>">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Profile Image</label>

                                    <?php if ($request['profile_image'] != '') { ?>

                                        <img src="<?php echo site_url('uploads/'. $request['profile_image']); ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control" value="<?php echo $request['profile_image']; ?>" type="hidden" name="profile_image">

                                    <?php } else { ?>
                                        <button type="button" id="profile_image"
                                                class="btn btn-primary orangebackground"><i
                                                    class="fa fa-upload"></i>
                                            <span class="hidden-sm-down"> Upload Photo</span>
                                            <input type="file" name="profile_image"/>
                                        </button>



                                        <div class="validation-message" data-field="profile_image"></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Upload Photo ID</label>

                                    <?php if ($request['manager_id_image'] != '') { ?>

                                        <img src="<?php echo site_url('uploads/'. $request['manager_id_image']) ?>"
                                             class="img-responsive formimage" alt="Cinque Terre">
                                        <input class="form-control" value="<?php echo $request['manager_id_image']; ?>" type="hidden" name="manager_id_image">

                                    <?php } else { ?>

                                        <button type="button" class="btn btn-primary  orangebackground">
                                            <i class="fa fa-upload"> </i>
                                            <span class="hidden-sm-down">Upoad Photo ID</span>
                                            <input type="file" name="manager_id_image"/>
                                        </button>



                                        <div class="validation-message" data-field="profile_id"></div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label>ID Expiry Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="card_expiry" disabled
                                           value="<?php echo date('d/m/Y', strtotime($request['card_expiry'])); ?>">
                                </div>
                            </div>
                        </div>


                        <!--                        Access Type-->


                        <div class="form-section bluebackground">
                            <span>Type of Access</span>
                        </div>
                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Pre Approved User <input type="checkbox"
                                                                                         name="access_type"
                                                                                         value="preapprover" disabled
                                                                                         class="check_access_type preapprover"
                                                                                         checked required readonly></label>
                                </div>
                            </div>


                        </div>
                        <!--                        Business-->


                        <!--                        zone-->
                        <div class="zone_div">
                            <div class="form-section">
                                <span>Zone Access Required</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Data Hall </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 1 <input type="checkbox" name="hall_1" disabled
                                                                             value="hall_1" <?php if(isset($request['hall_1']) && ($request['hall_1'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 2 <input type="checkbox" name="hall_2" disabled
                                                                             value="hall_2"  <?php if(isset($request['hall_2']) && ($request['hall_2'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 3 <input type="checkbox" name="hall_3" disabled
                                                                             value="hall_3" <?php if(isset($request['hall_3']) && ($request['hall_3'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Office Area <input type="checkbox" disabled
                                                                                       name="office_area"
                                                                                       value="office_area"  <?php if(isset($request['office_area']) && ($request['office_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> POE Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du1 <input type="checkbox" name="poe_room_du1" disabled
                                                                               value="poe_room_du1" <?php if(isset($request['poe_room_du1']) && ($request['poe_room_du1'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du2 <input type="checkbox" name="poe_room_du2" disabled
                                                                               value="poe_room_du2" <?php if(isset($request['poe_room_du2']) && ($request['poe_room_du2'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET1 <input type="checkbox" name="poe_room_et1" disabled
                                                                               value="poe_room_et1" <?php if(isset($request['poe_room_et1']) && ($request['poe_room_et1'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET2 <input type="checkbox" name="poe_room_et2" disabled
                                                                               value="poe_room_et2" <?php if(isset($request['poe_room_et2']) && ($request['poe_room_et2'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Receiving Area <input type="checkbox"
                                                                                          name="receiving_area" disabled
                                                                                          value="receiving_area" <?php if(isset($request['receiving_area']) && ($request['receiving_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="usp_room_a" disabled
                                                                             value="usp_room_a" <?php if(isset($request['usp_room_a']) && ($request['usp_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="usp_room_b" disabled
                                                                             value="usp_room_b" <?php if(isset($request['usp_room_b']) && ($request['usp_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="usp_room_c" disabled
                                                                             value="usp_room_c" <?php if(isset($request['usp_room_c']) && ($request['usp_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="usp_room_d" disabled
                                                                             value="usp_room_d" <?php if(isset($request['usp_room_d']) && ($request['usp_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Storage Area <input type="checkbox"
                                                                                        name="storage_area" disabled
                                                                                        value="storage_area" <?php if(isset($request['storage_area']) && ($request['storage_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Battery Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="battery_room_a" disabled
                                                                             value="battery_room_a" <?php if(isset($request['battery_room_a']) && ($request['battery_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="battery_room_b" disabled
                                                                             value="battery_room_b"  <?php if(isset($request['battery_room_b']) && ($request['battery_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="battery_room_c" disabled
                                                                             value="battery_room_c" <?php if(isset($request['battery_room_c']) && ($request['battery_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="battery_room_d" disabled
                                                                             value="battery_room_d"  <?php if(isset($request['battery_room_d']) && ($request['battery_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> LAB Area <input type="checkbox" name="lab_area" disabled
                                                                                    value="lab_area" <?php if(isset($request['lab_area']) && ($request['lab_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Output Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="ups_output_room_a" disabled
                                                                             value="ups_output_room_a"  <?php if(isset($request['ups_output_room_a']) && ($request['ups_output_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="ups_output_room_b" disabled
                                                                             value="ups_output_room_b" <?php if(isset($request['ups_output_room_b']) && ($request['ups_output_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="ups_output_room_c" disabled
                                                                             value="ups_output_room_c" <?php if(isset($request['ups_output_room_c']) && ($request['ups_output_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="ups_output_room_d" disabled
                                                                             value="ups_output_room_d" <?php if(isset($request['ups_output_room_d']) && ($request['ups_output_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Control Room <input type="checkbox"
                                                                                        name="control_room" disabled
                                                                                        value="control_room" <?php if(isset($request['control_room']) && ($request['control_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Main Switch Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox" name="main_switch_room_a" disabled
                                                                             value="main_switch_room_a" <?php if(isset($request['main_switch_room_a']) && ($request['main_switch_room_a'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox" name="main_switch_room_b" disabled
                                                                             value="main_switch_room_b" <?php if(isset($request['main_switch_room_b']) && ($request['main_switch_room_b'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox" name="main_switch_room_c" disabled
                                                                             value="main_switch_room_c" <?php if(isset($request['main_switch_room_c']) && ($request['main_switch_room_c'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox" name="main_switch_room_d" disabled
                                                                             value="main_switch_room_d" <?php if(isset($request['main_switch_room_d']) && ($request['main_switch_room_d'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>


                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Preaction Room <input type="checkbox"
                                                                                          name="preaction_room" disabled
                                                                                          value="preaction_room"  <?php if(isset($request['preaction_room']) && ($request['preaction_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Main Tank Area <input type="checkbox" disabled
                                                                                               name="fuel_main_tank_area"
                                                                                               value="fuel_main_tank_area" <?php if(isset($request['fuel_main_tank_area']) && ($request['fuel_main_tank_area'] !='')) { echo 'checked'; } ?>>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Pump Room <input type="checkbox" disabled
                                                                                          name="fuel_pump_room"
                                                                                          value="fuel_pump_room" <?php if(isset($request['fuel_pump_room']) && ($request['fuel_pump_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> AHU Room <input type="checkbox" name="ahu_room" disabled
                                                                                    value="ahu_room" <?php if(isset($request['ahu_room']) && ($request['ahu_room'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Water Pump Room <input type="checkbox" disabled
                                                                                                   name="chilled_water_pump_room"
                                                                                                   value="chilled_water_pump_room" <?php if(isset($request['chilled_water_pump_room']) && ($request['chilled_water_pump_room'] !='')) { echo 'checked'; } ?>>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Area Backyard <input type="checkbox" disabled
                                                                                                 name="chilled_area_backyard"
                                                                                                 value="chilled_area_backyard" <?php if(isset($request['chilled_area_backyard']) && ($request['chilled_area_backyard'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> External Area <input type="checkbox" disabled
                                                                                         name="external_area"
                                                                                         value="external_area" <?php if(isset($request['external_area']) && ($request['external_area'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Generator Room <input type="checkbox" disabled
                                                                                          name="generator_room"
                                                                                          value="generator_room" <?php if(isset($request['generator_room']) && ($request['generator_room'] !='')) { echo 'checked'; } ?>>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> First Floor Corridor <input type="checkbox" disabled
                                                                                                name="first_floor_corridor"
                                                                                                value="first_floor_corridor" <?php if(isset($request['first_floor_corridor']) && ($request['first_floor_corridor'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Ground Floor Corridor <input type="checkbox" disabled
                                                                                                 name="ground_floor_corridor"
                                                                                                 value="ground_floor_corridor" <?php if(isset($request['ground_floor_corridor']) && ($request['ground_floor_corridor'] !='')) { echo 'checked'; } ?>></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                        </div>


                        <!--                        preapprove section-->
                        <div class="preapprove_section">
                            <div class="form-section">
                                <span>Main Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text" disabled value="<?php  echo $request['pre_manager_name']; ?>"
                                               name="pre_manager_name">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="0971 0000000" required="required" disabled
                                               type="text" name="pre_manager_phone" value="<?php  echo $request['pre_manager_phone']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid" disabled
                                               placeholder="Manager" required="required" type="text"
                                               name="pre_manager_email" value="<?php  echo $request['pre_manager_email']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>




                            </div>


                            <div class="form-section">
                                <span>Backup Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Please Enter Contact Person name"
                                               placeholder="Contact Person" required="required" type="text" disabled
                                               name="contact_person_name" value="<?php  echo $request['contact_person_name']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Contact Number" required="required" disabled
                                               type="text" name="contact_person_number"  value="<?php  echo $request['contact_person_number']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text" disabled
                                               name="contact_person_email"  value="<?php  echo $request['contact_person_email']; ?>">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-section">
                                <span>Pre Approved Personnel</span>
                            </div>
                            <div class="personnels"></div>

                            <?php if(!empty($personnels)){

                                ?>






                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Contact Number</th>
                                            <th>Email</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                <?php foreach($personnels as $key=>$data){  ?>
                                        <tr>
                                            <td><?php echo $data['personnel_name'];  ?></td>
                                            <td><?php echo $data['personnel_phone'];  ?></td>
                                            <td><?php echo $data['personnel_email'];  ?></td>
                                        </tr>
                                       <?php } ?>
                                        </tbody>
                                    </table>




                            <?php } ?>



                            <div class="form-section">
                                <span>Operations</span>
                            </div>

                            <div class="row">





                             <?php  if($request['is_active'] == 0 ){ ?>

                                 <div class="col-sm-12 text-center">
                                     <div class="form-group">
                                         <label class="check-label">  <input type="checkbox"  class="term" name="business_requirements">Agree with the terms and conditions</label>
                                         <input class="form-control hidden" name="agree" id="agree_id" data-error="You must agree with the terms and conditions" placeholder="Designation"
                                                required="required" type="text" value="<?php echo set_value('agree'); ?>">
                                         <div class="help-block form-text with-errors form-control-feedback" id="agree_error"></div>
                                         <div class="clearfix"></div>
                                     </div>
                                 </div>

                                <div class="col-sm-12 text-center">
                                    <div class="form-group">
                                        <div class="signature_div">
                                            <label class="check-label">Signature for sign In</label>
                                            <canvas id="signature-pad" class="signature-pad" width=100 height=50></canvas>
                                            <div class="clearfix"></div>
                                            <div class="help-block form-text with-errors form-control-feedback hidden" id="sigerror" style=" color: #d9534f; padding-bottom: 10px;">Please Sign In First</div>

                                            <textarea name="sigpattern" class="hidden" id="sigpattern"></textarea>


                                            <button class="btn btn-default orangebackground" id="clear">Clear</button>
                                            <button class="btn btn-info bluebackground" id="signedbtn" >Sign In</button>
                                        </div>

                                    </div>
                                </div>

                                <?php } else if($request['is_active'] == 1) {?>


                                <div class="col-sm-12 text-center">
                                    <div class="form-group">

                                        <div class="signature_div">
                                            <div class="success heading3"> You are Now Signed In!</div>
                                            <label class="check-label">Signature for sign Out</label>
                                            <canvas id="signature-pad" class="signature-pad" width=100 height=50></canvas>
                                            <div class="clearfix"></div>
                                            <div class="help-block form-text with-errors form-control-feedback hidden" id="sigerror" style=" color: #d9534f; padding-bottom: 10px;">Please Sign In First</div>

                                            <textarea name="sigpattern" class="hidden" id="sigpattern"></textarea>
                                            <button class="btn btn-default orangebackground" id="clear">Clear</button>
                                            <button class="btn btn-info bluebackground" >Sign Out</button>
                                        </div>
                                    </div>
                                    <?php } ?>


                                </div>

                            </div>


                        <div class="content-box-footer">
                            <!--<button class="btn btn-primary bluebackground">Save</button>-->
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back
                                To
                                Requests</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>


    </div>

    <!-- Terms and conditions modal -->
    <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title heading3">Terms and conditions</h3>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="bullets col-sm-12">

                            <ul>
                                <li>This Permit is not applicable for specific task such as Confined Space
                                    Entry, work at Height, Hot work, Spray painting, Structural
                                    Penetration, Film Shooting, Excavation Works, Handling of Chemicals,
                                    Isolation of services and integration with existing systems
                                    etc. separate Work Permit will be required along with access
                                </li>
                                <li>All devices are to be housed in Racks. Racks can be procured and installed
                                    through the DC Operational Teams coordination.
                                </li>
                                <li>Prior approval from DC Operational Team is required to identify where to
                                    place the customer racks.
                                </li>
                                <li>Arrangements for delivery of equipment shall be made in coordination with
                                    the DC Operational Teams. The Operation Team will
                                    determine how to receive the order and where to store the equipment prior to
                                    installation on Separate Material Movement Form
                                </li>
                                <li>It is Customers responsibility to work with the DC Operations team to
                                    arrange for their equipment to be located in the proper
                                    location in the data center.
                                </li>

                                <li>Disposing of all refuse (cardboard, Styrofoam, plastic, pallets, etc.) is
                                    the responsibility of the customer to the Collection or
                                    Refuse Point.
                                </li>
                                <li>No supplies, boxes or unused equipment will be stored in or on top of server
                                    cabinets.
                                </li>
                                <li>No combustible material should be left in the data center at all. This
                                    includes cardboard, wood, plastic, tools, etc...
                                </li>
                                <li>Eating, Drinking, Smoking, packing & unpacking is strictly prohibited inside
                                    the Data Center except the designated areas
                                </li>
                                <li>The DC Operations Team will review the Access and Requester will be
                                    contacted if additional information is required.
                                </li>
                                <li>No Access will be granted without handing or giving a valid photo ID card at
                                    the security.
                                </li>
                                <li>Allocated access cards should always be worn during the time inside the DC
                                    Facility and returned at the security gate at the time
                                    of leaving.
                                </li>
                                <li>Any Noisy or malodourous activities such as drilling, coring, painting etc.
                                    are permitted only after working hours and the applicant
                                    to coordinate with the building security and DC Operations team for the same
                                    to avoid any stoppage of works.
                                </li>
                                <li>In case of loss of the given access card the customer should pay 100 AED for
                                    replacement
                                </li>


                            </ul>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary bluebackground" id="agreeButton" data-dismiss="modal">Agree</button>
                    <button type="button" class="btn btn-default orangebackground" id="disagreeButton" data-dismiss="modal">Disagree</button>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>





    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/signature_js/signature_pad.js'; ?>"></script>

    <script type="text/javascript">



        $(".term").change(function () {
            if (this.checked) {
                $('#termsModal').appendTo("body").modal('show');

            } else {


            }
        });

        // Update the value of "agree" input when clicking the Agree/Disagree button
        $('#agreeButton, #disagreeButton').on('click', function() {
            var whichButton = $(this).attr('id');

            $('#form-validate')
                .find('[name="agree"]')
                .val(whichButton === 'agreeButton' ? 'yes' : '')
                .end();
            var agreeValue = $('#agree_id').val();
            if(agreeValue=='yes'){
                $('#agree_error').hide(200);

            }else{
                $('.term').attr('checked', false)
                $('#agree_error').show(200);
            }
        });



        //material check boxes
        $('#signinbtn').click(function () {
            $('#matOut').appendTo("body").modal('show');
        });
        var canvas = document.getElementById('signature-pad');
        var canvas2 = document.getElementById('signature-pad2');
        function resizeCanvas() {
            // When zoomed out to less than 100%, for some very strange reason,
            // some browsers report devicePixelRatio as less than 1
            // and only part of the canvas is cleared then.
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
        }
        window.onresize = resizeCanvas;
        resizeCanvas();

        var signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        });

        $('.test').submit(function(){
            if( signaturePad.isEmpty()){
                $('#sigerror').show(500);
                return false;
            }else{
                $('#sigerror').hide(500);
                const data = signaturePad.toData();
                $('#sigpattern').val(JSON.stringify((data)));
            }


        });


        $('#clear').on('click',function(){
            signaturePad.clear();
            return false;
        });

    </script>
