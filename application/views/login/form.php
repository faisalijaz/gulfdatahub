<div class="darkbackround2 paddingtop30 paddingbtm30">
<div class="register-wrapper auth-wrapper">
    <div class="auth-body">




        <form id="form-validate" method="post" action="" class="padding3 paddingtop30" enctype="multipart/form-data">

            <?php if($this->session->flashdata('error_message')){ ?>
                <div class="alert alert-danger alert-dismissable margintopbtm20">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Sorry! </strong><?php echo  $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <div class="form-group">
                <label>Email address *</label>
                <input class="form-control" name="email" data-error="Your email address is invalid"
                       placeholder="Enter email"
                       required="required" type="email" data-remote="/auth/emailCheck"  value="<?php echo set_value('email'); ?>">
                <div class="help-block form-text with-errors form-control-feedback"></div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Password *</label>
                        <input class="form-control" name="password" data-minlength="6" id="inputPassword" placeholder="Password"
                               required="required"
                               type="password"  value="<?php echo set_value('password'); ?>">
                        <div class="help-block form-text text-muted form-control-feedback">Minimum of 6 characters</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Confirm Password *</label>
                        <input type="password" class="form-control" name="confirmPassword"
                               id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="The password and its confirm are not the same"
                               placeholder="Confirm Password" required="required"  value="<?php echo set_value('confirmPassword'); ?>" >
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>First Name *</label>
                        <input class="form-control" name="first_name" data-error="Please input your First Name"
                               placeholder="First Name"
                               required="required" type="text"  value="<?php echo set_value('first_name'); ?>">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Last Name *</label>
                        <input class="form-control" name="last_name" data-error="Please input your Last Name"
                               placeholder="Last Name"
                               required="required" type="text"  value="<?php echo set_value('last_name'); ?>">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Contact Detail *</label>
                        <input class="form-control" name="phone" id="phone" data-error="Please input your contact number"
                               placeholder="Contact Number (i.e 971 XXXXXXXXXX) "
                               required="required" type="text"  value="<?php echo set_value('phone'); ?>">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Company Name *</label>
                        <input class="form-control" name="company_name" data-error="Please input your Company Name"
                               placeholder="Company Name"
                               required="required" type="text"  value="<?php echo set_value('company_name'); ?>">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Designation *</label>
                        <input class="form-control" name="designation" data-error="Please input your Designation" placeholder="Designation"
                               required="required" type="text" value="<?php echo set_value('designation'); ?>">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                    </div>
                </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control" required>

                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                            <div class="validation-message" data-field="group"></div>
                        </div>
                    </div>
                </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Profile Image</label>

                        <button type="button" id="profile_image"
                                class="btn btn-primary orangebackground"><i
                                    class="fa fa-upload"></i>
                            <span class="hidden-sm-down"> Upload Photo</span>
                            <input type="file" name="photo" required />
                        </button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">  Upoad Photo ID</label>
                        <button type="button" id="profile_image"
                                class="btn btn-primary orangebackground"><i
                                    class="fa fa-upload"></i>
                            <span class="hidden-sm-down"> Upoad Photo ID</span>
                            <input type="file" name="image_id" required />
                        </button>
                        <div class="validation-message" data-field="images"></div>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>ID Expiry Date</label>
                    <input class="form-control single-daterange" name="id_expiry_date" data-error="Please input your Designation" placeholder="ID Expiry Date"
                           required="required" type="text" value="" >
                    <div class="help-block form-text with-errors form-control-feedback"></div>
                </div>
            </div>
            </div>


            <div class="form-group">
                <div class="col-xs-6 col-xs-offset-3">


                    <label class="check-label">  <input type="checkbox"  class="term" name="business_requirements">Agree with the terms and conditions</label>
                    <input class="form-control hidden" name="agree" id="agree_id" data-error="You must agree with the terms and conditions" placeholder="Designation"
                           required="required" type="text" value="<?php echo set_value('agree'); ?>">
                    <div class="help-block form-text with-errors form-control-feedback" id="agree_error"></div>

                </div>
            </div>
                <div class="content-box-footer">
                    <button class="btn btn-default bluebackground whitefont">Register</button>
                    <a href="<?php echo site_url('auth/login') ?>" class="btn btn-defult bluebackground whitefont">Back To Login</a>
                </div>
        </form>


        <!-- Terms and conditions modal -->
        <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title heading3">Terms and conditions</h3>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="bullets col-sm-12">

                                <ul>
                                    <li>This Permit is not applicable for specific task such as Confined Space
                                        Entry, work at Height, Hot work, Spray painting, Structural
                                        Penetration, Film Shooting, Excavation Works, Handling of Chemicals,
                                        Isolation of services and integration with existing systems
                                        etc. separate Work Permit will be required along with access
                                    </li>
                                    <li>All devices are to be housed in Racks. Racks can be procured and installed
                                        through the DC Operational Teams coordination.
                                    </li>
                                    <li>Prior approval from DC Operational Team is required to identify where to
                                        place the customer racks.
                                    </li>
                                    <li>Arrangements for delivery of equipment shall be made in coordination with
                                        the DC Operational Teams. The Operation Team will
                                        determine how to receive the order and where to store the equipment prior to
                                        installation on Separate Material Movement Form
                                    </li>
                                    <li>It is Customers responsibility to work with the DC Operations team to
                                        arrange for their equipment to be located in the proper
                                        location in the data center.
                                    </li>

                                    <li>Disposing of all refuse (cardboard, Styrofoam, plastic, pallets, etc.) is
                                        the responsibility of the customer to the Collection or
                                        Refuse Point.
                                    </li>
                                    <li>No supplies, boxes or unused equipment will be stored in or on top of server
                                        cabinets.
                                    </li>
                                    <li>No combustible material should be left in the data center at all. This
                                        includes cardboard, wood, plastic, tools, etc...
                                    </li>
                                    <li>Eating, Drinking, Smoking, packing & unpacking is strictly prohibited inside
                                        the Data Center except the designated areas
                                    </li>
                                    <li>The DC Operations Team will review the Access and Requester will be
                                        contacted if additional information is required.
                                    </li>
                                    <li>No Access will be granted without handing or giving a valid photo ID card at
                                        the security.
                                    </li>
                                    <li>Allocated access cards should always be worn during the time inside the DC
                                        Facility and returned at the security gate at the time
                                        of leaving.
                                    </li>
                                    <li>Any Noisy or malodourous activities such as drilling, coring, painting etc.
                                        are permitted only after working hours and the applicant
                                        to coordinate with the building security and DC Operations team for the same
                                        to avoid any stoppage of works.
                                    </li>
                                    <li>In case of loss of the given access card the customer should pay 100 AED for
                                        replacement
                                    </li>


                                </ul>


                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary bluebackground" id="agreeButton" data-dismiss="modal">Agree</button>
                        <button type="button" class="btn btn-default orangebackground" id="disagreeButton" data-dismiss="modal">Disagree</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>

<script  type="text/javascript">

    $(".term").change(function () {
        if (this.checked) {
            $('#termsModal').appendTo("body").modal('show');

        } else {


        }
    });
    $('#workform').on('click', function () {
        $('#myModal').appendTo("body").modal('show');

    });
    // Update the value of "agree" input when clicking the Agree/Disagree button
    $('#agreeButton, #disagreeButton').on('click', function() {
        var whichButton = $(this).attr('id');

        $('#form-validate')
            .find('[name="agree"]')
            .val(whichButton === 'agreeButton' ? 'yes' : '')
            .end();
        var agreeValue = $('#agree_id').val();
        if(agreeValue=='yes'){
            $('#agree_error').hide(200);

        }else{
            $('.term').attr('checked', false)
            $('#agree_error').show(200);
        }
    });

    $('#phone').mask('00000000000000', {'translation': {0: {pattern: /[0-9+]/}}});
</script>
