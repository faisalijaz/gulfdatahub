<?php if($this->session->flashdata('success_message')){ ?>
<div class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Congratulations! </strong><?php echo  $this->session->flashdata('success_message'); ?>
</div>
<?php } ?>

<div class="breadcrumb">
	<a href="">Home</a>
</div>
<div class="top-banner">

	<div class="top-banner-title">Dashboard</div>
	<div class="top-banner-subtitle">Welcome back, <?php echo $active_user->name; ?>, <i class="fa fa-map-marker"></i> United Arab Emirates</div>
</div>
<div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-newspaper-o"></i>
		<div class="content-header-title">Access Requests</div>
		<select class="select-rounded pull-right">
			<option>Today</option>
			<option>7 Days</option>
			<option>14 Days</option>
			<option>Last Month</option>
		</select>
	</div>
	<div class="panel">
		<div class="row">
			<div class="col-md-3 card-wrapper">
				<div class="card">
					<i class="fa fa-thumbs-o-up"></i>
					<div class="clear">
						<div class="card-title">
							<span class="timer" data-from="0" data-to="1121">121</span>
						</div>
						<div class="card-subtitle">Approved</div>
					</div>
				</div>
				<div class="card-menu">
					<ul>
						<li><a href="">Today</a></li>
						<li><a href="">7 Days</a></li>
						<li><a href="">14 Days</a></li>
						<li><a href="">Last Month</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 card-wrapper">
				<div class="card">
					<i class="fa fa-clock-o"></i>
					<div class="clear">
						<div class="card-title">
                            <span class="timer" data-from="0" data-to="1121">11</span>
						</div>
						<div class="card-subtitle">Pending</div>
					</div>
				</div>
				<div class="card-menu">
					<ul>
						<li><a href="">Today</a></li>
						<li><a href="">7 Days</a></li>
						<li><a href="">14 Days</a></li>
						<li><a href="">Last Month</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 card-wrapper">
				<div class="card">
					<i class="fa fa-times-circle-o"></i>
					<div class="clear">
						<div class="card-title">
                            <span class="timer" data-from="0" data-to="1121">2</span>
						</div>
						<div class="card-subtitle">Expired</div>
					</div>
				</div>
				<div class="card-menu">
					<ul>
						<li><a href="">Today</a></li>
						<li><a href="">7 Days</a></li>
						<li><a href="">14 Days</a></li>
						<li><a href="">Last Month</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-ban"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="1121">7</span>
                        </div>
                        <div class="card-subtitle">Canceled</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>


            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-ban"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="16">7</span>
                        </div>
                        <div class="card-subtitle">Today Vistors</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-ban"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="4">7</span>
                        </div>
                        <div class="card-subtitle">Signed In</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-ban"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="7">7</span>
                        </div>
                        <div class="card-subtitle">Signed Out</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>


		</div>
	</div>

</div>