<?php if($this->session->flashdata('success_message')){ ?>
<div class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Congratulations! </strong><?php echo  $this->session->flashdata('success_message'); ?>
</div>
<?php } ?>

<div class="breadcrumb">
	<a href="">Home</a>
</div>
<div class="top-banner">

	<div class="top-banner-title">Dashboard</div>
	<div class="top-banner-subtitle">Welcome back, <?php echo $active_user->name; ?>, <i class="fa fa-map-marker"></i> United Arab Emirates</div>
</div>
<div class="content with-top-banner">
	<div class="content-header no-mg-top">
		<i class="fa fa-newspaper-o"></i>
		<div class="content-header-title">Access Requests</div>
		<select class="select-rounded pull-right">
			<option>Today</option>
			<option>7 Days</option>
			<option>14 Days</option>
			<option>Last Month</option>
		</select>
	</div>
	<div class="panel">
		<div class="row">
            <?php if($this->session->userdata['active_user']->group_id == 1 ){ ?>
			<div class="col-md-3 card-wrapper">
				<div class="card">
					<i class="fa fa-thumbs-o-up"></i>
					<div class="clear">
						<div class="card-title">
							<span class="timer" data-from="0" data-to="<?php echo (count_preappeover_Approved() + count_other_Approved()); ?>">121</span>
						</div>
						<div class="card-subtitle">Approved</div>
					</div>
				</div>
				<div class="card-menu">
					<ul>
						<li><a href="">Today</a></li>
						<li><a href="">7 Days</a></li>
						<li><a href="">14 Days</a></li>
						<li><a href="">Last Month</a></li>
					</ul>
				</div>
			</div>
            <?php } ?>
            <?php if($this->session->userdata['active_user']->group_id == 1 || $this->session->userdata['active_user']->group_id != 5){ ?>
			<div class="col-md-3 card-wrapper">
				<div class="card">
					<i class="fa fa-clock-o"></i>
					<div class="clear">
						<div class="card-title">
                            <span class="timer" data-from="0" data-to="<?php echo (count_preappeover_Pending() + count_other_Pending()); ?>">1</span>
						</div>
						<div class="card-subtitle">Pending</div>
					</div>
				</div>
				<div class="card-menu">
					<ul>
						<li><a href="">Today</a></li>
						<li><a href="">7 Days</a></li>
						<li><a href="">14 Days</a></li>
						<li><a href="">Last Month</a></li>
					</ul>
				</div>
			</div>

			<div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-ban"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="<?php echo (count_preappeover_Reject() + count_other_Reject()); ?>">7</span>
                        </div>
                        <div class="card-subtitle">Canceled</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>
            <?php } ?>

            <?php if($this->session->userdata['active_user']->group_id == 1 || $this->session->userdata['active_user']->group_id == 2){ ?>
            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-user"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="15">7</span>
                        </div>
                        <div class="card-subtitle">Today Vistors</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>


            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-user"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="<?php echo (count_preappeover_checkin() + count_other_checkin()); ?>"></span>
                        </div>
                        <div class="card-subtitle">Check In</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-user"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="<?php echo (count_preappeover_checkout() + count_other_checkout()); ?>"></span>
                        </div>
                        <div class="card-subtitle">Check Out</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-user"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="<?php echo (count_preappeover_checkin() + count_other_checkin()); ?>">7</span>
                        </div>
                        <div class="card-subtitle">On-Site Current Status</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>

         <!--   <div class="col-md-3 card-wrapper">
                <div class="card">
                    <i class="fa fa-user"></i>
                    <div class="clear">
                        <div class="card-title">
                            <span class="timer" data-from="0" data-to="4">7</span>
                        </div>
                        <div class="card-subtitle">PM Notification(Data Team)</div>
                    </div>
                </div>
                <div class="card-menu">
                    <ul>
                        <li><a href="">Today</a></li>
                        <li><a href="">7 Days</a></li>
                        <li><a href="">14 Days</a></li>
                        <li><a href="">Last Month</a></li>
                    </ul>
                </div>
            </div>-->
            <?php } ?>


		</div>
	</div>

</div>