<?php

if (isset($request_data) && count($request_data) > 0) {
    $request_data = $request_data[0];
} else {
    $request_data = null;
}
?>
<div class="breadcrumb">
    <a href="<?php echo base_url(); ?>">Home</a>
    <a href="">Request Form</a>
</div>
<div class="content" id="printable">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">

                    <?php if (isset($request_data) && count($request_data) > 0) { ?>

                        <input type="button" class="btn btn-info orangebackground pull-right" style="margin-bottom: 20px;" value="Print Form"
                               id="btnPrintmainForm"/>
                        <div class="clearfix"></div>
                    <?php } ?>

                    <form id="validate-form" class="accessRequestForm" method="POST"
                          action="<?= base_url(); ?>accessRequests/<?= (isset($request_data) && $request_data <> null) ? 'update/'.$request_data->id : 'create'; ?>"
                          class=" padding3 paddingtop30"
                          enctype="multipart/form-data">

                        <div class="form-section-head" style="height: 45px;">
                            <span class="pull-left">DSODC1-DAR-FRM-001</span>
                            <span class="pull-right">Access Request Form</span>
                        </div>

                        <input class="form-control" data-error="Please input your First Name"
                               placeholder="DSODC1-DAR-FRM-001"
                               value="<?= (isset($request_data) && $request_data <> null) ? $request_data->refrence_number : ''; ?>"
                               type="hidden"
                               name="refrence_number">
                        <input type="hidden" name="id"
                               value="<?= (isset($request_data) && $request_data <> null) ? $request_data->id : ''; ?>">

                        <!--##### requester information-->

                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>

                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" data-error="Your email address is invalid"
                                   placeholder="ABC User" required="required" type="text" name="name"
                                   value="<?= (isset($request_data) && $request_data <> null) ? $request_data->name : $this->session->userdata['active_user']->name; ?>">
                            <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="abc@gmail.com" required="required" type="email"
                                           name="email"
                                           value="<?= (isset($request_data) && $request_data <> null) ? $request_data->email : $this->session->userdata['active_user']->email; ?>"
                                           readonly>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" placeholder="009711234567" required="required"
                                           type="text" name="phone"
                                           value="<?= (isset($request_data) && $request_data <> null) ? $request_data->phone : $this->session->userdata['active_user']->phone; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label>Designation</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           name="designation"
                                           value="<?= (isset($request_data) && $request_data <> null) ? $request_data->designation : $this->session->userdata['active_user']->designation; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                           type="text" name="company"
                                           value="<?= (isset($request_data) && $request_data <> null) ? $request_data->company : ''; ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Access Start & End Date</label>
                                    <input class="form-control start_date" placeholder="Date of birth"
                                           type="text" name="start_date" id="start_end_date"
                                           value="<?= (isset($request_data) && $request_data <> null) ? date('m/d/Y h:i', strtotime($request_data->start_date)) : ''; ?>">
                                </div>
                            </div>

                            <!--<div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access End Date</label>
                                    <input class="form-control end_date" placeholder="Date of birth"
                                           type="text" name="end_date"
                                           value="<? /*= (isset($request_data) && $request_data <> null) ? date('m/d/Y h:i', strtotime($request_data->end_date)) : ''; */ ?>">
                                </div>
                            </div>-->

                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Profile Image</label>
                                    <?php
                                    $p_image = "required";
                                    if (isset($request_data) && $request_data <> null) {
                                        if ($request_data->profile_image <> "") {
                                            $p_image = "";
                                            ?>
                                            <img src="<?= base_url() . 'uploads/' . $request_data->profile_image; ?>"
                                                 width="100"/>
                                            <input type="hidden" name="old_profile_image"
                                                   value="<?= $request_data->profile_image; ?>"/>
                                            <?php
                                        }
                                    }
                                    ?> <br/>
                                    <button type="button" id="profile_image"
                                            class="btn btn-primary orangebackground"><i
                                                class="fa fa-upload"></i>
                                        Upoad Photo
                                        <input type="file" name="profile_image" <?= $p_image; ?> />
                                    </button>
                                    <div class="validation-message" data-field="profile_image"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for=""> Upload ID</label>
                                    <?php
                                    $p_m_image = "required";
                                    if (isset($request_data) && $request_data <> null) {
                                        if ($request_data->manager_id_image <> "") {
                                            $p_m_image = "";
                                            ?>
                                            <img src="<?= base_url() . 'uploads/' . $request_data->manager_id_image; ?>"
                                                 width="100"/>
                                            <input type="hidden" name="old_manager_id_image"
                                                   value="<?= $request_data->manager_id_image; ?>"/>
                                            <?php
                                        }
                                    }
                                    ?> <br/>
                                    <button type="button" class="btn btn-primary  orangebackground">
                                        <i class="fa fa-upload"> </i>
                                        Upoad Image ID
                                        <input type="file" name="manager_id_image" <?= $p_m_image; ?> />
                                    </button>

                                </div>
                                <div class="validation-message" data-field="profile_id"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Card Expiry Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="card_expiry"
                                           value="<?= (isset($request_data) && $request_data <> null) ? date('d/m/Y', strtotime($request_data->card_expiry)) : ''; ?>">
                                </div>
                            </div>
                        </div>
                </div>

                <!-- Access Type-->

                <div class="form-section bluebackground">
                    <span>Type of Access</span>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="check-label"> New User
                                <input type="checkbox" name="access_type"
                                    <?= (isset($request_data) && $request_data <> null && $request_data->access_type == "new_user") ? "checked" : ''; ?>
                                       value="new_user"
                                       class="check_access_type"></label>
                        </div>
                    </div>
                    <!-- <div class="col-sm-3">
                         <div class="form-group">
                             <label class="check-label"> Pre Approved User <input type="checkbox"
                                                                                  name="access_type" value="preapprover"
                                                                                  class="check_access_type preapprover" required></label>
                         </div>
                     </div>-->
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="check-label"> Emergency Access <input type="checkbox"
                                    <?= (isset($request_data) && $request_data <> null && $request_data->access_type == "emergency") ? "checked" : ''; ?>
                                                                                name="access_type" value="emergency"
                                                                                class="check_access_type">
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">

                    <?php if (isset($request_data) && count($request_data) > 0) { ?>

                        <input type="button" class="btn btn-info orangebackground pull-right" value="Print Work Permit"
                               id="printWorkPermitbtn"/>
                    <?php }else{ ?>


                                <label class="check-label"> Work Permit <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->access_type == "work_permit") ? "checked" : ''; ?>
                                                                               name="work_permit_type" id="workoutform"
                                                                               value="work_permit"
                                                                               class="check_access_type">
                                </label>



                    <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- Business-->

                <div class="business_div">
                    <div class="form-section">
                        <span>Specify Business Requests For Access Request</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Site Visit <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "site_visit") ? "checked" : ''; ?>
                                                                              name="business_requirements"
                                                                              value="site_visit"
                                                                              class="business_requirements"
                                    ></label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Mobilization <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "mobilization") ? "checked" : ''; ?>

                                                                                name="business_requirements"
                                                                                value="mobilization"
                                                                                class="business_requirements"></label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Fit-Out Work <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "fit_out_work") ? "checked" : ''; ?>

                                                                                name="business_requirements"
                                                                                value="fit_out_work"
                                                                                class="business_requirements">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Material Movement <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "material_movement") ? "checked" : ''; ?>

                                                                                     name="business_requirements"
                                                                                     value="material_movement"
                                                                                     class="business_requirements"
                                    ></label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> FM Services <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "fm_services") ? "checked" : ''; ?>

                                                                               name="business_requirements"
                                                                               value="fm_services"
                                                                               class="business_requirements"
                                    ></label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Equipment Inspection <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "business_requirements") ? "checked" : ''; ?>

                                                                                        name="business_requirements"
                                                                                        value="equipment_inspection"
                                                                                        class="business_requirements"
                                    >
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Meeting <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "meeting") ? "checked" : ''; ?>

                                                                           name="business_requirements" value="meeting"
                                                                           class="business_requirements">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Courier <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "courier") ? "checked" : ''; ?>

                                                                           name="business_requirements" value="courier"
                                                                           class="business_requirements">
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Minor Work <input type="checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "minor_work") ? "checked" : ''; ?>

                                                                              name="business_requirements"
                                                                              value="minor_work"
                                                                              class="business_requirements">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Other <input type="checkbox" id="businessreqs_other_check"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->business_requirements == "other") ? "checked" : ''; ?>

                                                                         name="business_requirements" value="other"
                                                                         class="business_requirements">
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6" id="business_reqs_other"></div>

                    </div>
                </div>
                <!--                        zone-->
                <div class="zone_div">
                    <div class="form-section">
                        <span>Zone Access Required</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Data Hall </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> 1 <input type="checkbox" name="hall_1"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->hall_1 == "hall_1") ? "checked" : ''; ?>
                                                                     value="hall_1"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> 2 <input type="checkbox" name="hall_2"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->hall_2 == "hall_2") ? "checked" : ''; ?>

                                                                     value="hall_2"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> 3 <input type="checkbox" name="hall_3"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->hall_3 == "hall_3") ? "checked" : ''; ?>

                                                                     value="hall_3"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">

                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="check-label"> Office Area <input type="checkbox"
                                                                               class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->office_area == "office_area") ? "checked" : ''; ?>

                                                                               name="office_area"
                                                                               value="office_area"></label>
                            </div>
                        </div>
                        <div class="clearifix"></div>


                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> POE Room </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> Du1 <input type="checkbox" name="poe_room_du1"
                                                                       class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->poe_room_du1 == "poe_room_du1") ? "checked" : ''; ?>

                                                                       value="poe_room_du1"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> Du2 <input type="checkbox" name="poe_room_du2"
                                                                       class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->poe_room_du2 == "poe_room_du2") ? "checked" : ''; ?>

                                                                       value="poe_room_du2"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> ET1 <input type="checkbox" name="poe_room_et1"
                                                                       class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->poe_room_et1 == "poe_room_et1") ? "checked" : ''; ?>

                                                                       value="poe_room_et1"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> ET2 <input type="checkbox" name="poe_room_et2"
                                                                       class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->poe_room_et2 == "poe_room_et2") ? "checked" : ''; ?>

                                                                       value="poe_room_et2"></label>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="check-label"> Receiving Area <input type="checkbox"
                                                                                  class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->receiving_area == "receiving_area") ? "checked" : ''; ?>

                                                                                  name="receiving_area"
                                                                                  value="receiving_area"></label>
                            </div>
                        </div>
                        <div class="clearifix"></div>


                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> UPS Room </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> A <input type="checkbox" name="usp_room_a"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->usp_room_a == "usp_room_a") ? "checked" : ''; ?>

                                                                     value="usp_room_a"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> B <input type="checkbox" name="usp_room_b"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->usp_room_b == "usp_room_b") ? "checked" : ''; ?>

                                                                     value="usp_room_b"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> C <input type="checkbox" name="usp_room_c"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->usp_room_c == "usp_room_c") ? "checked" : ''; ?>

                                                                     value="usp_room_c"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> D <input type="checkbox" name="usp_room_d"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->usp_room_d == "usp_room_d") ? "checked" : ''; ?>

                                                                     value="usp_room_d"></label>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="check-label"> Storage Area <input type="checkbox"
                                                                                class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->storage_area == "storage_area") ? "checked" : ''; ?>

                                                                                name="storage_area"
                                                                                value="storage_area"></label>
                            </div>
                        </div>
                        <div class="clearifix"></div>


                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Battery Room </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> A <input type="checkbox" name="battery_room_a"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->battery_room_a == "battery_room_a") ? "checked" : ''; ?>

                                                                     value="battery_room_a"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> B <input type="checkbox" name="battery_room_b"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->battery_room_b == "battery_room_b") ? "checked" : ''; ?>

                                                                     value="battery_room_b"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> C <input type="checkbox" name="battery_room_c"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->battery_room_c == "battery_room_c") ? "checked" : ''; ?>

                                                                     value="battery_room_c"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> D <input type="checkbox" name="battery_room_d"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->battery_room_d == "battery_room_d") ? "checked" : ''; ?>

                                                                     value="battery_room_d"></label>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="check-label"> LAB Area <input type="checkbox" name="lab_area"
                                                                            class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->lab_area == "lab_area") ? "checked" : ''; ?>

                                                                            value="lab_area"></label>
                            </div>
                        </div>
                        <div class="clearifix"></div>

                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> UPS Output Room </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> A <input type="checkbox" name="ups_output_room_a"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->ups_output_room_a == "ups_output_room_a") ? "checked" : ''; ?>

                                                                     value="ups_output_room_a"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> B <input type="checkbox" name="ups_output_room_b"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->ups_output_room_b == "ups_output_room_b") ? "checked" : ''; ?>

                                                                     value="ups_output_room_b"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> C <input type="checkbox" name="ups_output_room_c"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->ups_output_room_c == "ups_output_room_c") ? "checked" : ''; ?>

                                                                     value="ups_output_room_c"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> D <input type="checkbox" name="ups_output_room_d"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->ups_output_room_d == "ups_output_room_d") ? "checked" : ''; ?>

                                                                     value="ups_output_room_d"></label>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="check-label"> Control Room <input type="checkbox"
                                                                                class="zone_access_checkbox"
                                                                                name="control_room"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->control_room == "control_room") ? "checked" : ''; ?>

                                                                                value="control_room"></label>
                            </div>
                        </div>
                        <div class="clearifix"></div>

                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Main Switch Room </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> A <input type="checkbox" name="main_switch_room_a"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->main_switch_room_a == "main_switch_room_a") ? "checked" : ''; ?>

                                                                     value="main_switch_room_a"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> B <input type="checkbox" name="main_switch_room_b"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->main_switch_room_b == "main_switch_room_b") ? "checked" : ''; ?>

                                                                     value="main_switch_room_b"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> C <input type="checkbox" name="main_switch_room_c"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->main_switch_room_c == "main_switch_room_c") ? "checked" : ''; ?>

                                                                     value="main_switch_room_c"></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> D <input type="checkbox" name="main_switch_room_d"
                                                                     class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->main_switch_room_d == "main_switch_room_d") ? "checked" : ''; ?>
                                                                     value="main_switch_room_d"></label>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>


                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="check-label"> Preaction Room <input type="checkbox"
                                                                                  class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->preaction_room == "preaction_room") ? "checked" : ''; ?>

                                                                                  name="preaction_room"
                                                                                  value="preaction_room"></label>
                            </div>
                        </div>
                        <div class="clearifix"></div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> Fuel Main Tank Area <input type="checkbox"
                                                                                       class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->fuel_main_tank_area == "fuel_main_tank_area") ? "checked" : ''; ?>

                                                                                       name="fuel_main_tank_area"
                                                                                       value="fuel_main_tank_area">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> Fuel Pump Room <input type="checkbox"
                                                                                  class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->fuel_pump_room == "fuel_pump_room") ? "checked" : ''; ?>

                                                                                  name="fuel_pump_room"
                                                                                  value="fuel_pump_room"></label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> AHU Room <input type="checkbox" name="ahu_room"
                                                                            class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->ahu_room == "ahu_room") ? "checked" : ''; ?>

                                                                            value="ahu_room"></label>
                            </div>
                        </div>

                        <div class="clearifix"></div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> Chilled Water Pump Room <input type="checkbox"
                                                                                           class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->chilled_water_pump_room == "chilled_water_pump_room") ? "checked" : ''; ?>

                                                                                           name="chilled_water_pump_room"
                                                                                           value="chilled_water_pump_room">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> Chilled Area Backyard <input type="checkbox"
                                                                                         class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->chilled_area_backyard == "chilled_area_backyard") ? "checked" : ''; ?>

                                                                                         name="chilled_area_backyard"
                                                                                         value="chilled_area_backyard"></label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> External Area <input type="checkbox"
                                                                                 class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->external_area == "external_area") ? "checked" : ''; ?>

                                                                                 name="external_area"
                                                                                 value="external_area"></label>
                            </div>
                        </div>

                        <div class="clearifix"></div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> Generator Room <input type="checkbox"
                                                                                  class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->generator_room == "generator_room") ? "checked" : ''; ?>

                                                                                  name="generator_room"
                                                                                  value="generator_room">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> First Floor Corridor <input type="checkbox"
                                                                                        class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->first_floor_corridor == "first_floor_corridor") ? "checked" : ''; ?>

                                                                                        name="first_floor_corridor"
                                                                                        value="first_floor_corridor"></label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="check-label"> Ground Floor Corridor <input type="checkbox"
                                                                                         class="zone_access_checkbox"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->ground_floor_corridor == "ground_floor_corridor") ? "checked" : ''; ?>

                                                                                         name="ground_floor_corridor"
                                                                                         value="ground_floor_corridor"></label>
                            </div>
                        </div>

                        <div class="clearifix"></div>

                    </div>
                </div>

                <!--                    End    preapprove section-->
                <div class="material_div business_div">
                    <div class="form-section">
                        <span>Material Movement Details</span>
                    </div>


                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Material </label>
                            </div>
                        </div>

                        <?php if (isset($request_data) && count($request_data) > 0) { ?>
                        <div class="col-sm-2">
                            <div class="form-group">

                            <input type="button" class="btn btn-info orangebackground pull-right" value="Print Material In"
                                   id="printMaterialInbtn"/>
                            </div>
                        </div>

                            <div class="col-sm-2">
                                <div class="form-group">

                                    <input type="button" class="btn btn-info orangebackground pull-right" value="Print Material Out"
                                           id="printMaterialOutbtn"/>
                                </div>
                            </div>

                        <?php }else{ ?>


                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> IN <input type="checkbox" name="material_in"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->material_in == "material_in") ? "checked" : ''; ?>

                                                                      value="material_in"
                                                                      class="check_material_in material_checkin_out">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="check-label"> OUT <input type="checkbox" name="material_out"
                                        <?= (isset($request_data) && $request_data <> null && $request_data->material_out == "material_out") ? "checked" : ''; ?>

                                                                       value="material_out"
                                                                       class="check_material_out material_checkin_out">
                                </label>

                            </div>
                        </div>
                        <?php } ?>
                    </div>


                    <!--  <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="check-label"> Material </label>
                            </div>
                        </div>


                    <?php /*if((isset($request_data->material_in) && $request_data->material_in =='material_in')){*/ ?>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input type="button" class="btn btn-info orangebackground" value="Print Material In"
                                       id="printMaterialInbtn"/>
                            </div>
                        </div>


                    <?php /*} */ ?>

                        <?php /*if((isset($request_data->material_out) && $request_data->material_out =='material_out')){*/ ?>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <input type="button" class="btn btn-info orangebackground" value="Print Material Out"
                                           id="printMaterialOutbtn"/>
                                </div>
                            </div>


                        <?php /*} */ ?>

                    <?php /*} */ ?>

                </div>-->


                <div class="form-section business_div">
                    <span>Accompanying Visitors Detail</span>
                </div>
                <div class="row business_div" id="visitor_fields">

                    <?php
                    if ((isset($request_data) && $request_data <> null)) {
                        $visitors = getDataByColumn($table = 'visiters', $col = 'request_id', $request_data->id);
                        if (count($visitors) > 0) {
                            $total = count($visitors);
                            $i = 1;
                            foreach ($visitors as $v) {

                                ?>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Visiter Name</label>
                                        <input class="form-control" name="visiter_name[]" data-error="Enter Name Please"
                                               placeholder="XYZ Visitor" required="required" value="<?= $v->name; ?>"
                                               type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                        <input type="hidden" name="vistor_id[]" value="<?= $v->id; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Company Name</label>

                                        <input class="form-control" name="visiter_company[]" data-error="Company Name"
                                               placeholder="Company Name" required="required"
                                               value="<?= $v->company; ?>" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Details</label>
                                        <div class="input-group">
                                            <input class="form-control" name="visiter_phone[]" value="<?= $v->phone; ?>"
                                                   placeholder="052 1234567"
                                                   required="required"
                                                   type="text">
                                            <?php
                                            if ($total == $i) { ?>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-success" id="addScnt" type="button"><i
                                                                class="fa fa-plus"></i></button>
                                                </div> <?php
                                            } else {
                                                ?>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger" type="button" id="remScnt"><i
                                                                class="fa fa-times"></i></button>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>

                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>


                                </div>
                                <?php
                                $i++;
                            }
                        }
                    } else { ?>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Visiter Name</label>

                                <input class="form-control" name="visiter_name[]" data-error="Enter Name Please"
                                       placeholder="XYZ Visitor" required="required" type="text">
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Company Name</label>

                                <input class="form-control" name="visiter_company[]" data-error="Company Name"
                                       placeholder="Company Name" required="required" type="text">
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Contact Details</label>
                                <div class="input-group">
                                    <input class="form-control" name="visiter_phone[]" placeholder="052 1234567"
                                           required="required"
                                           type="text">

                                    <div class="input-group-btn">
                                        <button class="btn btn-success" id="addScnt" type="button"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>

                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>


                        </div>
                        <?php
                    }
                    ?>

                </div>

                <div class="requester_acknowledge">
                    <div class="form-section">
                        <span>Requester Acknowledgement</span>
                    </div>
                    <div class="row">
                        <div class="bullets col-sm-12">

                            <ul>
                                <li>I fully understand and will abide by all policies and procedures as
                                    described in the GDH Data Center Policies and Procedures
                                    document.
                                </li>
                                <li>I fully understand and agree to these rules and that the facility is
                                    monitored by CCTV. I also agree to provide my full
                                    cooperation during any investigation concerning a security matter, which
                                    might have occurred in the Data Center during a
                                    time when my presence in the facility has been recorded.
                                </li>
                                <li>GDH reserves their right to terminate the access of the Data Center for its
                                    personnel, clients, subcontractors and visitors
                                    found violating these rules and in case of emergency
                                </li>
                            </ul>


                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Sinature</label>
                                <div class="input-group">
                                    <div class="btn-group radio-group">
                                        <label class="btn btn-primary ">Accept <input type="radio"
                                                                                      value="approved"
                                                                                      name="requester_signature"
                                                                                      checked required></label>
                                        <label class="btn btn-primary bluebackground">Reject <input type="radio"
                                                                                                    value="reject"
                                                                                                    name="requester_signature"
                                                                                                    required></label>
                                    </div>
                                </div>
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date</label>
                                <input class="single-daterange  form-control" data-error="Enter date first"
                                       name="requester_signature_date"
                                       placeholder="12-03-2017" value="" required="required" type="text"
                                       value="<?= (isset($request_data) && $request_data <> null) ? $request_data->requester_signature_date : ''; ?>">
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                        </div>
                    </div>
                </div>


                    <?php if ($this->session->userdata['active_user']->group_id != 3) { ?>

                        <div class="form-section">
                            <span> Manager / Pre Approved Managers Details</span>
                        </div>

                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" data-error="Your email address is invalid"
                                   placeholder="ABC User" name="manager_name" required="required" type="text"
                                   value="<?= (isset($request_data) && $request_data <> null) ? $request_data->manager_name : $this->session->userdata['active_user']->name; ?>">
                            <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="abc@gmail.com" name="manager_email" required="required" readonly
                                           type="email"
                                           value=" <?= (isset($request_data) && $request_data <> null) ? $request_data->manager_email : $this->session->userdata['active_user']->email; ?> ">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" name="manager_phone" placeholder="009711234567"
                                           value=" <?= (isset($request_data) && $request_data <> null) ? $request_data->manager_phone : $this->session->userdata['active_user']->phone; ?> "
                                           required="required"
                                           type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input class="form-control" name="manager_designation"
                                           data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           value="<?= (isset($request_data) && $request_data <> null) ? $request_data->manager_designation : $this->session->userdata['active_user']->designation; ?>"
                                           name="requester_designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" name="manager_company" placeholder="Wisdom IT Solution"
                                           required="required" type="text" name="manager_company"
                                           value="<?= (isset($request_data) && $request_data <> null) ? $request_data->manager_company : '' ?>">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Signature</label>
                                    <div class="input-group">
                                        <div class="btn-group radio-group mangerradio">
                                            <label class="btn btn-primary <?php if (isset($request_data) && $request_data->manager_signature == "Reject") {
                                                echo 'not-active';
                                            } else if (isset($request_data->manager_signature) && $request_data->manager_signature == "") {
                                                echo 'not-active';
                                            } ?> ">Approve <input type="radio" value="Approved"
                                                                  name="manager_signature" <?php if (isset($request_data) && $request_data->manager_signature == "Approved") {
                                                    echo 'checked';
                                                } ?> required></label>
                                            <label class="btn btn-primary <?php if (isset ($request_data) && $request_data->manager_signature == "Approved") {
                                                echo 'not-active';
                                            } else if (isset($request_data) && $request_data->manager_signature == "") {
                                                echo 'not-active';
                                            } ?>">Reject <input type="radio" value="Reject"
                                                                name="manager_signature" <?php if (isset($request_data) && $request_data->manager_signature == "Reject") {
                                                    echo 'checked';
                                                } ?> required></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input class=" form-control" name="manager_signature_date" placeholder=""
                                           type="text"
                                           value="<?php if ($request_data && $request_data->manager_signature_date != "") {
                                               echo $request_data->manager_signature_date;
                                           } else {
                                               echo date('d/m/Y h:i');
                                           } ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6 <?php if (isset($request_data) && $request_data->manager_signature == "Reject") {
                            } else {
                                echo 'hidden';
                            } ?>" id="manager_reject_reason">
                                <div class="form-group">
                                    <label>Reason</label>
                                    <input class=" form-control" name="manager_reject_reason" placeholder=""
                                           type="text"
                                           value="<?php if (isset($request_data) && $request_data->manager_reject_reason != "") {
                                               echo $request_data->manager_reject_reason;
                                           } ?>">
                                </div>
                            </div>


                        </div>


                        <?php


                        if ($this->session->userdata['active_user']->group_id == 1 || $this->session->userdata['active_user']->group_id == 5) { ?>

                            <div class="form-section">
                                <span> For Data Centre Operations Team</span>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Access Number</label>
                                        <input class="form-control" name="access_number" data-error="Your Access Number"
                                               placeholder="AERT-711-34567" required="required" value="AERT-711-34567"
                                               type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row paddingtop8">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Esscort Service </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label ">Optional <input type="checkbox"
                                                                                    name="esscort_service"
                                                                                    value="optional" <?php if ($request_data->team_signature == "optional") {
                                                echo 'checked';
                                            } ?> ></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Mandatory <input type="checkbox"
                                                                                    name="esscort_service"
                                                                                    value="mandatory" <?php if ($request_data->esscort_service == "mandatory") {
                                                echo 'checked';
                                            } ?> ></label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                            <div class="row paddingtop8">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Approver Type</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label ">Visitor <input type="checkbox" name="appprover_type"
                                                                                   value="visitor" <?php if ($request_data->appprover_type == "visitor") {
                                                echo 'checked';
                                            } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Contracter <input type="checkbox"
                                                                                     name="appprover_type"
                                                                                     value="contracter" <?php if ($request_data->appprover_type == "contracter") {
                                                echo 'checked';
                                            } ?>></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label">Customer <input type="checkbox" name="appprover_type"
                                                                                   value="customer" <?php if ($request_data->appprover_type == "customer") {
                                                echo 'checked';
                                            } ?>></label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>


                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <div class="input-group">
                                            <div class="btn-group radio-group teamradio">

                                                <label class="btn btn-primary  <?php if ($request_data->team_signature == "Reject") {
                                                    echo 'not-active';
                                                } else if ($request_data->team_signature == "") {
                                                    echo 'not-active';
                                                } ?> ">Approve <input type="radio" value="Approved"
                                                                      name="team_signature" <?php if ($request_data->team_signature == "Approved") {
                                                        echo 'checked';
                                                    } ?> required></label>
                                                <label class="btn btn-primary  <?php if ($request_data->team_signature == "Approved") {
                                                    echo 'not-active';
                                                } else if ($request_data->team_signature == "") {
                                                    echo 'not-active';
                                                } ?>">Reject <input type="radio" value="Reject"
                                                                    name="team_signature" <?php if ($request_data->team_signature == "Reject") {
                                                        echo 'checked';
                                                    } ?> required></label>

                                            </div>
                                        </div>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control " placeholder="Date of birth"
                                               name="team_signature_date"
                                               type="text"
                                               value="<?php if (isset($request_data) && $request_data->team_signature_date != "0000-00-00") {
                                                   echo $request_data->team_signature_date;
                                               } else {
                                                   echo date('d/m/Y h:i');
                                               } ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6 <?php if ($request_data->team_signature == "Reject") {
                                } else {
                                    echo 'hidden';
                                } ?>" id="datateam_reject_reason">
                                    <div class="form-group">
                                        <label>Reason</label>
                                        <input class=" form-control" name="datateam_reject_reason" placeholder=""
                                               type="text"
                                               value="<?php if (isset($request_data->datateam_reject_reason)) {
                                                   echo $request_data->datateam_reject_reason;
                                               } ?>">
                                    </div>
                                </div>


                            </div>
                        <?php } ?>

                    <?php } ?>

                <div class="content-box-footer">
                    <input type="button" id="saveAccessRequestForm"
                           class="saveAccessRequestForm btn btn-info bluebackground" value="Save">
                    <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back
                        To Requests
                    </a>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>


    <script type="text/javascript">


        $(function () {

            // $.ajax({async: false});

            $('body').on('click', '.saveAccessRequestForm', function (e) {


                var Valid = 1;

                    e.preventDefault();

                $.ajax({
                        url: "/accessRequests/validateForms/",
                        type: "post",
                        data: {'validate': 'data'},
                        success: function (data) {

                            var data = JSON.parse(data);


                            if (!data.work_permit && $('#workoutform').is(':checked')) {

                                Valid = 0;

                                swal({
                                    title: "Warning",
                                    text: "Please Fill work permit form first",
                                    type: "error",
                                    confirmButtonText: "Open Form"
                                }, function (e) {

                                    $('#workoutModel').appendTo("body").modal('show');

                                });

                                return false;
                            }

                            if (!data.material_in && $('.check_material_in').is(':checked')) {

                                Valid = 0;

                                swal({
                                    title: "Warning",
                                    text: "Please Fill Material In form first",
                                    type: "error",
                                    confirmButtonText: "Open Form"
                                }, function (e) {
                                    $('#matIn').appendTo("body").modal('show');

                                });

                                return false;
                            }

                            if (!data.material_out && $('.check_material_out').is(':checked')) {

                                Valid = 0;

                                swal({
                                    title: "Warning",
                                    text: "Please Fill Material Out form first",
                                    type: "error",
                                    confirmButtonText: "Open Form"
                                }, function (e) {
                                    $('#matOut').appendTo("body").modal('show');

                                });

                                return false;
                            }

                            if (jQuery('input[type=checkbox].business_requirements:checked').length <= 0) {

                                Valid = 0;

                                swal({
                                    title: "Warning",
                                    text: "Please select one option from business requests",
                                    type: "error",
                                    confirmButtonText: "OK"
                                });

                                return false;
                            }


                            if (jQuery('input[type=checkbox].zone_access_checkbox:checked').length <= 0) {

                                Valid = 0;

                                swal({
                                    title: "Warning",
                                    text: "Please select one option from zone access",
                                    type: "error",
                                    confirmButtonText: "OK"
                                });

                                return false;
                            }


                            if (Valid) {

                                $('form.accessRequestForm').submit();

                                /*var form = $(this);
                                var formData = form.serialize();

                                $.ajax({

                                    url: form.attr("action"),
                                    type: "POST",
                                    data: formData,
                                    success: function (data) {
                                        console.log(data);
                                        swal({
                                            title: "Sccuess",
                                            text: "Request access submitted successfully!",
                                            type: "success",
                                            confirmButtonText: "Close"
                                        });
                                    },
                                    error: function (e) {
                                        $("#SaveInfoMaterial").attr('disabled', false);

                                    }
                                });*/
                            }


                        },
                    error: function (e) {
                        $("#SaveInfoMaterial").attr('disabled', false);
                        return false;
                    }
                });
            });


            $("#btnPrintmainForm").on('click', function () {
                $.print("#printable");
            });

            $("#printMaterialInbtn").on('click', function () {
                $.print("#printMaterialIn");
            });
            $("#printMaterialOutbtn").on('click', function () {
                $.print("#printMaterialOut");
            });

            $("#printWorkPermitbtn").on('click', function () {
                $.print("#printWorkPermit");
            });


            // Date picker
            if ($('input.single-daterange-sig').length) {
                $('input.single-daterange-sig').daterangepicker({
                    "singleDatePicker": true,
                    locale: {
                        format: 'DD/MM/YYYY hh:mm'
                    }
                })

            }



            //###########################
            // Datepicker and Data Range
            /* $('body').find('.start_date').daterangepicker({
                     singleDatePicker: true,
                     showDropdowns: true,
                     "timePicker": true,
                     "timePicker24Hour": true,
                     locale: {
                         format: 'MM/DD/YYYY hh:mm'
                     }
                 }
             );

             $('body').find('.end_date').daterangepicker({
                     singleDatePicker: true,
                     showDropdowns: true,
                     "timePicker": true,
                     "timePicker24Hour": true,
                     locale: {
                         format: 'MM/DD/YYYY hh:mm'
                     }
                 }
             );*/


            $('#start_end_date').daterangepicker({
                "showDropdowns": true,
                "showISOWeekNumbers": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "timePickerIncrement": 5,
                /*"dateLimit": {
                    "days": 7
                },*/
                "locale": {
                    format: 'MM/DD/YYYY hh:mm',
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "fromLabel": "From",
                    "toLabel": "To",
                    "customRangeLabel": "Custom",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                },
                "showCustomRangeLabel": false,
                "alwaysShowCalendars": true,
                "parentEl": "body",
            });
            //###########################
        });


        //material check boxes
        $('.business_requirements').click(function () {
            $('.business_requirements').not(this).prop('checked', false);
        });


        // Input radio-group visual controls
        $('.mangerradio label').on('click', function () {

            $(this).removeClass('not-active').siblings().addClass('not-active');

        });
        $('.radio-group label').on('click', function () {

            $(this).removeClass('not-active').siblings().addClass('not-active');

        });

        $('input[type=radio][name=manager_signature]').change(function () {

            if ($(this).val() == 'Reject') {
                $('#manager_reject_reason').show(500);
            } else {
                $('#manager_reject_reason').hide(500);
            }
        });
        $('input[type=radio][name=team_signature]').change(function () {

            if ($(this).val() == 'Reject') {
                $('#datateam_reject_reason').show(500);
            } else {
                $('#datateam_reject_reason').hide(500);
            }
        });

        $('#myModal').appendTo("body").modal('show');

        $('#workform').on('click', function () {
            $('#myModal').modal('show');

        });

        //###########################
        //material check boxes
        $('#workoutform').click(function () {
            if (this.checked) {
                $('#workoutModel').appendTo("body").modal('show');
            } else {
                $('#workoutModel').hide(500);
            }
        });

        // Mariral addmore
        $(".check_material_in").change(function () {
            if (this.checked) {
                $('#matIn').appendTo("body").modal('show');
            }
            else {
                $('#matIn').hide(500);
            }
        });

        $(".check_material_out").change(function () {
            if (this.checked) {
                $('#matOut').appendTo("body").modal('show');
            } else {
                $('#matOut').hide(500);
            }
        });

        $("#businessreqs_other_check").change(function () {
            if (this.checked) {

                var html = '<div class="">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label style=" display: inline;"> Other Contracted Work </label>\n' +
                    '                                <input style=" display: inline; width: 65%" class="form-control" value=" " data-error="Your email address is invalid" placeholder="Any Purpose or Reason" name="business_requirements_other" type="text">\n' +
                    '                            </div>\n' +
                    '                        </div>';


                $('#business_reqs_other').html(html);
            } else {
                $('#business_reqs_other').html('');
            }
        });


        $(function () {

            //workers div
            var workDiv = $('#addWorkersDiv');
            var html_work_fiels = '';
            html_work_fiels += '<div class="row addWorker">';
            html_work_fiels += '    <div class="col-sm-12">';
            html_work_fiels += '        <div class="form-group">';
            html_work_fiels += '        <label>Name</label>';
            html_work_fiels += '        <div class="input-group">';
            html_work_fiels += '                <input class="form-control" placeholder="Name" required="required" type="text">';
            html_work_fiels += '                <div class="input-group-btn">';
            html_work_fiels += '                    <button  class="btn btn-danger" id="remWorker" type="button"><i class="fa fa-times"></i></button>';
            html_work_fiels += '                </div>';
            html_work_fiels += '            </div>';
            html_work_fiels += '            <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_work_fiels += '        </div>';
            html_work_fiels += '    </div>';
            html_work_fiels += '</div>';


            $('#addworker').on('click', function () {

                $(html_work_fiels).insertAfter(workDiv);
                $('#remWorker').on('click', function () {
                    $(this).parents('.addWorker').remove();
                    return false;
                });

                return false;
            });

            //visitors form add more
            var mattDiv = $('#material_fields');
            var mattDiv1 = $('#material_fields1');

            var html_matrial_fiels = '';

            html_matrial_fiels += '<div class="row materialitems addmaterial">';
            html_matrial_fiels += '     <div class="col-sm-3">';
            html_matrial_fiels += '         <input class="form-control" placeholder="Item Description" required="required" name="Itmes[material_item][]" type="text">';
            html_matrial_fiels += '         <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-2">';
            html_matrial_fiels += '         <input class="form-control" placeholder="Qunatity" name="Itmes[material_quntity][]" required="required" type="text">';
            html_matrial_fiels += '         <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-2">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <input class="form-control" placeholder="Serial Number" type="text" name="Itmes[material_serial][]" required="required">';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback" ></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-2">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <input class="form-control" placeholder=" Model" type="text" name="Itmes[material_serial][]" required="required">';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-3">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <input class="form-control" placeholder="Power" type="text"  name="Itmes[material_power_capacity][]" required="required">';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-3">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <input class="form-control" placeholder="Heat Dissipation" type="text" name="Itmes[material_power_heat][]">';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-3">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <input class="form-control" placeholder="Storage" type="text" name="Itmes[material_power_storage][]">';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-3">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <input class="form-control" placeholder=" Comliance check name" type="text" name="Itmes[material_power_check_name][]">';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '     <div class="col-sm-3">';
            html_matrial_fiels += '         <div class="form-group">';
            html_matrial_fiels += '             <div class="input-group">';
            html_matrial_fiels += '                 <input class="form-control" placeholder="Status" type="text" name="Itmes[material_status][]>';
            html_matrial_fiels += '                 <div class="input-group-btn">';
            html_matrial_fiels += '                     <button class="btn btn-danger remMat"  id="remMat" type="button"><i class="fa fa-times"></i></button>';
            html_matrial_fiels += '                 </div>';
            html_matrial_fiels += '             </div>';
            html_matrial_fiels += '             <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '         </div>';
            html_matrial_fiels += '     </div>';
            html_matrial_fiels += '</div>';


            $('#addMat').on('click', function () {

                $(html_matrial_fiels).insertBefore(mattDiv);
                $('materialitems').find('.remMat').on('click', function () {
                    $(this).parents('.addmaterial').remove();
                    return false;
                });

                return false;
            });

            $('#addMat1').on('click', function () {
                $(html_matrial_fiels).insertBefore(mattDiv1);
                $('materialitems').find('.remMat').on('click', function () {
                    $(this).parents('.addmaterial').remove();
                    return false;
                });

                return false;
            });


            //visitors form add more
            var scntDiv = $('#visitor_fields');

            var html_vistor = '';
            html_vistor += '<div class="row addvisitor">';
            html_vistor += '    <div class="col-sm-4">';
            html_vistor += '        <div class="form-group">';
            html_vistor += '            <label>Visotor Name</label>';
            html_vistor += '            <input class="form-control" data-error="" placeholder="XYZ Visitor" name="visiter_name[]" required="required" type="text">';
            html_vistor += '            <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '        </div>';
            html_vistor += '    </div>';
            html_vistor += '    <div class="col-sm-4">';
            html_vistor += '        <div class="form-group">';
            html_vistor += '            <label>Company Name</label>';
            html_vistor += '            <input class="form-control" data-error="" placeholder="Wisdom IT"  name="visiter_company[]" required="required"  name="" type="text">';
            html_vistor += '            <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '        </div>';
            html_vistor += '    </div>';
            html_vistor += '    <div class="col-sm-4">';
            html_vistor += '        <div class="form-group">';
            html_vistor += '            <label>Contact Details</label>';
            html_vistor += '            <div class="input-group">';
            html_vistor += '            <input class="form-control" placeholder="052 1234567" required="required"  name="visiter_phone[]"  name="" type="text">';
            html_vistor += '            <div class="input-group-btn">';
            html_vistor += '                <button class="btn btn-danger" type="button" id="remScnt"> <i class="fa fa-times"></i> </button>';
            html_vistor += '        </div>';
            html_vistor += '    </div>';
            html_vistor += '    <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '    </div>';
            html_vistor += '    </div>';
            html_vistor += '</div>';


            $('#addScnt').on('click', function () {
                $(html_vistor).insertBefore(scntDiv);

                $('#remScnt').on('click', function () {
                    $(this).parents('.addvisitor').remove();
                    return false;
                });

                return false;
            });

            //accesstype checkboxes

            //material check boxes
            $('.check_access_type').click(function () {
                $('.check_access_type').not(this).prop('checked', false);
            });
            //material check boxes
            $('.check_material').click(function () {
                $('.check_material').not(this).prop('checked', false);
            });
            // Mariral addmore


            $('#addmatrial').on('click', function () {
                $(html_material).insertAfter(matDiv);

                $('#remMatrial').on('click', function () {
                    $(this).parents('.materials').remove();
                    return false;
                });

                return false;
            });

            $('#remMatrial').on('click', function () {
                $(this).parents('.materials').remove();
                return false;
            });

        });


    </script>

<?php
if (!isset($work_permit)) {
    $work_permit = [];
}
if (!isset($material_in_data)) {
    $material_in_data = [];
}
if (!isset($material_out_data)) {
    $material_out_data = [];
}
?>
<?php $this->load->view('requests/workPermitForm', ['work_permit' => $work_permit]); ?>

    <?php $this->load->view('requests/material_in_form', ['material_in_data' => $material_in_data]); ?>

    <?php $this->load->view('requests/material_out_form', ['material_out_data' => $material_out_data]); ?>

    <!--<div id="printable">-->

    <!--</div>-->

