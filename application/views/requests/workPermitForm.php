<?php

if (isset($work_permit) && count($work_permit) > 0) {
    $work_permit = $work_permit[0];
} else {
    $work_permit = null;
}
?>
<div class="modal fade" id="workoutModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title heading3">Work out Form</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="" data-validation="required" class="work_permit_form" method="post"
                  action="/accessRequests/submitRequestInOutForm">
                <div class="modal-body">
                    <div class="row" id="printWorkPermit">
                        <div class="col-md-12">
                            <div class="content-box">
                                <div class="form-section-head">
                                    <span>Work Permit</span>
                                </div>
                                <!--                        requester information-->

                                <div class="form-section">
                                    <span>Requester Information</span>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Name <span class="required">*</span> </label>
                                            <input class="form-control" data-error="Enter your name first"
                                                   placeholder="Name" required="required" type="text" name="name"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->name : ''; ?>"
                                            />
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <input class="form-control" id="work_permit" name="type"
                                               aria-required="true" type="hidden"
                                               value="work_permit"/>

                                        <?php
                                        if (isset($work_permit) && $work_permit <> null) {
                                            ?>
                                            <input type="hidden" value="<?= $work_permit->id; ?>" name="id">
                                            <?php
                                        }
                                        ?>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Contact Detail <span class="required">*</span> </label>
                                            <input class="form-control" placeholder="009711234567"
                                                   aria-required="true"
                                                   required="required" type="text" name="contact_detail"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->contact_detail : ''; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Designation <span class="required">*</span> </label>
                                        <div class="form-group">
                                            <input class="form-control" data-error="Enter Designation"
                                                   aria-required="true"
                                                   placeholder="Manager" required="required" type="text"
                                                   name="designation"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->designation : ''; ?>">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Company Name <span class="required">*</span> </label>
                                            <input class="form-control" placeholder="ABC Company"
                                                   aria-required="true"
                                                   required="required"
                                                   type="text" name="company"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->company : ''; ?>">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="check-label"> Permit Required for? <span
                                                        class="required">*</span> </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clreafix"></div>
                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox" name="permit_require_for"
                                                       aria-required="true"
                                                       value="mobilization_only"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->permit_require_for == "mobilization_only") ? "checked" : ''; ?>

                                                       class="business_requirements"/> Mobilization Only
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       aria-required="true"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->permit_require_for == "fit_out_work") ? "checked" : ''; ?>

                                                       name="permit_require_for"
                                                       value="fit_out_work"
                                                       class="business_requirements"/> Fit-Out Work
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       aria-required="true"
                                                       name="permit_require_for"
                                                       value="fm_services"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->permit_require_for == "fm_services") ? "checked" : ''; ?>

                                                       class="business_requirements"/> FM Services
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       aria-required="true"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->permit_require_for == "minor_work") ? "checked" : ''; ?>

                                                       name="permit_require_for"
                                                       value="minor_work"
                                                       class="business_requirements"/> Minor Work
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       aria-required="true"
                                                       name="permit_require_for"
                                                       value="other_contracted_work"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->permit_require_for == "other_contracted_work") ? "checked" : ''; ?>

                                                       class="business_requirements"/> Other Contracted Work
                                            </label>
                                        </div>
                                    </div>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Permit Validity Date</label>
                                            <input class="single-daterange form-control" placeholder="Start Date"
                                                   aria-required="true"
                                                   type="text" name="start_date"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->start_date : ''; ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Permit Validity Time</label>
                                            <input class="timepicker form-control" placeholder="Start Time"
                                                   aria-required="true"
                                                   type="text" name="start_time"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->start_time : '01:00 pm'; ?>">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Permit Expiry Date</label>
                                            <input class="single-daterange form-control" placeholder="Expiry Date"
                                                   aria-required="true"
                                                   type="text" name="end_date" value="04-12-2017"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->end_date : ''; ?>">

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Permit Expiry Time</label>
                                            <input class="timepicker form-control" placeholder="Expiry Time"
                                                   aria-required="true"
                                                   type="text" name="end_time"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->end_time : '01:00 pm'; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Location of work <span class="required">*</span> </label>
                                        <div class="form-group">
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="According to request form" required="required"
                                                   type="text"
                                                   name="location"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->location : ''; ?>">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Access Reference# <span class="required">*</span> </label>
                                            <input class="form-control" placeholder="Access Reference#"
                                                   required="required"
                                                   type="text" name="access_number"
                                                   value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->access_number : ''; ?>">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-section">
                                    <span>Name of team workers to carry the work</span>
                                </div>
                                <div class="row" id="workers">
                                    <div class="col-sm-12">
                                        <div id="addWorkersDiv"></div>
                                        <?php
                                        $required = 'required="required"';
                                        if (isset($work_permit) && $work_permit <> null) {
                                            $workers = getDataByColumn('work_permit_workers', 'work_permit_id', $work_permit->id);
                                            if (count($workers) > 0) {
                                                $required = '';
                                                $total = count($workers);
                                                $i = 1;
                                                foreach ($workers as $worker) {
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <div class="input-group">

                                                            <input class="form-control" placeholder="Name"
                                                                   required="required"
                                                                   type="text" name="worker_name[]"
                                                                   value="<?= $worker->worker_name; ?>"/>

                                                            <?php
                                                            if ($total == $i) {
                                                                ?>
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-success" id="addworker"
                                                                            type="button"><i
                                                                                class="fa fa-plus"></i></button>
                                                                </div>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-danger" id="remWorker"
                                                                            type="button"><i class="fa fa-times"></i>
                                                                    </button>
                                                                </div>
                                                                <?php
                                                            } ?>
                                                        </div>
                                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                        } else {
                                            ?>
                                            <div class="form-group">
                                                <label>Name <span class="required">*</span> </label>
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="Name" <?= $required; ?>
                                                           type="text" name="worker_name[]">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-success" id="addworker" type="button"><i
                                                                    class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-section">
                                    <span>Type of work to be carried out  <span class="required">*</span> </span>
                                </div>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="planned_power_hazzard"
                                                        value="planned_power_hazzard"
                                                        class="type_of_work"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->planned_power_hazzard == "planned_power_hazzard") ? "checked" : ''; ?>

                                                /> Planned Power Hazzard/Outage </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="interruption_of_exsisting_services"
                                                        value="interruption_of_exsisting_services"
                                                        class=" type_of_work"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->interruption_of_exsisting_services == "interruption_of_exsisting_services") ? "checked" : ''; ?>

                                                > Interruption of Exsisting Services</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="contact_with_electrical_supply"
                                                        value="contact_with_electrical_supply"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->contact_with_electrical_supply == "contact_with_electrical_supply") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Any Contact with Electrical Supply
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="installation_of_racks"
                                                        value="installation_of_racks"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->installation_of_racks == "installation_of_racks") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Installation of Racks or Devices</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="removal_of_floor_tiles"
                                                        value="removal_of_floor_tiles"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->removal_of_floor_tiles == "removal_of_floor_tiles") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Lifting/Removal of Floor Tiles</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="welding_grinding"
                                                        value="welding_grinding"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->welding_grinding == "welding_grinding") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Welding,Grinding,Drilling or Cutting
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="cable_pulling_and_termination"
                                                        value="cable_pulling_and_termination"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->cable_pulling_and_termination == "cable_pulling_and_termination") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Cable Pulling and Termination</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input type="checkbox"
                                                                               name="material_movement"
                                                                               value="material_movement"
                                                                               class=" type_of_work"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->planned_power_hazzard == "planned_power_hazzard") ? "checked" : ''; ?>

                                                > Material Movement</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="installation_or_modifications_of_cage"
                                                        value="installation_or_modifications_of_cage"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->installation_or_modifications_of_cage == "installation_or_modifications_of_cage") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Installation or Modifications of Cage
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input type="checkbox"
                                                                               name="waste_management"
                                                                               value="waste_management"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->waste_management == "waste_management") ? "checked" : ''; ?>

                                                                               class=" type_of_work"> Waste Management
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="excavation_or_civil_work"
                                                        value="excavation_or_civil_work"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->excavation_or_civil_work == "excavation_or_civil_work") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Excavation or Civil Work</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="planned_cooling_system_azzard"
                                                        value="planned_cooling_system_azzard"
                                                    <?= (isset($work_permit) && $work_permit <> null && $work_permit->planned_cooling_system_azzard == "planned_cooling_system_azzard") ? "checked" : ''; ?>

                                                        class=" type_of_work"> Planned Cooling system Hazzard/Outage
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="check-label"> <input
                                                        type="checkbox"
                                                        name="other_special_reqs"
                                                        id="other_special_reqs" class=" type_of_work"> Other</label>
                                        </div>
                                    </div>


                                    <div class="col-sm-6" id="other_special_reqs_div">

                                    </div>

                                    <div class="help-block form-text with-errors form-control-feedback"></div>

                                </div>

                                <div class="form-section">
                                    <span>Detail of tested tool and material required</span>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                                <textarea rows="4" class="fullwidth" name="detail_of_tested_tool"
                                                          placeholder=""
                                                          required="required"><?= (isset($work_permit) && $work_permit <> null) ? $work_permit->detail_of_tested_tool : ''; ?></textarea>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-section">

                                    <span>Method of Statement for work to be carried Out</span>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                                <textarea rows="20" class="fullwidth" name="method_of_statement"
                                                          placeholder=""
                                                          required="required"><?= (isset($work_permit) && $work_permit <> null) ? $work_permit->method_of_statement : ''; ?></textarea>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-section">

                                    <span>Risk Assessment for Work to be carried Out</span>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                                <textarea rows="20" class="fullwidth" name="risk_assessment"
                                                          placeholder=""
                                                          required="required"><?= (isset($work_permit) && $work_permit <> null) ? $work_permit->risk_assessment : ''; ?></textarea>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-section">

                                    <span>Special Requirements and Use of Safety Equipment</span>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       name="special_requirement_head"
                                                       class=" special_requirements"><img
                                                        src="<?= base_url() . 'assets/plugins/icons/head.png'; ?>"
                                                        width="20"> Head
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       name="special_requirement_eye"
                                                       class=" special_requirements"><img
                                                        src="<?= base_url() . 'assets/plugins/icons/eye.png'; ?>"
                                                        width="20"> Eye</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="check-label"> <input type="checkbox"
                                                                               name="special_requirement_hand"
                                                                               class=" special_requirements"><img
                                                        src="<?= base_url() . 'assets/plugins/icons/hands.png'; ?>"
                                                        width="20"> Hand
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox"
                                                       name="special_requirement_ear"
                                                       class=" special_requirements"> <img
                                                        src="<?= base_url() . 'assets/plugins/icons/ear.jpeg'; ?>"
                                                        width="20"> Ear</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="check-label"> <input type="checkbox"
                                                                               name="special_requirement_face"
                                                                               class=" special_requirements">
                                                <img src="<?= base_url() . 'assets/plugins/icons/face.png'; ?>"
                                                     width="20"> Face</label>

                                        </div>
                                    </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label">
                                                    <input type="checkbox"
                                                           name="special_requirement_feet"
                                                           class=" special_requirements"> <img
                                                            src="<?= base_url() . 'assets/plugins/icons/feet.png'; ?>"
                                                            width="20"> Feet
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> <input type="checkbox"
                                                                                   name="special_requirement_overall"
                                                                                   class=" special_requirements">
                                                    <img src="<?= base_url() . 'assets/plugins/icons/body.png'; ?>"
                                                         width="20"> Overall</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> <input type="checkbox"
                                                                                   name="special_requirement_dusk_msk"
                                                                                   class=" special_requirements"> <img
                                                            src="<?= base_url() . 'assets/plugins/icons/mask.png'; ?>"
                                                            width="20"> Dusk
                                                    Mask
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label">
                                                    <input type="checkbox"
                                                           name="special_requirement_respiator"
                                                           class=" special_requirements"> <img
                                                            src="<?= base_url() . 'assets/plugins/icons/respirator.png'; ?>"
                                                            width="20"> Respiator

                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label">
                                                    <input type="checkbox"
                                                           name="special_requirement_scba"
                                                           class=" special_requirements"> <img
                                                            src="<?= base_url() . 'assets/plugins/icons/scuba.png'; ?>"
                                                            width="20"> SCBA</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label">
                                                    <input type="checkbox"
                                                           name="special_requirement_full_harness"
                                                           class=" special_requirements"> <img
                                                            src="<?= base_url() . 'assets/plugins/icons/harness.png'; ?>"
                                                            width="20"> Full Harness</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label">
                                                    <input type="checkbox"
                                                           name="special_requirement_hi_viz_ves"
                                                           class=" special_requirements"> <img
                                                            src="<?= base_url() . 'assets/plugins/icons/vest.png'; ?>"
                                                            width="20"> Hi-viz Vest

                                                </label>
                                            </div>
                                        </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="check-label">
                                                <input type="checkbox" id="special_requirement_other_checkbox"
                                                       name="special_requirement_other_checkbox"
                                                       class=" special_requirements"> Other

                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-12" id="special_reqs_other_div"></div>

                                    </div>


                                <div class="requester_acknowledge">
                                        <div class="form-section">
                                            <span>Requester Acknowledgement</span>
                                        </div>
                                        <div class="row">
                                            <div class="bullets col-sm-12">

                                                <ul>
                                                    <li>I fully understand and will abide by all policies and
                                                        procedures
                                                        as
                                                        described in the GDH Data Center Policies and Procedures
                                                        document.
                                                    </li>
                                                    <li>I fully understand and agree to these rules and that the
                                                        facility is
                                                        monitored by CCTV. I also agree to provide my full
                                                        cooperation during any investigation concerning a security
                                                        matter, which
                                                        might have occurred in the Data Center during a
                                                        time when my presence in the facility has been recorded.
                                                    </li>
                                                    <li>GDH reserves their right to terminate the access of the Data
                                                        Center for its
                                                        personnel, clients, subcontractors and visitors
                                                        found violating these rules and in case of emergency
                                                    </li>
                                                </ul>


                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Sinature</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="Signature" required="required" type="text"
                                                           name="signature"
                                                           value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->signature : ''; ?>">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Date</label>
                                                    <input class="single-daterange form-control" data-error="Rquired"
                                                           placeholder="12-03-2017" name="signature_date"
                                                           type="text"
                                                           value="<?= (isset($work_permit) && $work_permit <> null) ? $work_permit->signature_date : ''; ?>">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="SaveInfoWorkPermit" class="btn btn-info bluebackground">Save</button>
                        <button type="button" class="btn btn-default" id="close_work_permit" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>

        $(document).ready(function () {

            $(document).on('click', '#SaveInfoWorkPermit', function (e) {

                // e.preventDefault();

                $('form.work_permit_form').on('beforeSubmit', function (event) {

                    // validateInputs();

                }).on('submit', function (e) {

                    var Valid = 1;

                    if (jQuery('input[type=checkbox].business_requirements:checked').length <= 0) {

                        Valid = 0;

                        swal({
                            title: "Warning",
                            text: "Please select one option from Permit Required for",
                            type: "error",
                            confirmButtonText: "OK"
                        });
                    }


                    if (jQuery('input[type=checkbox].type_of_work:checked').length <= 0) {

                        Valid = 0;

                        swal({
                            title: "Warning",
                            text: "Please select one option from Type of work to be carried out",
                            type: "error",
                            confirmButtonText: "OK"
                        });
                    }

                    if (jQuery('input[type=checkbox].special_requirements:checked').length <= 0) {

                        Valid = 0;

                        swal({
                            title: "Warning",
                            text: "Please select one option from Special Requirements and Use of Safety Equipment",
                            type: "error",
                            confirmButtonText: "OK"
                        });
                    }


                    if (Valid) {

                        e.preventDefault();

                        var form = $(this);
                        var formData = form.serialize();

                        $.ajax({
                            url: form.attr("action"),
                            type: form.attr("method"),
                            data: formData,
                            success: function (data) {
                                console.log(data);
                                $("#SaveInfoMaterial").attr('disabled', false);

                                swal({
                                    title: "Sccuess",
                                    text: "Data Saved",
                                    type: "success",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: true
                                }, function () {
                                    $("#close_work_permit").trigger('click');
                                });
                            },
                            error: function (e) {
                                $("#SaveInfoMaterial").attr('disabled', false);

                            }
                        });

                    }

                    return false;
                });

            });


            $("#other_special_reqs").change(function () {
                if (this.checked) {

                    var html = '<div class="form-group">\n' +
                        '                                            <label>Other</label>\n' +
                        '                                            <input class="form-control type_of_work" placeholder=""\n' +
                        '                                                   type="text" name="other_work_type"\n' +
                        '                                            <div class="help-block form-text with-errors form-control-feedback"></div>\n' +
                        '                                        </div>';


                    $('#other_special_reqs_div').html(html);
                } else {
                    $('#other_special_reqs_div').html('');
                }
            });


            $("#special_requirement_other_checkbox").change(function () {
                if (this.checked) {

                    var html = '<div class="form-group">\n' +
                        '        <label>Other</label>\n' +
                        '        <textarea rows="3" class="fullwidth"\n' +
                        '                  name="special_requirement_other"\n' +
                        '                  placeholder=""\n' +
                        '                  required="required"></textarea> \n' +
                        '    </div>';


                    $('#special_reqs_other_div').html(html);
                } else {
                    $('#special_reqs_other_div').html('');
                }
            });




        });

    </script>