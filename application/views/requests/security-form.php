
<style>

    .signature-pad {

        width:400px;
        height:100px;
        background-color: white;
        border: 1px solid #333;
    }
</style>

<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Request Form</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>

        </div>
        <div class="row">


            <div class="col-md-12">
                <div class="content-box">


                    <form id="form-validate">
                        <div class="form-section-head">
                            <span>Access Request Form</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Document Number : 12243455555</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Date : 12-03-2017</label>
                                </div>
                            </div>
                        </div>
                        <!--                        requester information-->

                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <img  src="<?php echo site_url('assets/images/profile.png') ?>" class="img-rounded formimage showLable" alt="Cinque Terre">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <img  src="<?php echo site_url('assets/images/uaeid.jpg') ?>" class="img-rounded formimage showLable" alt="Cinque Terre">
                                </div>
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="showLable">Name : ABC USER</label>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Email address : abc@gmail.com</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Contact Detail : 00971 23 111111</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Designation: Manager</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Company Name : Wisdom IT</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Access Start Date : 02-02-2018</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Time : 09:00am</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Access End Date : 02-02-2018</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Time : 10:00am</label>
                                </div>
                            </div>
                        </div>


                        <!--                        Access Type-->


                        <div class="form-section">
                            <span>Type of Access</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label showLable">New User <input type="checkbox" name="access_type"
                                                                                         class="check_access_type" checked></label>
                                </div>
                            </div>
                        </div>


                        <!--                        Business-->

                        <div class="business_div">
                            <div class="form-section">
                                <span>Specify Business Requests For Access Request</span>
                            </div>
                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label showLable">Mobilization <input type="checkbox"
                                                                                                 name="business_requirements"
                                                                                                 class="business_requirements" checked></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--                        zone-->
                        <div class="form-section">
                            <span>Zone Access Required</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label showLable">Generator Room <input type="checkbox" checked> </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label showLable">First Floor Corridor <input type="checkbox" checked></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label showLable">Ground Floor Corridor <input type="checkbox" checked></label>
                                </div>
                            </div>

                        </div>

                        <div class="form-section">
                            <span>Operations</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <div class="form-group">

                                    <div class="signature_div">
                                        <label class="check-label">Signature for sign In</label>
                                        <canvas id="signature-pad" class="signature-pad" width=100 height=50></canvas>
                                        <div class="clearfix"></div>
                                        <button class="btn btn-default " id="clear">Clear</button>
                                        <button class="btn btn-info " id="signedbtn" >Sign In</button>
                                    </div>



                                </div>
                            </div>


                            <div class="col-sm-12 text-center">
                                <div class="form-group">

                                    <div class="signature_div2 hidden">
                                        <div class="success heading3"> You are Now Signed In!</div>
                                        <label class="check-label">Signature for sign Out</label>
                                        <canvas id="signature-pad2" class="signature-pad" width=100 height=50></canvas>
                                        <div class="clearfix"></div>

                                        <button class="btn btn-default " id="clear">Clear</button>
                                        <button class="btn btn-info " >Sign Out</button>
                                    </div>
                                </div>


                            </div>

                        </div>





                        <div class="content-box-footer">
                            <button class="btn btn-primary bluebackground ">Save</button>
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back To
                                Requests</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>





    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/signature_js/signature_pad.js'; ?>"></script>

    <script type="text/javascript">
        //material check boxes
        $('#signinbtn').click(function () {
            $('#matOut').appendTo("body").modal('show');
        });
        var canvas = document.getElementById('signature-pad');
        var canvas2 = document.getElementById('signature-pad2');
        function resizeCanvas() {
            // When zoomed out to less than 100%, for some very strange reason,
            // some browsers report devicePixelRatio as less than 1
            // and only part of the canvas is cleared then.
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
            var ratio1 =  Math.max(window.devicePixelRatio || 1, 1);
            canvas2.width = canvas2.offsetWidth * ratio1;
            canvas2.height = canvas2.offsetHeight * ratio1;
            canvas2.getContext("2d").scale(ratio1, ratio1);
        }
        window.onresize = resizeCanvas;
        resizeCanvas();

        var signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        });
        var signaturePad2 = new SignaturePad(canvas2, {
            backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        });

        $('#clear').on('click',function(){
            signaturePad.clear();
            return false;
        });
        $('#clear').on('click',function(){
            signaturePad2.clear();
            return false;
        });
        $('#signedbtn').on('click',function(){
            $('.signature_div').html('');
            $('.signature_div2').show(500);
            var canvas2 = document.getElementById('signature-pad2');

            return false;
        });

    </script>
