<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Request Form</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">


                    <form id="form-validate">
                        <div class="form-section-head">
                            <span>Access Request Form</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Document Number</label>
                                    <input class="form-control" data-error="Please input your First Name"
                                           placeholder="12344JKL" required="required" type="text"
                                           name="refrence_number">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>

                        </div>
                        <!--                        requester information-->

                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" data-error="Your email address is invalid"
                                   placeholder="ABC User" required="required" type="text" name="requester_name">
                            <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="abc@gmail.com" required="required" type="email">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" placeholder="009711234567" required="required"
                                           type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Designation</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           name="requester_designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                           type="text" name="requester_company">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access Start Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="request_start_date" value="04-12-2017">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Time</label>
                                    <input class=" form-control" placeholder="Date of birth"
                                           type="text" name="request_start_time" value="01:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access End Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="request_end_date" value="04-12-2017">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Time</label>
                                    <input class="form-control" placeholder="Date of birth"
                                           type="text" name="request_end_time" value="03:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <img  src="<?php echo site_url('assets/images/profile.png') ?>" class="img-rounded formimage showLable" alt="Cinque Terre">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <img  src="<?php echo site_url('assets/images/uaeid.jpg') ?>" class="img-rounded formimage showLable" alt="Cinque Terre">
                                </div>
                            </div>
                        </div>

                        <!--                        Access Type-->


                        <div class="form-section">
                            <span>Type of Access</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> New User <input type="checkbox" name="access_type"
                                                                                class="check_access_type"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Pre Approved User <input type="checkbox"
                                                                                         name="access_type"
                                                                                         class="check_access_type preapprover"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Emergency Access <input type="checkbox"
                                                                                        name="access_type"
                                                                                        class="check_access_type">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Work Permit <input type="checkbox"
                                                                                        name="access_type"
                                                                                        class="check_access_type">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">

                                    <button type="button" class="btn btn-info" >Print Work Out Form </button>

                                </div>
                            </div>
                        </div>
                        <!--                        Business-->

                        <div class="business_div">
                            <div class="form-section">
                                <span>Specify Business Requests For Access Request</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Site Visit <input type="checkbox"
                                                                                      name="business_requirements"
                                                                                      class="business_requirements"></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Mobilization <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements"></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Fit-Out Work <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Material Movement <input type="checkbox"
                                                                                             name="business_requirements"
                                                                                             class="business_requirements"></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> FM Services <input type="checkbox"
                                                                                       name="business_requirements"
                                                                                       class="business_requirements"></label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Equipment Inspection <input type="checkbox"
                                                                                                name="business_requirements"
                                                                                                class="business_requirements">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Meeting <input type="checkbox"
                                                                                   name="business_requirements"
                                                                                   class="business_requirements">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Courier <input type="checkbox"
                                                                                   name="business_requirements"
                                                                                   class="business_requirements">
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Minor Work <input type="checkbox"
                                                                                      name="business_requirements"
                                                                                      class="business_requirements">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Other Contracted Work <input type="checkbox"
                                                                                                 name="business_requirements"
                                                                                                 class="business_requirements">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <label style=" display: inline;">Other</label>
                                    <input style=" display: inline; width: 65%" class="form-control"
                                           data-error="Your email address is invalid"
                                           placeholder="Manager" name="business_requirements_other" type="text">
                                </div>
                                <div class="help-block form-text with-errors form-control-feedback"></div>

                            </div>
                        </div>
                        <!--                        zone-->
                        <div class="zone_div">
                            <div class="form-section">
                                <span>Zone Access Required</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Data Hall </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 1 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 2 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> 3 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Office Area <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> POE Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du1 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> Du2 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET1 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> ET2 <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Receiving Area <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Storage Area <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>


                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Battery Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> LAB Area <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> UPS Output Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Output Room <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Main Switch Room </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> A <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> B <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> C <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> D <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>


                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="check-label"> Preaction Room <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Main Tank Area <input type="checkbox"> </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Fuel Pump Room <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> AHU Room <input type="checkbox"></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Water Pump Room <input type="checkbox">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Chilled Area Backyard <input
                                                    type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> External Area <input type="checkbox"></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Generator Room <input type="checkbox"> </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> First Floor Corridor <input type="checkbox"></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="check-label"> Ground Floor Corridor <input
                                                    type="checkbox"></label>
                                    </div>
                                </div>

                                <div class="clearifix"></div>

                            </div>
                        </div>


                        <!--                        preapprove section-->
                        <div class="preapprove_section hidden">
                            <div class="form-section">
                                <span>Main Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for=""> ID Details</label>
                                        <div class="uploader-wrapper">
                                            <button type="button" class="btn btn-primary picker-uploader"><i
                                                        class="fa fa-cloud-upload"></i> Upload / Select Images
                                            </button>
                                        </div>
                                        <div class="validation-message" data-field="images"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <input class="form-control" placeholder="Signature" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-section">
                                <span>Backup Contact Person(Approver/Rquestor)</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for=""> ID Details</label>
                                        <div class="uploader-wrapper">
                                            <button type="button" class="btn btn-primary picker-uploader"><i
                                                        class="fa fa-cloud-upload"></i> Upload / Select Images
                                            </button>
                                        </div>
                                        <div class="validation-message" data-field="images"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <input class="form-control" placeholder="Signature" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-section">
                                <span>Pre Approved Personnel</span>
                            </div>
                            <div class="personnels"></div>


                            <div class="row materialitems">
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                               type="text" name="requester_company">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Manager" required="required" type="text"
                                               name="requester_designation">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for=""> ID Details</label>
                                        <div class="uploader-wrapper">
                                            <button type="button" class="btn btn-primary picker-uploader"><i
                                                        class="fa fa-cloud-upload"></i> Upload / Select Images
                                            </button>
                                        </div>
                                        <div class="validation-message" data-field="images"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Signature</label>
                                        <div class="input-group">
                                            <input class="form-control" placeholder="Signature" required="required"
                                                   type="text" name="requester_company">
                                            <div class="input-group-btn">
                                                <button class="btn btn-success" id="addPersonnel" type="button"><i
                                                            class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--                    End    preapprove section-->
                        <div class="material_div business_div">
                            <div class="form-section">
                                <span>Material Movement Details</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label"> Material </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> IN <input type="checkbox"
                                                                              class="check_material_in"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label"> OUT <input type="checkbox"
                                                                               class="check_material_out"></label>

                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                       
                                        <button type="button" class="btn btn-info" >Print Materia In Form </button>

                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">

                                        <button type="button" class="btn btn-info" >Print Materia Out Form </button>

                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="form-section business_div">
                            <span>Accompanying Visitors Detail</span>
                        </div>
                        <div class="row business_div" id="visitor_fields">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Visotor Name</label>

                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="XYZ Visitor" required="required" type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Company Name</label>

                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Wisdom IT" required="required" type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Contact Details</label>
                                    <div class="input-group">
                                        <input class="form-control" placeholder="052 1234567" required="required"
                                               type="text">

                                        <div class="input-group-btn">
                                            <button class="btn btn-success" id="addScnt" type="button"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>


                            </div>

                        </div>

                        <div class="requester_acknowledge">
                            <div class="form-section">
                                <span>Requester Acknowledgement</span>
                            </div>
                            <div class="row">
                                <div class="bullets col-sm-12">

                                    <ul>
                                        <li>I fully understand and will abide by all policies and procedures as
                                            described in the GDH Data Center Policies and Procedures
                                            document.
                                        </li>
                                        <li>I fully understand and agree to these rules and that the facility is
                                            monitored by CCTV. I also agree to provide my full
                                            cooperation during any investigation concerning a security matter, which
                                            might have occurred in the Data Center during a
                                            time when my presence in the facility has been recorded.
                                        </li>
                                        <li>GDH reserves their right to terminate the access of the Data Center for its
                                            personnel, clients, subcontractors and visitors
                                            found violating these rules and in case of emergency
                                        </li>
                                    </ul>


                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Sinature</label>
                                        <div class="input-group">
                                            <div class="btn-group radio-group">
                                                <label class="btn btn-primary bluebackground">Approve <input type="radio" value="male" name="gender" checked></label>
                                                <label class="btn btn-primary  not-active">Reject <input type="radio" value="female" name="gender"></label>
                                            </div>
                                        </div>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control" data-error="Please input your Last Name"
                                               placeholder="12-03-2017" required="required" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-section">
                            <span> For Data Centre Operations Team</span>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Access Number</label>
                                    <input class="form-control" name="access_number" data-error="Your Access Number"
                                           placeholder="AERT-711-34567" required="required" type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row paddingtop8">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="check-label">Esscort Service </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label ">Optional <input type="checkbox" name="esscort_service" value="optional" ></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Mandatory <input type="checkbox"  name="esscort_service" value="mandatory" ></label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="row paddingtop8">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Approver Type</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label ">Visitor <input type="checkbox" name="appprover_type" value="visitor"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Contracter <input type="checkbox"  name="appprover_type" value="contracter"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Customer <input type="checkbox"  name="appprover_type" value="customer"></label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Access Start Date</label>
                                    <input class="single-daterange form-control"  name="start_date" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Time</label>
                                    <input class="single-daterange form-control" ame="start_time" placeholder="Date of birth"
                                           type="text" value="01:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Access End Date</label>
                                    <input class="single-daterange form-control" name="end_date" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Time</label>
                                    <input class="single-daterange form-control"  name="end_time"  placeholder="Date of birth"
                                           type="text" value="03:00 pm">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Signature</label>
                                    <div class="input-group">
                                        <div class="btn-group radio-group">
                                            <label class="btn btn-primary bluebackground not-active">Approve <input type="radio" value="approved" name="team_signature" checked></label>
                                            <label class="btn btn-primary  ">Reject <input type="radio" value="rejected" name="team_signature"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth" name="team_signature_date"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>


                        </div>

                        <div class="content-box-footer">
                            <button class="btn btn-primary bluebackground" id="workoutform">Save</button>
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back To
                                Requests</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="workoutModel" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title heading3">Work out Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">

                                <form id="form-validate">
                                    <div class="form-section-head">
                                        <span>Gulf Data Hub</span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">


                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input class="form-control" data-error="Please input your Last Name"
                                                       placeholder="12-03-2017" required="required" type="text"
                                                       name="form_date">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div><!--                        requester information-->

                                    <div class="form-section">
                                        <span>Requester Information</span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input class="form-control" data-error="Your email address is invalid"
                                                       placeholder="Name" required="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Contact Detail</label>
                                                <input class="form-control" placeholder="009711234567"
                                                       required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Designation</label>
                                            <div class="form-group">
                                                <input class="form-control" data-error="Your email address is invalid"
                                                       placeholder="Manager" required="required" type="text"
                                                       name="requester_designation">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input class="form-control" placeholder="Wisdom IT Solution"
                                                       required="required"
                                                       type="text" name="requester_company">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="check-label"> Permit Required for? </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clreafix"></div>
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Mobilization Only <input type="checkbox"
                                                                                                     name="business_requirements"
                                                                                                     class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Fit-Out Work <input type="checkbox"
                                                                                                name="business_requirements"
                                                                                                class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> FM Services <input type="checkbox"
                                                                                               name="business_requirements"
                                                                                               class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Minor Work <input type="checkbox"
                                                                                              name="business_requirements"
                                                                                              class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Other Contracted Work <input type="checkbox"
                                                                                                         name="business_requirements"
                                                                                                         class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Permit Validity Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" name="request_start_date" value="04-12-2017">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Permit Validity Time</label>
                                                <input class=" form-control" placeholder="Date of birth"
                                                       type="text" name="request_start_time" value="01:00 pm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Permit Expiry Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" name="request_end_date" value="04-12-2017">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Permit Expiry Time</label>
                                                <input class="form-control" placeholder="Date of birth"
                                                       type="text" name="request_end_time" value="03:00 pm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Location of work</label>
                                            <div class="form-group">
                                                <input class="form-control" data-error="Your email address is invalid"
                                                       placeholder="Location of work" required="required" type="text"
                                                       name="requester_designation">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Access Reference#</label>
                                                <input class="form-control" placeholder="Access Reference#"
                                                       required="required"
                                                       type="text" name="requester_company">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-section">
                                        <span>Name of team workers to carry the work</span>
                                    </div>
                                    <div class="row" id="workers">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="Name" required="required"
                                                           type="text">

                                                    <div class="input-group-btn">
                                                        <button class="btn btn-success" id="addworker" type="button"><i
                                                                    class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>


                                        </div>

                                    </div>

                                    <div class="form-section">
                                        <span>Type of work to be carried out</span>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Planned Power Hazzard/Outage <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Interruption of Exsisting Services <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Any Contact with Electrical Supply <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Installation of Racks or Devices <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Lifting/Removal of Floor Tiles <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Welding,Grinding,Drilling or Cutting <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Cable Pulling and Termination <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Material Movement <input type="checkbox"
                                                                                                     name="business_requirements"
                                                                                                     class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Installation or Modifications of Cage <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Waste Management <input type="checkbox"
                                                                                                    name="business_requirements"
                                                                                                    class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Excavation or Civil Work <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Planned Cooling system Hazzard/Outage <input
                                                            type="checkbox"
                                                            name="business_requirements"
                                                            class="business_requirements">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Other</label>
                                                <input class="form-control" placeholder="Access Reference#"
                                                       required="required"
                                                       type="text" name="requester_company">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>

                                        <div class="help-block form-text with-errors form-control-feedback"></div>

                                    </div>

                                    <div class="form-section">
                                        <span>Detail of tested tool and material required</span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea rows="20" class="form-control" placeholder=""
                                                          required="required"></textarea>
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-section">

                                        <span>Method of Statement for work to be carried Out</span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea rows="20" class="form-control" placeholder=""
                                                          required="required"></textarea>
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-section">

                                        <span>Risk Assessment for Work to be carried Out</span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea rows="20" class="form-control" placeholder=""
                                                          required="required"></textarea>
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-section">

                                        <span>Special Requirements and Use of Safety Equipment</span>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Head <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Eye <input type="checkbox"
                                                                                       name="business_requirements"
                                                                                       class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Hand <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Ear <input type="checkbox"
                                                                                       name="business_requirements"
                                                                                       class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Face <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Feet <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Overall <input type="checkbox"
                                                                                           name="business_requirements"
                                                                                           class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Dusk Mask <input type="checkbox"
                                                                                             name="business_requirements"
                                                                                             class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Respiator <input type="checkbox"
                                                                                             name="business_requirements"
                                                                                             class="business_requirements">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> SCBA <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Full Harness <input type="checkbox"
                                                                                                name="business_requirements"
                                                                                                class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Hi-viz Vest <input type="checkbox"
                                                                                               name="business_requirements"
                                                                                               class="business_requirements">
                                                </label>
                                            </div>
                                        </div>


                                        <div class="help-block form-text with-errors form-control-feedback"></div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea rows="20" class="form-control" placeholder=""
                                                          required="required"></textarea>
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="requester_acknowledge">
                                        <div class="form-section">
                                            <span>Requester Acknowledgement</span>
                                        </div>
                                        <div class="row">
                                            <div class="bullets col-sm-12">

                                                <ul>
                                                    <li>I fully understand and will abide by all policies and procedures
                                                        as
                                                        described in the GDH Data Center Policies and Procedures
                                                        document.
                                                    </li>
                                                    <li>I fully understand and agree to these rules and that the
                                                        facility is
                                                        monitored by CCTV. I also agree to provide my full
                                                        cooperation during any investigation concerning a security
                                                        matter, which
                                                        might have occurred in the Data Center during a
                                                        time when my presence in the facility has been recorded.
                                                    </li>
                                                    <li>GDH reserves their right to terminate the access of the Data
                                                        Center for its
                                                        personnel, clients, subcontractors and visitors
                                                        found violating these rules and in case of emergency
                                                    </li>
                                                </ul>


                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Sinature</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="Signature" required="required" type="text"
                                                           name="Signature">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Date</label>
                                                    <input class="form-control" data-error="Please input your Last Name"
                                                           placeholder="12-03-2017" required="required" type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="matIn" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title heading3">Material In Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">

                                <form id="form-validate">
                                    <div class="form-section-head">
                                        <span>Material In Form</span>
                                    </div>
                                    <div class="test">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input class="form-control" placeholder="Name" required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Email address</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="abc@gmail.com" required="required" type="email">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Contact Detail</label>
                                                    <input class="form-control" placeholder="009711234567"
                                                           required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <input class="form-control" placeholder="Wisdom IT Solution"
                                                           required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Designation</label>
                                                    <input class="form-control" placeholder="Designation"
                                                           required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Tools Requirements</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="Tools Requirements" required="required"
                                                           type="email">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Manpower Requirements</label>
                                                    <input class="form-control" placeholder="Manpower Requirements"
                                                           required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>For Customer Name</label>
                                                    <input class="form-control" placeholder="Customer Nam"
                                                           required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Assigned Person for check and super vision</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="Manager" required="required" type="text"
                                                           name="requester_designation">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Date</label>
                                                    <input class="single-daterange form-control"
                                                           placeholder="Date of birth"
                                                           type="text" name="request_start_date" value="04-12-2017">
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Time</label>
                                                    <input class="form-control" placeholder="01:00 pm"
                                                           type="text" name="request_start_time" value="01:00 pm">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-section">
                                            <span>Add Items Information</span>
                                        </div>
                                        <div id="material_fields"></div>
                                        <div class="row materialitems">
                                            <div class="col-sm-3">
                                                <input class="form-control" data-error="Item Description"
                                                       placeholder="Item Description " required="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <input class="form-control" data-error="Item Description"
                                                       placeholder="Quantity " required="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Serial#"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Model"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Power"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Heat Dissipation"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Storage"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Compliance check name"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>


                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="Status"
                                                               type="text">

                                                        <div class="input-group-btn">
                                                            <button class="btn btn-success" id="addMat" type="button"><i
                                                                        class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>

                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row paddingtop30">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Requester Signature</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Customer Pre-approved Signature</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved By Signature</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Reference For Approval</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Remarks</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="matOut" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title heading3">Material Out Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box">

                                <form id="form-validate">
                                    <div class="form-section-head">
                                        <span>Material Out Form</span>
                                    </div>
                                    <div class="test">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input class="form-control" placeholder="Name" required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Email address</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="abc@gmail.com" required="required" type="email">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Contact Detail</label>
                                                    <input class="form-control" placeholder="009711234567"
                                                           required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <input class="form-control" placeholder="Wisdom IT Solution"
                                                           required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Designation</label>
                                                    <input class="form-control" placeholder="Designation"
                                                           required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Tools Requirements</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="Tools Requirements" required="required"
                                                           type="email">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Manpower Requirements</label>
                                                    <input class="form-control" placeholder="Manpower Requirements"
                                                           required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>For Customer Name</label>
                                                    <input class="form-control" placeholder="Customer Nam"
                                                           required="required"
                                                           type="text" name="requester_company">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Assigned Person for check and super vision</label>
                                                    <input class="form-control"
                                                           data-error="Your email address is invalid"
                                                           placeholder="Manager" required="required" type="text"
                                                           name="requester_designation">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Date</label>
                                                    <input class="single-daterange form-control"
                                                           placeholder="Date of birth"
                                                           type="text" name="request_start_date" value="04-12-2017">
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Time</label>
                                                    <input class="form-control" placeholder="01:00 pm"
                                                           type="text" name="request_start_time" value="01:00 pm">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-section">
                                            <span>Add Items Information</span>
                                        </div>
                                        <div id="material_fields1"></div>
                                        <div class="row materialitems" id="material_fields1">
                                            <div class="col-sm-3">
                                                <input class="form-control" data-error="Item Description"
                                                       placeholder="Item Description " required="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <input class="form-control" data-error="Item Description"
                                                       placeholder="Quantity " required="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Serial#"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Model"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Power"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Heat Dissipation"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Storage"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Compliance check name"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>


                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="Status"
                                                               type="text">

                                                        <div class="input-group-btn">
                                                            <button class="btn btn-success" id="addMat1" type="button">
                                                                <i
                                                                        class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>

                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row paddingtop30">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Requester Signature</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Customer Pre-approved Signature</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved By Signature</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Reference For Approval</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Remarks</label>
                                                    <input class="form-control" placeholder="" required="required"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        // Input radio-group visual controls
        $('.radio-group label').on('click', function(){
            $(this).removeClass('not-active').siblings().addClass('not-active');
        });
        $('#myModal').appendTo("body").modal('show');
        $('#workform').on('click', function () {
            $('#myModal').appendTo("body").modal('show');

        });


        $(".preapprover").change(function () {
            if (this.checked) {
                $('.requester_acknowledge').hide(500);
                $('.business_div').hide(500);
                $('.preapprove_section').show(500);

            } else {
                $('.requester_acknowledge').show(500);
                $('.business_div').show(500);
                $('.preapprove_section').hide(500);

            }
        });


        //material check boxes
        $('#workoutform').click(function () {
            $('#workoutModel').appendTo("body").modal('show');
        });
        // Mariral addmore




        $(".check_material_in").change(function () {
            if (this.checked) {
                $('#matIn').appendTo("body").modal('show');
            } else {
                $('.matIn').hide(500);
            }
        });

        $(".check_material_out").change(function () {
            if (this.checked) {
                $('#matOut').appendTo("body").modal('show');
            } else {
                $('.matOut').hide(500);
            }
        });



        if ($('input[name="access_type"]').is(':checked')) {
            //  alert('sadasd');
            $(this).val();
        }
        else {
            $('input[name="email"]').hide();
        }


        $(function () {

            //workers div
            var perkDiv = $('.personnels');
            var html_personnel = '';
            html_personnel += '<div class="row addPersonnel materialitems">';
            html_personnel += '<div class="col-sm-4">';

            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Name</label>';
            html_personnel += '<input class="form-control" data-error="Your email address is invalid" placeholder="Manager" required="required" type="text"name="requester_designation">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Title</label>';
            html_personnel += '<input class="form-control" placeholder="Wisdom IT Solution" required="required" type="text" name="requester_company">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += ' </div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Contact Number</label>';
            html_personnel += '<input class="form-control" placeholder="Wisdom IT Solution" required="required" type="text" name="requester_company">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Email</label>';
            html_personnel += '<input class="form-control" data-error="Your email address is invalid" placeholder="Manager" required="required" type="text" name="requester_designation">';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += ' <label for=""> ID Details</label>';
            html_personnel += '<div class="uploader-wrapper">';
            html_personnel += '<button type="button" class="btn btn-primary picker-uploader"><i class="fa fa-cloud-upload"></i> Upload / Select Images </button>';
            html_personnel += '</div>';
            html_personnel += '<div class="validation-message" data-field="images"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="col-sm-4">';
            html_personnel += '<div class="form-group">';
            html_personnel += '<label>Signature</label>';
            html_personnel += '<div class="input-group">';
            html_personnel += '<input class="form-control" placeholder="Signature" required="required" type="text" name="requester_company">';
            html_personnel += '<div class="input-group-btn">';
            html_personnel += '<button class="btn btn-danger" id="remPersonnel" type="button"><i class="fa fa-times"></i></button>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_personnel += '</div>';
            html_personnel += '</div>';
            html_personnel += '</div>';

            $('#addPersonnel').on('click', function () {
                $(html_personnel).insertAfter(perkDiv);

                $('#remPersonnel').on('click', function () {
                    $(this).parents('.addPersonnel').remove();
                    return false;
                });

                return false;
            });


            //workers div
            var workDiv = $('#workers');
            var html_work_fiels = '';
            html_work_fiels += '<div class="row addWorker">';
            html_work_fiels += '<div class="col-sm-12">';
            html_work_fiels += ' <div class="form-group">';
            html_work_fiels += '<label>Name</label>';
            html_work_fiels += '<div class="input-group">';
            html_work_fiels += '<input class="form-control" placeholder="Name" required="required" type="text">';

            html_work_fiels += ' <div class="input-group-btn">';
            html_work_fiels += ' <button  class="btn btn-danger" id="remWorker" type="button"><i class="fa fa-times"></i></button>';
            html_work_fiels += '</div>';
            html_work_fiels += '</div>';

            html_work_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_work_fiels += ' </div>';
            html_work_fiels += ' </div>';
            html_work_fiels += '</div>';


            $('#addworker').on('click', function () {
                $(html_work_fiels).insertAfter(workDiv);

                $('#remWorker').on('click', function () {
                    $(this).parents('.addWorker').remove();
                    return false;
                });

                return false;
            });

            //visitors form add more
            var mattDiv = $('#material_fields');
            var mattDiv1 = $('#material_fields1');

            var html_matrial_fiels = '';

            html_matrial_fiels += '<div class="row materialitems addmaterial">';
            html_matrial_fiels += '<div class="col-sm-3">';
            html_matrial_fiels += '<input class="form-control" placeholder="Item Description" required="required" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-2">';
            html_matrial_fiels += '<input class="form-control" placeholder="Qunatity" required="required" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-2">';
            html_matrial_fiels += '<div class="form-group">';
            html_matrial_fiels += '<input class="form-control" placeholder="Serial#" type="text">';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-2">';
            html_matrial_fiels += '<div class="form-group">';
            html_matrial_fiels += '<input class="form-control" placeholder=" Model" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-3">';
            html_matrial_fiels += '<div class="form-group">';
            html_matrial_fiels += ' <input class="form-control" placeholder="Power" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += ' <div class="col-sm-3">';
            html_matrial_fiels += ' <div class="form-group">';

            html_matrial_fiels += ' <input class="form-control" placeholder=" Heat Dissipation" type="text">';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-3">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += ' <input class="form-control" placeholder="Storage" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-3">';
            html_matrial_fiels += '<div class="form-group">';
            html_matrial_fiels += '<input class="form-control" placeholder=" Comliance check name" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += ' </div>';
            html_matrial_fiels += ' </div>';
            html_matrial_fiels += ' <div class="col-sm-3">';
            html_matrial_fiels += ' <div class="form-group">';
            html_matrial_fiels += ' <div class="input-group">';
            html_matrial_fiels += ' <input class="form-control" placeholder="Status" type="text">';
            html_matrial_fiels += '<div class="input-group-btn">';
            html_matrial_fiels += '<button class="btn btn-danger" id="remMat" type="button"><i class="fa fa-times"></i></button>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += ' </div>';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';


            $('#addMat').on('click', function () {
                $(html_matrial_fiels).insertAfter(mattDiv);

                $('#remMat').on('click', function () {
                    $(this).parents('.addmaterial').remove();
                    return false;
                });

                return false;
            });
            $('#addMat1').on('click', function () {
                $(html_matrial_fiels).insertAfter(mattDiv1);
                $('#remMat').on('click', function () {
                    $(this).parents('.addmaterial').remove();
                    return false;
                });

                return false;
            });


            //visitors form add more
            var scntDiv = $('#visitor_fields');

            var html_vistor = '';
            html_vistor += '<div class="row addvisitor">';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label>Visotor Name</label>';
            html_vistor += '<input class="form-control" data-error="Your email address is invalid"placeholder="XYZ Visitor" required="required" type="text">';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label>Company Name</label>';
            html_vistor += '<input class="form-control" data-error="Your email address is invalid" placeholder="Wisdom IT" required="required" type="text">';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label>Contact Details</label>';
            html_vistor += '<div class="input-group">';
            html_vistor += '<input class="form-control" placeholder="052 1234567" required="required" type="text">';
            html_vistor += '<div class="input-group-btn">';
            html_vistor += '<button class="btn btn-danger" type="button" id="remScnt"> <i class="fa fa-times"></i> </button>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '</div>';


            $('#addScnt').on('click', function () {
                $(html_vistor).insertAfter(scntDiv);

                $('#remScnt').on('click', function () {
                    $(this).parents('.addvisitor').remove();
                    return false;
                });

                return false;
            });


            $('#agreeButton, #disagreeButton').on('click', function () {
                $(html_vistor).insertAfter("#tt");

                $('#form-validate')
                    .find('[name="agree"]')
                    .val(whichButton === 'agreeButton' ? 'yes' : '')
                    .end();
                var agreeValue = $('#agree_id').val();
                if (agreeValue == 'yes') {
                    $('#agree_error').hide(200);

                } else {
                    $('#agree_error').show(200);
                }
            });


            //accesstype checkboxes

            //material check boxes
            $('.check_access_type').click(function () {
                $('.check_access_type').not(this).prop('checked', false);
            });
            //material check boxes
            $('.check_material').click(function () {
                $('.check_material').not(this).prop('checked', false);
            });
            // Mariral addmore


            $(".check_material").change(function () {
                if (this.checked) {
                    $('.test').show(500);
                } else {
                    $('.test').hide(500);
                }
            });


            $('#addmatrial').on('click', function () {
                $(html_material).insertAfter(matDiv);

                $('#remMatrial').on('click', function () {
                    $(this).parents('.materials').remove();
                    return false;
                });

                return false;
            });

            $('#remMatrial').on('click', function () {
                $(this).parents('.materials').remove();
                return false;
            });


        });


    </script>