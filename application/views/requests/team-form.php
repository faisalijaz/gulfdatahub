<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Request Form</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>
            <div class="content-header-title">Access Request Form</div>
        </div>
        <div class="row">


            <div class="col-md-12">
                <div class="content-box">


                    <form id="form-validate">
                        <div class="form-section-head">
                            <span>Gulf Data Hub</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Document Number : 12243455555</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Date : 12-03-2017</label>
                                </div>
                            </div>
                        </div>
                        <!--                        requester information-->

                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <img  src="<?php echo site_url('assets/images/profile.png') ?>" class="img-rounded formimage showLable" alt="Cinque Terre">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <img  src="<?php echo site_url('assets/images/uaeid.jpg') ?>" class="img-rounded formimage showLable" alt="Cinque Terre">
                                </div>
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="showLable">Name : ABC USER</label>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Email address : abc@gmail.com</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Contact Detail : 00971 23 111111</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Designation: Manager</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Company Name : Wisdom IT</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Access Start Date : 02-02-2018</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Time : 09:00am</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Access End Date : 02-02-2018</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="showLable">Time : 10:00am</label>
                                </div>
                            </div>
                        </div>


                        <!--                        Access Type-->


                        <div class="form-section">
                            <span>Type of Access</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label showLable">New User <input type="checkbox" name="access_type"
                                                                                class="check_access_type" checked></label>
                                </div>
                            </div>
                        </div>


                        <!--                        Business-->

                        <div class="business_div">
                            <div class="form-section">
                                <span>Specify Business Requests For Access Request</span>
                            </div>
                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label showLable">Mobilization <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements" checked></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--                        zone-->
                        <div class="form-section">
                            <span>Zone Access Required</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label showLable">Generator Room <input type="checkbox" checked> </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label showLable">First Floor Corridor <input type="checkbox" checked></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label showLable">Ground Floor Corridor <input type="checkbox" checked></label>
                                </div>
                            </div>

                        </div>





                        <div class="zone_div">
                            <div class="form-section">
                                <span>Material Movement Details</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="check-label showLable">Material </label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label">IN <input type="checkbox"
                                                                              class="check_material"></label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label class="check-label">OUT <input type="checkbox"
                                                                               class="check_material" checked></label>

                                    </div>
                                </div>
                            </div>

                            <div class="test">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Name : XYZ</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Email address : xyz@gmail.com</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Contact Detail : 00971xxxxxx</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Company Name: Wisdom IT</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Designation : Manager</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Tools Requirements : xxxxx</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Manpower Requirements : abcds</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">For Customer Name : xcvbny</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="showLable">Assigned Person for check and super vision : XYZ</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Date : 12-02-2018</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="showLable">Time : 11:00am</label>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-section">
                                    <span>Items</span>
                                </div>
                                <div class="row" id="material_fields">


                                    <div class="container">

                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Serial#</th>
                                                <th>Item Description</th>

                                                <th>Model</th>
                                                <th>Power</th>
                                                <th>Heat Dissipation</th>
                                                <th>Storage</th>
                                                <th>Comliance check name</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Doe</td>
                                                <td>john@example.com</td>
                                                <td>Mary</td>
                                                <td>Moe</td>
                                                <td>mary@example.com</td>
                                                <td>Moe</td>
                                                <td>mary@example.com</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Doe</td>
                                                <td>john@example.com</td>
                                                <td>Mary</td>
                                                <td>Moe</td>
                                                <td>mary@example.com</td>
                                                <td>Moe</td>
                                                <td>mary@example.com</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Doe</td>
                                                <td>john@example.com</td>
                                                <td>Mary</td>
                                                <td>Moe</td>
                                                <td>mary@example.com</td>
                                                <td>Moe</td>
                                                <td>mary@example.com</td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>






                                </div>


                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group paddingtop30">
                                            <label class="showLable">Remarks : Some Remarks</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-section">
                            <span>Accompanying Visitors Detail</span>
                        </div>


                        <div class="container">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Visitor Name</th>
                                    <th>Company Name</th>
                                    <th>Contact Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                </tr>
                                <tr>
                                    <td>Mary</td>
                                    <td>Moe</td>
                                    <td>mary@example.com</td>
                                </tr>
                                <tr>
                                    <td>July</td>
                                    <td>Dooley</td>
                                    <td>july@example.com</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="form-section">
                            <span> For Data Centre Operations Team</span>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label >Access Number</label>
                                <input class="form-control" data-error="Your email address is invalid"
                                       placeholder="AERT-711-34567" required="required" type="text">
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row paddingtop8">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Esscort Service </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label ">Optional <input type="checkbox"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Mandatory <input type="checkbox"></label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Access Start Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Time</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="01:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Access End Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Time</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="03:00 pm">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Signature</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Signature" required="required" type="text"
                                           name="Signature">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>


                        </div>


                        <div class="content-box-footer">
                            <button class="btn btn-primary">Save</button>
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning">Back To
                                Requests</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="basicModal" style="margin-top: 50px;" tabindex="-1" role="dialog"
         aria-labelledby="Terms and conditions" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">

                    <h3 class="modal-title">Terms and conditions</h3>
                </div>

                <div class="modal-body">
                    <div class="row">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="agreeButton" data-dismiss="modal">Agree</button>
                    <button type="button" class="btn btn-default" id="disagreeButton" data-dismiss="modal">Disagree
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">


        $(function () {

            //visitors form add more
            var mattDiv = $('#material_fields');

            var html_matrial_fiels = '';

            html_matrial_fiels += '<div class="row addmaterial">';
            html_matrial_fiels += '<div class="col-sm-2">';

            html_matrial_fiels += '<input class="form-control" placeholder="Item Description" required="required" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += '<input class="form-control" placeholder="Serial#" type="text">';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += '<input class="form-control" placeholder=" Model" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';
            html_matrial_fiels += ' <input class="form-control" placeholder="Power" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += ' <div class="col-sm-2">';
            html_matrial_fiels += ' <div class="form-group">';

            html_matrial_fiels += ' <input class="form-control" placeholder=" Heat Dissipation" type="text">';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += ' <input class="form-control" placeholder="Storage" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-2">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += '<input class="form-control" placeholder=" Comliance check name" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += ' </div>';
            html_matrial_fiels += ' </div>';


            html_matrial_fiels += ' <div class="col-sm-2">';
            html_matrial_fiels += ' <div class="form-group">';

            html_matrial_fiels += ' <div class="input-group">';
            html_matrial_fiels += ' <input class="form-control" placeholder="Status" type="text">';

            html_matrial_fiels += '<div class="input-group-btn">';
            html_matrial_fiels += '<button class="btn btn-danger" id="remMat" type="button"><i class="fa fa-times"></i></button>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += ' </div>';

            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';


            $('#addMat').on('click', function () {
                $(html_matrial_fiels).insertAfter(mattDiv);

                $('#remMat').on('click', function () {
                    $(this).parents('.addmaterial').remove();
                    return false;
                });

                return false;
            });


            //visitors form add more
            var scntDiv = $('#visitor_fields');

            var html_vistor = '';
            html_vistor += '<div class="row addvisitor">';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label class="showLable">Visotor Name</label>';
            html_vistor += '<input class="form-control" data-error="Your email address is invalid"placeholder="XYZ Visitor" required="required" type="text">';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label class="showLable">Company Name</label>';
            html_vistor += '<input class="form-control" data-error="Your email address is invalid" placeholder="Wisdom IT" required="required" type="text">';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label class="showLable">Contact Details</label>';
            html_vistor += '<div class="input-group">';
            html_vistor += '<input class="form-control" placeholder="052 1234567" required="required" type="text">';
            html_vistor += '<div class="input-group-btn">';
            html_vistor += '<button class="btn btn-danger" type="button" id="remScnt"> <i class="fa fa-times"></i> </button>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '</div>';


            $('#addScnt').on('click', function () {
                $(html_vistor).insertAfter(scntDiv);

                $('#remScnt').on('click', function () {
                    $(this).parents('.addvisitor').remove();
                    return false;
                });

                return false;
            });


            $('#agreeButton, #disagreeButton').on('click', function () {
                $(html_vistor).insertAfter("#tt");

                $('#form-validate')
                    .find('[name="agree"]')
                    .val(whichButton === 'agreeButton' ? 'yes' : '')
                    .end();
                var agreeValue = $('#agree_id').val();
                if (agreeValue == 'yes') {
                    $('#agree_error').hide(200);

                } else {
                    $('#agree_error').show(200);
                }
            });


            //accesstype checkboxes

            //material check boxes
            $('.check_access_type').click(function () {
                $('.check_access_type').not(this).prop('checked', false);
            });
            //material check boxes
            $('.check_material').click(function () {
                $('.check_material').not(this).prop('checked', false);
            });
            // Mariral addmore



            $(".check_material").change(function () {
                if (this.checked) {
                    $('.test').show(500);
                } else {
                    $('.test').hide(500);
                }
            });


            $('#addmatrial').on('click', function () {
                $(html_material).insertAfter(matDiv);

                $('#remMatrial').on('click', function () {
                    $(this).parents('.materials').remove();
                    return false;
                });

                return false;
            });

            $('#remMatrial').on('click', function () {
                $(this).parents('.materials').remove();
                return false;
            });


        });


    </script>