<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Data Access Requests</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>
            <div class="content-header-title">Requests</div>
        </div>
        <?php if ($this->session->flashdata('error_message')) { ?>
            <div class="alert alert-danger alert-dismissable margintopbtm20">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Sorry! </strong><?php echo $this->session->flashdata('error_message'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success_message')) { ?>
            <div class="alert alert-success alert-dismissable margintopbtm20">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success! </strong><?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php } ?>

        <div class="row">



            <div class="col-md-6 paddingtop8">
                <a class="btn btn-primary orangebackground" href="<?php echo site_url('accessRequests/create') ?>"><i
                            class="fa fa-pencil"></i> Create Data Access Request</a>
            </div>
            <div class="col-md-6 paddingtop8">
                <a class="btn btn-info orangebackground pull-right"
                   href="<?php echo base_url('') . 'DC_Access_Request_Form.pdf' ?>"><i class="fa fa-download"></i>
                    Download Access Form For Manual Process</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered datatable">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Requester Name</th>
                                <th class="text-center">Designation</th>
                                <th class="text-center">Approval Manger Name</th>
                                <th class="text-center">Status</th>
                                <th class="text-right"> Date</th>
                                <th class="text-center">Preview / Print</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($requests) > 0) {

                                foreach ($requests as $row) {
                                    ?>
                                    <tr>
                                        <th class="text-center"><?php echo $row['id']; ?></th>
                                        <td class="nowrap text-center"><?php echo $row['name']; ?></td>
                                        <td class="text-center">
                                            <?php echo $row['designation']; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php echo $row['manager_name']; ?>
                                        </td>
                                        <td class="nowrap text-center"> <?php if($row['team_signature']!=''){echo $row['team_signature'];}else{echo 'Pending';} ?></td>


                                        <td class="text-right"> <?php echo $row['team_signature_date']; ?></td>

                                        <?php if($this->session->userdata['active_user']->group_id == 2 ){ ?>
                                            <td class="text-center"><a href="<?php echo site_url('product/securityform?id=' . $this->common->encode($row['id'])); ?>"> <i class="fa fa-eye"></i></a></i></td>
                                        <?php }else{ ?>
                                            <td class="text-center"><a href="<?php echo site_url('accessRequests/update/' . $row['id']); ?>"> <i class="fa fa-eye"></i></a></i></td>
                                        <?php } ?>

                                       <!-- <td class="text-center">
                                            <a href="/accessRequests/update/<?php /*echo $row['id']; */?>" > <i class="fa fa-download"></i> </a>
                                        </td>-->
                                    </tr>


                                    <?php
                                }
                                ?>

                            <?php }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>