<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Pre Approver Requests</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>
            <div class="content-header-title">Requests</div>
        </div>
        <div class="row">
            <div class="col-md-6 paddingtop8">
                <a class="btn btn-primary" href="<?php echo site_url('accessRequests/create') ?>"><i class="fa fa-pencil"></i> Create Data Access Request</a>
            </div>
            <div class="col-md-6 paddingtop8">
                <a class="btn btn-info pull-right" href="<?php echo base_url('').'DC_Access_Request_Form.pdf' ?>"><i class="fa fa-download"></i> Download Access Form For Manual Process</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered datatable">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Requester Name</th>
                                <th class="text-center">Manger Name</th>
                                <th class="text-center">Backup Person</th>
                                <th class="text-center">Status</th>
                                <th class="text-right">Date</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th class="text-center">1</th>
                                <td class="nowrap text-center">Any Requester</td>
                                <td class="text-center">
                                   Manager Name
                                </td>
                                <td class="nowrap text-center">Some Person</td>
                                <td class="nowrap text-center">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <tr>
                                <th class="text-center">2</th>
                                <td class="nowrap text-center">Any Requester</td>
                                <td class="text-center">
                                    Manager Name
                                </td>
                                <td class="nowrap text-center">Some Person</td>
                                <td class="nowrap text-center">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <tr>
                                <th class="text-center">3</th>
                                <td class="nowrap text-center">Any Requester</td>
                                <td class="text-center">
                                    Manager Name
                                </td>
                                <td class="nowrap text-center">Some Person</td>
                                <td class="nowrap text-center">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <tr>
                                <th class="text-center">4</th>
                                <td class="nowrap text-center">Any Requester</td>
                                <td class="text-center">
                                    Manager Name
                                </td>
                                <td class="nowrap text-center">Some Person</td>
                                <td class="nowrap text-center">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <tr>
                                <th class="text-center">5</th>
                                <td class="nowrap text-center">Any Requester</td>
                                <td class="text-center">
                                    Manager Name
                                </td>
                                <td class="nowrap text-center">Some Person</td>
                                <td class="nowrap text-center">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <tr>
                                <th class="text-center">6</th>
                                <td class="nowrap text-center">Any Requester</td>
                                <td class="text-center">
                                    Manager Name
                                </td>
                                <td class="nowrap text-center">Some Person</td>
                                <td class="nowrap text-center">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>