<?php
if (isset($material_out_data) && count($material_out_data) > 0) {
    $material_out_data = $material_out_data[0];
} else {
    $material_out_data = null;
}
?>
<div class="modal fade" id="matOut" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form-material-in-out" data-validation="required" class="form-material-out" method="post"
                  action="/accessRequests/submitRequestInOutForm">
                <div class="modal-header">
                    <h4 class="modal-title heading3 material_from_heading">Material Out Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row" id="printMaterialOut">
                        <div class="col-md-12">
                            <div class="content-box">
                                <div class="form-section-head ">
                                    <span class="material_from_heading">Material Out Form</span>
                                </div>
                                <div class="test material_input_form">
                                    <div class="row">
                                        <input class="form-control" id="material_form_type" name="type"
                                               value="out" type="hidden">

                                        <?php
                                        if (isset($material_out_data) && $material_out_data <> null) {
                                            ?>
                                            <input type="hidden" name="id" value="<?= $material_out_data->id; ?>">
                                            <?php
                                        }
                                        ?>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input class="form-control" placeholder="Name"
                                                       data-validation="required"
                                                       type="text" name="Name" required="required"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->Name : ''; ?>"
                                                >
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Email address</label>
                                                <input class="form-control"
                                                       data-error="Your email address is invalid" required="required"
                                                       placeholder="abc@gmail.com" type="email"
                                                       data-validation="required"
                                                       name="m_email"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->m_email : ''; ?>"
                                                >
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Contact Detail</label>
                                                <input class="form-control" placeholder="009711234567"
                                                       data-validation="required"
                                                       name="phone"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->phone : ''; ?>"

                                                >
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input class="form-control" placeholder="Wisdom IT Solution"
                                                       data-validation="required"
                                                       type="text" name="company_name"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->company_name : ''; ?>"
                                                >
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input class="form-control" placeholder="Designation"
                                                       data-validation="required" required="required"
                                                       type="text" name="designation"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->designation : ''; ?>">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Tools Requirements</label>
                                                <input class="form-control" required="required"
                                                       data-error="Enter tool requirements"
                                                       placeholder="Tools Requirements" data-validation="required"
                                                       name="tools_requirements"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->tools_requirements : ''; ?>">

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Manpower Requirements</label>
                                                <input class="form-control" placeholder="Manpower Requirements"
                                                       data-validation="required"
                                                       name="manpower_requirements"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->manpower_requirements : ''; ?>">

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>For Customer Name</label>
                                                <input class="form-control" placeholder="Customer Nam"
                                                       data-validation="required"
                                                       type="text" name="for_customer_name"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->for_customer_name : ''; ?>">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Assigned Person for check and super vision</label>
                                                <input class="form-control"
                                                       data-error="Your email address is invalid"
                                                       placeholder="Manager" data-validation="required" type="text"
                                                       name="assigned_person"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->assigned_person : ''; ?>">


                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input class="single-daterange form-control"
                                                       placeholder=""
                                                       type="text" name="m_date"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->m_date : ''; ?>">

                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input class="form-control timepicker" placeholder="01:00 pm"
                                                       id="time"
                                                       type="text" name="time"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->time : date('H:i:s'); ?>">

                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-section">
                                        <span>Add Items Information</span>
                                    </div>

                                    <div id="material_fields1"></div>
                                    <?php
                                    if (isset($material_out_data) && $material_out_data <> null) {
                                        $materialItems = getDataByColumn($table = 'material_items', $col = 'material_id', $material_out_data->id);
                                        if (count($materialItems) > 0) {

                                            $total = count($materialItems);
                                            $i = 1;

                                            foreach ($materialItems as $items) {
                                                ?>
                                                <div class="row materialitems">
                                                    <div class="col-sm-3">
                                                        <input class="form-control" data-error="Item Description"
                                                               name="Itmes[material_item][]"
                                                               placeholder="Item Description "
                                                               data-validation="required" type="text"
                                                               value="<?= $items->material_item; ?>">
                                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input class="form-control" data-error="Quntity"
                                                               name="Itmes[material_quntity][]"
                                                               placeholder="Quantity" data-validation="required"
                                                               value="<?= $items->material_quntity; ?>"
                                                               type="text">
                                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">

                                                            <input class="form-control" placeholder="Serial Number"
                                                                   data-error="Serial Number"
                                                                   name="Itmes[material_serial][]"
                                                                   value="<?= $items->material_serial; ?>"
                                                                   type="text" data-validation="required">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">

                                                            <input class="form-control" placeholder="Model"
                                                                   data-error="Model"
                                                                   name="Itmes[material_model][]"
                                                                   value="<?= $items->material_model; ?>"
                                                                   type="text" data-validation="required">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">

                                                            <input class="form-control" placeholder="Power Capacity"
                                                                   data-error="Model"
                                                                   name="Itmes[material_power_capacity][]"
                                                                   value="<?= $items->material_power_capacity; ?>"
                                                                   type="text" data-validation="required">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">

                                                            <input class="form-control" placeholder=" Heat Dissipation"
                                                                   value="<?= $items->material_power_heat; ?>"
                                                                   type="text" name="Itmes[material_power_heat][]">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input class="form-control" placeholder="Storage"
                                                                   name="Itmes[material_power_storage][]"
                                                                   value="<?= $items->material_power_storage; ?>"
                                                                   type="text">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input class="form-control"
                                                                   placeholder="Compliance check name"
                                                                   name="Itmes[material_power_check_name][]"
                                                                   value="<?= $items->material_power_check_name; ?>"
                                                                   type="text">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input class="form-control" placeholder="Status"
                                                                       name="Itmes[material_status][]"
                                                                       value="<?= $items->material_status; ?>"
                                                                       type="text">
                                                                <?php
                                                                if ($total == $i) {
                                                                    ?>
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-success" id="addMat1"
                                                                                type="button"><i class="fa fa-plus"></i>
                                                                        </button>
                                                                    </div>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <button class="btn btn-danger remMat" id="remMat"
                                                                            type="button"><i class="fa fa-times"></i>
                                                                    </button>
                                                                    <?php
                                                                } ?>
                                                            </div>

                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                            }
                                        }
                                    } else {
                                        ?>
                                        <div class="row materialitems">
                                            <div class="col-sm-3">
                                                <input class="form-control" data-error="Item Description"
                                                       name="Itmes[material_item][]"
                                                       placeholder="Item Description " data-validation="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <input class="form-control" data-error="Quntity"
                                                       name="Itmes[material_quntity][]"
                                                       placeholder="Quantity" data-validation="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Serial Number"
                                                           data-error="Serial Number" name="Itmes[material_serial][]"
                                                           type="text" data-validation="required">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Model" data-error="Model"
                                                           name="Itmes[material_model][]"
                                                           type="text" data-validation="required">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Power Capacity"
                                                           data-error="Model" name="Itmes[material_power_capacity][]"
                                                           type="text" data-validation="required">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder=" Heat Dissipation"
                                                           type="text" name="Itmes[material_power_heat][]">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Storage"
                                                           name="Itmes[material_power_storage][]"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <input class="form-control" placeholder="Compliance check name"
                                                           name="Itmes[material_power_check_name][]"
                                                           type="text">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>


                                            <div class="col-sm-3">
                                                <div class="form-group">

                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="Status"
                                                               name="Itmes[material_status][]"
                                                               type="text">

                                                        <div class="input-group-btn">
                                                            <button class="btn btn-success" id="addMat1" type="button">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                    <div class="row paddingtop30">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Requester Signature</label>
                                                <input class="form-control" placeholder="" data-validation="required"
                                                       name="Itmes_requester_signature"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->Itmes_requester_signature : ''; ?>">

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Customer Pre-approved Signature</label>
                                                <input class="form-control" placeholder="" data-validation="required"
                                                       name="preapproved_signature"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->preapproved_signature : ''; ?>">

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved By Signature</label>
                                                <input class="form-control" placeholder="" data-validation="required"
                                                       name="approved_by_signature"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->approved_by_signature : ''; ?>">

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Reference For Approval</label>
                                                <input class="form-control" placeholder="" data-validation="required"
                                                       name="reference_for_approval"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->reference_for_approval : ''; ?>">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <input class="form-control" placeholder="" data-validation="required"
                                                       name="remarks"
                                                       type="text"
                                                       value="<?= (isset($material_out_data) && $material_out_data <> null) ? $material_out_data->remarks : ''; ?>">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="saveForm" id="SaveInfoMaterial" class="btn btn-info bluebackground">Save</button>
                    <button type="button" id="matOutClose" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        $('body').on('click', '#SaveInfoMaterial', function (e) {

            $('form.form-material-out').on('beforeSubmit', function (event) {

                // validateInputs();

            }).on('submit', function (e) {

                e.preventDefault();

                 var form = $(this);
                var formData = form.serialize();

                $.ajax({
                    url: form.attr("action"),
                    type: form.attr("method"),
                    data: formData,
                    success: function (data) {

                        $("#SaveInfoMaterial").attr('disabled', false);

                        swal({
                            title: "Sccuess",
                            text: "Data Saved",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }, function (e) {
                            $("#matOutClose").trigger('click');
                        });
                    },
                    error: function (e) {
                        $("#SaveInfoMaterial").attr('disabled', false);
                    }
                });
            });

        });

    });

</script>