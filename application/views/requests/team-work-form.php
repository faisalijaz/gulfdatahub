<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Request Form</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">

                    <form id="form-validate">
                        <div class="form-section-head">
                            <span>Work Permit Form</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">


                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="form-control" data-error="Please input your Last Name"
                                           placeholder="12-03-2017" required="required" type="text"
                                           name="form_date">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div><!--                        requester information-->

                        <div class="form-section">
                            <span>Requester Information</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Name" required="required" type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Detail</label>
                                    <input class="form-control" placeholder="009711234567" required="required"
                                           type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Designation</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           name="requester_designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                           type="text" name="requester_company">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="check-label"> Permit Required for? </label>
                                </div>
                            </div>
                        </div>
                        <div class="clreafix"></div>
                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Mobilization Only <input type="checkbox"
                                                                                         name="business_requirements"
                                                                                         class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Fit-Out Work <input type="checkbox"
                                                                                    name="business_requirements"
                                                                                    class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> FM Services <input type="checkbox"
                                                                                   name="business_requirements"
                                                                                   class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Minor Work <input type="checkbox"
                                                                                  name="business_requirements"
                                                                                  class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Other Contracted Work <input type="checkbox"
                                                                                             name="business_requirements"
                                                                                             class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="help-block form-text with-errors form-control-feedback"></div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Permit Validity Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="request_start_date" value="04-12-2017">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Permit Validity Time</label>
                                    <input class=" form-control" placeholder="Date of birth"
                                           type="text" name="request_start_time" value="01:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Permit Expiry Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" name="request_end_date" value="04-12-2017">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Permit Expiry Time</label>
                                    <input class="form-control" placeholder="Date of birth"
                                           type="text" name="request_end_time" value="03:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Location of work</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Location of work" required="required" type="text"
                                           name="requester_designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Access Reference#</label>
                                    <input class="form-control" placeholder="Access Reference#" required="required"
                                           type="text" name="requester_company">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>


                        <div class="form-section">
                            <span>Name of team workers to carry the work</span>
                        </div>
                        <div class="row" id="workers">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="input-group">
                                        <input class="form-control" placeholder="Name" required="required"
                                               type="text">

                                        <div class="input-group-btn">
                                            <button class="btn btn-success" id="addworker" type="button"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>


                            </div>

                        </div>

                        <div class="form-section">
                            <span>Type of work to be carried out</span>
                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Planned Power Hazzard/Outage <input type="checkbox"
                                                                                                    name="business_requirements"
                                                                                                    class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Interruption of Exsisting Services <input type="checkbox"
                                                                                                          name="business_requirements"
                                                                                                          class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Any Contact with Electrical Supply <input type="checkbox"
                                                                                                          name="business_requirements"
                                                                                                          class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Installation of Racks or Devices <input type="checkbox"
                                                                                                        name="business_requirements"
                                                                                                        class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Lifting/Removal of Floor Tiles <input type="checkbox"
                                                                                                      name="business_requirements"
                                                                                                      class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Welding,Grinding,Drilling or Cutting <input type="checkbox"
                                                                                                            name="business_requirements"
                                                                                                            class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Cable Pulling and Termination <input type="checkbox"
                                                                                                     name="business_requirements"
                                                                                                     class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Material Movement <input type="checkbox"
                                                                                         name="business_requirements"
                                                                                         class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Installation or Modifications of Cage <input type="checkbox"
                                                                                                             name="business_requirements"
                                                                                                             class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Waste Management <input type="checkbox"
                                                                                        name="business_requirements"
                                                                                        class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Excavation or Civil Work <input type="checkbox"
                                                                                                name="business_requirements"
                                                                                                class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="check-label"> Planned Cooling system Hazzard/Outage <input type="checkbox"
                                                                                                             name="business_requirements"
                                                                                                             class="business_requirements">
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Other</label>
                                    <input class="form-control" placeholder="Access Reference#" required="required"
                                           type="text" name="requester_company">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>

                            <div class="help-block form-text with-errors form-control-feedback"></div>

                        </div>

                        <div class="form-section">
                            <span>Detail of tested tool and material required</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea rows="20"  class="form-control" placeholder="" required="required"></textarea>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>


                        <div class="form-section">

                            <span>Method of Statement for work to be carried Out</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea rows="20"  class="form-control" placeholder="" required="required"></textarea>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-section">

                            <span>Risk Assessment for Work to be carried Out</span>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea rows="20"  class="form-control" placeholder="" required="required"></textarea>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-section">

                            <span>Special Requirements and Use of Safety Equipment</span>
                        </div>

                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Head <input type="checkbox"
                                                                            name="business_requirements"
                                                                            class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Eye <input type="checkbox"
                                                                           name="business_requirements"
                                                                           class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Hand <input type="checkbox"
                                                                            name="business_requirements"
                                                                            class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Ear <input type="checkbox"
                                                                           name="business_requirements"
                                                                           class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Face <input type="checkbox"
                                                                            name="business_requirements"
                                                                            class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Feet <input type="checkbox"
                                                                            name="business_requirements"
                                                                            class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Overall <input type="checkbox"
                                                                               name="business_requirements"
                                                                               class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Dusk Mask <input type="checkbox"
                                                                                 name="business_requirements"
                                                                                 class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Respiator <input type="checkbox"
                                                                                 name="business_requirements"
                                                                                 class="business_requirements">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> SCBA <input type="checkbox"
                                                                            name="business_requirements"
                                                                            class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Full Harness <input type="checkbox"
                                                                                    name="business_requirements"
                                                                                    class="business_requirements"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label"> Hi-viz Vest <input type="checkbox"
                                                                                   name="business_requirements"
                                                                                   class="business_requirements">
                                    </label>
                                </div>
                            </div>


                            <div class="help-block form-text with-errors form-control-feedback"></div>

                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Other</label>
                                    <textarea rows="20"  class="form-control" placeholder="" required="required"></textarea>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>

                        <div class="requester_acknowledge">
                            <div class="form-section">
                                <span>Requester Acknowledgement</span>
                            </div>
                            <div class="row">
                                <div class="bullets col-sm-12">

                                    <ul>
                                        <li>I fully understand and will abide by all policies and procedures as
                                            described in the GDH Data Center Policies and Procedures
                                            document.
                                        </li>
                                        <li>I fully understand and agree to these rules and that the facility is
                                            monitored by CCTV. I also agree to provide my full
                                            cooperation during any investigation concerning a security matter, which
                                            might have occurred in the Data Center during a
                                            time when my presence in the facility has been recorded.
                                        </li>
                                        <li>GDH reserves their right to terminate the access of the Data Center for its
                                            personnel, clients, subcontractors and visitors
                                            found violating these rules and in case of emergency
                                        </li>
                                    </ul>


                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Sinature</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="Signature" required="required" type="text"
                                               name="Signature">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control" data-error="Please input your Last Name"
                                               placeholder="12-03-2017" required="required" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-section">
                            <span> For Data Centre Operations Team</span>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Permit Number</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="AERT-711-34567" required="required" type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row paddingtop8">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Esscort Service </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label ">Optional <input type="checkbox"></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="check-label">Mandatory <input type="checkbox"></label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Permit Validity Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Permit Validity Time</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="01:00 pm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Permit Expiry Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Permit Expiry Time</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="03:00 pm">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Approved By</label>
                                    <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                           type="text" name="requester_company">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Designation</label>
                                <div class="form-group">
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Manager" required="required" type="text"
                                           name="requester_designation">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-12">
                                <label>Remarks</label>
                                <div class="form-group">
                                    <textarea class="form-control" name="remarks"></textarea>
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>

                        </div>





                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >Signature</label>
                                    <input class="form-control" data-error="Your email address is invalid"
                                           placeholder="Signature" required="required" type="text"
                                           name="Signature">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="single-daterange form-control" placeholder="Date of birth"
                                           type="text" value="04-12-2017">
                                </div>
                            </div>


                        </div>

                        <div class="content-box-footer">
                            <button class="btn btn-primary bluebackground">Save</button>
                            <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning orangebackground">Back To
                                Requests</a>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        $('#myModal').appendTo("body").modal('show');
        $('#workform').on('click', function () {
            $('#myModal').appendTo("body").modal('show');

        });




        $(".preapprover").change(function() {
            if(this.checked) {
                $('.requester_acknowledge').hide(500);
                $('.business_div').hide(500);
            }else{
                $('.requester_acknowledge').show(500);
                $('.business_div').show(500);

            }
        });


        if ( $('input[name="access_type"]').is(':checked') ) {
          //  alert('sadasd');
           $(this).val();
        }
        else {
            $('input[name="email"]').hide();
        }


        $(function () {


            //workers div
            var workDiv = $('#workers');
            var html_work_fiels = '';
            html_work_fiels += '<div class="row addWorker">';


            html_work_fiels += '<div class="col-sm-12">';
            html_work_fiels += ' <div class="form-group">';
            html_work_fiels += '<label>Name</label>';
            html_work_fiels += '<div class="input-group">';
            html_work_fiels += '<input class="form-control" placeholder="Name" required="required" type="text">';

            html_work_fiels += ' <div class="input-group-btn">';
            html_work_fiels += ' <button  class="btn btn-danger" id="remWorker" type="button"><i class="fa fa-times"></i></button>';
            html_work_fiels += '</div>';
            html_work_fiels += '</div>';

            html_work_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_work_fiels += ' </div>';
            html_work_fiels += ' </div>';
            html_work_fiels += '</div>';


            $('#addworker').on('click', function () {
                $(html_work_fiels).insertAfter(workDiv);

                $('#remWorker').on('click', function () {
                    $(this).parents('.addWorker').remove();
                    return false;
                });

                return false;
            });

            //visitors form add more
            var mattDiv = $('#material_fields');

            var html_matrial_fiels = '';

            html_matrial_fiels += '<div class="row addmaterial">';
            html_matrial_fiels += '<div class="col-sm-2">';

            html_matrial_fiels += '<input class="form-control" placeholder="Item Description" required="required" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += '<input class="form-control" placeholder="Serial#" type="text">';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += '<input class="form-control" placeholder=" Model" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';
            html_matrial_fiels += ' <input class="form-control" placeholder="Power" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += ' <div class="col-sm-2">';
            html_matrial_fiels += ' <div class="form-group">';

            html_matrial_fiels += ' <input class="form-control" placeholder=" Heat Dissipation" type="text">';
            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-1">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += ' <input class="form-control" placeholder="Storage" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '<div class="col-sm-2">';
            html_matrial_fiels += '<div class="form-group">';

            html_matrial_fiels += '<input class="form-control" placeholder=" Comliance check name" type="text">';
            html_matrial_fiels += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += ' </div>';
            html_matrial_fiels += ' </div>';


            html_matrial_fiels += ' <div class="col-sm-2">';
            html_matrial_fiels += ' <div class="form-group">';

            html_matrial_fiels += ' <div class="input-group">';
            html_matrial_fiels += ' <input class="form-control" placeholder="Status" type="text">';

            html_matrial_fiels += '<div class="input-group-btn">';
            html_matrial_fiels += '<button class="btn btn-danger" id="remMat" type="button"><i class="fa fa-times"></i></button>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += ' </div>';

            html_matrial_fiels += ' <div class="help-block form-text with-errors form-control-feedback"></div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';
            html_matrial_fiels += '</div>';


            $('#addMat').on('click', function () {
                $(html_matrial_fiels).insertAfter(mattDiv);

                $('#remMat').on('click', function () {
                    $(this).parents('.addmaterial').remove();
                    return false;
                });

                return false;
            });


            //visitors form add more
            var scntDiv = $('#visitor_fields');

            var html_vistor = '';
            html_vistor += '<div class="row addvisitor">';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label>Visotor Name</label>';
            html_vistor += '<input class="form-control" data-error="Your email address is invalid"placeholder="XYZ Visitor" required="required" type="text">';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label>Company Name</label>';
            html_vistor += '<input class="form-control" data-error="Your email address is invalid" placeholder="Wisdom IT" required="required" type="text">';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="col-sm-4">';
            html_vistor += '<div class="form-group">';
            html_vistor += '<label>Contact Details</label>';
            html_vistor += '<div class="input-group">';
            html_vistor += '<input class="form-control" placeholder="052 1234567" required="required" type="text">';
            html_vistor += '<div class="input-group-btn">';
            html_vistor += '<button class="btn btn-danger" type="button" id="remScnt"> <i class="fa fa-times"></i> </button>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
            html_vistor += '</div>';
            html_vistor += '</div>';
            html_vistor += '</div>';


            $('#addScnt').on('click', function () {
                $(html_vistor).insertAfter(scntDiv);

                $('#remScnt').on('click', function () {
                    $(this).parents('.addvisitor').remove();
                    return false;
                });

                return false;
            });


            $('#agreeButton, #disagreeButton').on('click', function () {
                $(html_vistor).insertAfter("#tt");

                $('#form-validate')
                    .find('[name="agree"]')
                    .val(whichButton === 'agreeButton' ? 'yes' : '')
                    .end();
                var agreeValue = $('#agree_id').val();
                if (agreeValue == 'yes') {
                    $('#agree_error').hide(200);

                } else {
                    $('#agree_error').show(200);
                }
            });


            //accesstype checkboxes

            //material check boxes
            $('.check_access_type').click(function () {
                $('.check_access_type').not(this).prop('checked', false);
            });
            //material check boxes
            $('.check_material').click(function () {
                $('.check_material').not(this).prop('checked', false);
            });
            // Mariral addmore



            $(".check_material").change(function () {
                if (this.checked) {
                    $('.test').show(500);
                } else {
                    $('.test').hide(500);
                }
            });


            $('#addmatrial').on('click', function () {
                $(html_material).insertAfter(matDiv);

                $('#remMatrial').on('click', function () {
                    $(this).parents('.materials').remove();
                    return false;
                });

                return false;
            });

            $('#remMatrial').on('click', function () {
                $(this).parents('.materials').remove();
                return false;
            });


        });


    </script>