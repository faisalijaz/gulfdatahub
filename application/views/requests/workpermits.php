<div class="breadcrumb">
    <a href="">Home</a>
    <a href="">Work Requests</a>
</div>
<div class="content">
    <div class="panel">
        <div class="content-header no-mg-top">
            <i class="fa fa-newspaper-o"></i>
            <div class="content-header-title">Requests</div>
        </div>
        <div class="row">


        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered datatable">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Requester Name</th>
                                <th class="text-center">Reason</th>
                                <th class="text-center">Approval Member</th>
                                <th class="text-center">Status</th>
                                <th class="text-right">Date</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th class="text-center">1</th>
                                <td class="nowrap">ABC</td>
                                <td class="nowrap">Mobilization</td>
                                <td class="nowrap">Data ABC</td>
                                <td class="nowrap">Approved</td>
                                <td class="text-right">02-02-2017</td>
                                <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">2</th>
                            <td class="nowrap">EFD</td>
                            <td class="nowrap">Mobilization</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Pending</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">3</th>
                            <td class="nowrap">XYZ</td>
                            <td class="nowrap">FM Services</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Approved</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">4</th>
                            <td class="nowrap">ABC</td>
                            <td class="nowrap">Mobilization</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Approved</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">5</th>
                            <td class="nowrap">ABC</td>
                            <td class="nowrap">FM Services</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Approved</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">6</th>
                            <td class="nowrap">ABC</td>
                            <td class="nowrap">FM Services</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Approved</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">7</th>
                            <td class="nowrap">ABC</td>
                            <td class="nowrap">Minor Work</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Approved</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>
                            <th class="text-center">8</th>
                            <td class="nowrap">ABC</td>
                            <td class="nowrap">Minor Work</td>
                            <td class="nowrap">Data ABC</td>
                            <td class="nowrap">Approved</td>
                            <td class="text-right">02-02-2017</td>
                            <td class="text-center"><i class="fa fa-pencil"></i> <i class="fa fa-trash"></i> <i class="fa fa-download"></i></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>