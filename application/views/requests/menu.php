

<?php $this->load->view('components/head'); ?>


<div class="top-nav">
    <div class="top-nav-box">
        <div class="side-nav-mobile"><i class="fa fa-bars"></i></div>
        <div class="logo-wrapper">
            <div class="logo-box">
               <!-- <button type="button" id="sidebarCollapse1" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-align-left"></i>
                    <span></span>
                </button> &nbsp;-->
                <img alt="pongo" src="<?php echo base_url() . 'assets/images/logo1.jpg'; ?>">
                <a href="<?php echo base_url(); ?>">
                    <div class="logo-title">GULF DATA HUB</div>
                </a>
            </div>
        </div>
        <div class="top-nav-content">
            <div class="top-nav-box">
                <div class="quick-link"  id="sidebarCollapse1" >
                    <div class="link-icon"><i class="fa fa-bars"></i></div>

                </div>

                <div class="top-notification">
                    <div class="notification-icon pull-right">
                        <div class="notification-badge bounceInDown animated timer pull-right" data-from="0"
                             data-to="3">3
                        </div>
                        <i class="fa fa-bell"></i>
                        <div class="notification-wrapper animated bounceInUp">
                            <div class="notification-header">Notificationse <span class="notification-count">3</span>
                            </div>
                            <div class="notification-body">
                                <a class="notification-list" href="">
                                    <div class="notification-image">
                                        <img alt="pongo"
                                             src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                                    </div>
                                    <div class="notification-content">
                                        <div class="notification-text"><strong>Mark</strong> Approved Request</div>
                                        <div class="notification-time">1 minutes ago</div>
                                    </div>
                                </a>
                                <a class="notification-list" href="">
                                    <div class="notification-image">
                                        <img alt="pongo"
                                             src="<?php echo base_url() . 'assets/images/chocolate.jpg'; ?>">
                                    </div>
                                    <div class="notification-content">
                                        <div class="notification-text"><strong>Lisa</strong> Complete Request</div>
                                        <div class="notification-time">1 minutes ago</div>
                                    </div>
                                </a>
                                <a class="notification-list" href="">
                                    <div class="notification-image">
                                        <img alt="pongo" src="<?php echo base_url() . 'assets/images/belts.jpg'; ?>">
                                    </div>
                                    <div class="notification-content">
                                        <div class="notification-text"><strong>Parker</strong> Cancel Request</div>
                                        <div class="notification-time">1 minutes ago</div>
                                    </div>
                                </a>
                                <a class="notification-list" href="">
                                    <div class="notification-image">
                                        <img alt="pongo"
                                             src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                                    </div>
                                    <div class="notification-content">
                                        <div class="notification-text"><strong>Sophie</strong> Complete Request</div>
                                        <div class="notification-time">1 minutes ago</div>
                                    </div>
                                </a>
                                <a class="notification-list" href="">
                                    <div class="notification-image">
                                        <img alt="pongo"
                                             src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                                    </div>
                                    <div class="notification-content">
                                        <div class="notification-text"><strong>Sophie</strong> Complete Request</div>
                                        <div class="notification-time">1 minutes ago</div>
                                    </div>
                                </a>
                                <a class="notification-list" href="">
                                    <div class="notification-image">
                                        <img alt="pongo"
                                             src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                                    </div>
                                    <div class="notification-content">
                                        <div class="notification-text"><strong>Sophie</strong> Cancel Request</div>
                                        <div class="notification-time">1 minutes ago</div>
                                    </div>
                                </a>
                            </div>
                            <div class="notification-footer">
                                <a href="">See all notifications</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="user-top-profile">
                    <div class="user-image">
                        <div class="user-on"></div>
                        <img alt="pongo" src="<?php echo base_url() . 'assets/images/profile.png'; ?>">
                    </div>
                    <div class="clear">
                        <div class="user-name"><?php echo $active_user->name; ?></div>
                        <div class="user-group"><?php echo $active_user_group->group_name; ?></div>
                        <ul class="user-top-menu animated bounceInUp">
                            <li><a href="<?php echo base_url() . 'profile'; ?>">Profile
                                    <div class="badge badge-yellow pull-right">2</div>
                                </a></li>
                            <li><a href="<?php echo base_url() . 'settings'; ?>">Settings</a></li>
                            <li><a href="<?php echo base_url() . 'change_password'; ?>">Change Password</a></li>
                            <li><a href="<?php echo base_url() . 'auth/logout'; ?>">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="profile-nav-mobile"><i class="fa fa-power-off"></i></div>
    </div>
</div>
<div class="wrapper <?php echo $menu_style != 'default' ? $menu_style : ''; ?>" style="table-layout: auto;">



    <aside id="sidebar" class="side-nav">
        <div class="side-notification">
            <div id="dismiss">
                <i class="glyphicon glyphicon-arrow-left"></i>
            </div>

            <div class="notification-icon">
                <div class="notification-badge bounceInDown animated timer" data-from="0" data-to="3">3</div>
                <i class="fa fa-bell"></i>

                <div class="notification-wrapper animated bounceInUp">
                    <div class="notification-header">Notifications <span class="notification-count">3</span></div>
                    <div class="notification-body">
                        <a class="notification-list" href="">
                            <div class="notification-image">
                                <img alt="pongo" src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                            </div>
                            <div class="notification-content">
                                <div class="notification-text"><strong>Mark</strong> sent you a email</div>
                                <div class="notification-time">1 minutes ago</div>
                            </div>
                        </a>
                        <a class="notification-list" href="">
                            <div class="notification-image">
                                <img alt="pongo" src="<?php echo base_url() . 'assets/images/chocolate.jpg'; ?>">
                            </div>
                            <div class="notification-content">
                                <div class="notification-text"><strong>Lisa</strong> sent you a email</div>
                                <div class="notification-time">1 minutes ago</div>
                            </div>
                        </a>
                        <a class="notification-list" href="">
                            <div class="notification-image">
                                <img alt="pongo" src="<?php echo base_url() . 'assets/images/belts.jpg'; ?>">
                            </div>
                            <div class="notification-content">
                                <div class="notification-text"><strong>Parker</strong> sent you a email</div>
                                <div class="notification-time">1 minutes ago</div>
                            </div>
                        </a>
                        <a class="notification-list" href="">
                            <div class="notification-image">
                                <img alt="pongo" src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                            </div>
                            <div class="notification-content">
                                <div class="notification-text"><strong>Sophie</strong> sent you a email</div>
                                <div class="notification-time">1 minutes ago</div>
                            </div>
                        </a>
                        <a class="notification-list" href="">
                            <div class="notification-image">
                                <img alt="pongo" src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                            </div>
                            <div class="notification-content">
                                <div class="notification-text"><strong>Sophie</strong> sent you a email</div>
                                <div class="notification-time">1 minutes ago</div>
                            </div>
                        </a>
                        <a class="notification-list" href="">
                            <div class="notification-image">
                                <img alt="pongo" src="<?php echo base_url() . 'assets/images/asparagus.jpg'; ?>">
                            </div>
                            <div class="notification-content">
                                <div class="notification-text"><strong>Sophie</strong> sent you a email</div>
                                <div class="notification-time">1 minutes ago</div>
                            </div>
                        </a>
                    </div>
                    <div class="notification-footer">
                        <a href="">See all notifications</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-side-profile">
            <div class="user-image">
                <div class="user-on"></div>
                <img alt="pongo" src="<?php echo base_url() . 'assets/images/profile.png'; ?>">
            </div>
            <div class="clear">
                <div class="user-name"><?php echo $active_user->name; ?></div>
                <div class="user-group"><?php echo $active_user_group->group_name; ?></div>
                <ul class="user-side-menu animated bounceInUp">
                    <li><a href="<?php echo base_url() . 'profile'; ?>">Profile
                            <div class="badge badge-yellow pull-right">2</div>
                        </a></li>
                    <li><a href="<?php echo base_url() . 'settings'; ?>">Settings</a></li>
                    <li><a href="<?php echo base_url() . 'change_password'; ?>">Change Password</a></li>
                    <li><a href="<?php echo base_url() . 'auth/logout'; ?>">Logout</a></li>
                </ul>
            </div>
        </div>
        <div class="main-menu-title">Menu</div>
        <div class="main-menu">
            <ul>
                <li class="<?php echo $active_menu == 0 ? 'active' : ''; ?>">
                    <a href="<?php echo base_url(); ?>">
                        <i class="fa fa-bars"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php foreach ($list_menu as $key => $menu) { ?>
                    <?php if ($menu->id < 60) { ?>
                        <!-- Print parent menu -->
                        <?php if ($menu->parent_id == 0 && $menu->is_have_child != 0) { ?>
                            <li class="<?php echo $active_menu == $menu->id && $menu_style != 'compact-nav' ? 'active' : ''; ?>">
                                <a href="">
                                    <i class="<?php echo $menu->icon; ?>"></i>
                                    <span><?php echo $menu->title; ?></span>
                                    <?php if ($key == 0) { ?>
                                        <div class="badge badge-grey pull-right">2</div>

                                    <?php } elseif ($key == 6) { ?>
                                        <div class="badge badge-grey pull-right">121</div>
                                    <?php } ?>
                                </a>
                                <ul>
                                    <!-- Print submenu -->
                                    <?php foreach ($list_menu as $submenu) { ?>
                                        <?php if ($submenu->parent_id == $menu->id) { ?>
                                            <li>
                                                <a href="<?php echo base_url() . $submenu->link; ?>"><?php echo $submenu->title; ?></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } elseif ($menu->parent_id == 0 && $menu->is_have_child == 0) { ?>
                            <li class="<?php echo $active_menu == $menu->id ? 'active' : ''; ?>">
                                <a href="<?php echo base_url() . $menu->link; ?>">
                                    <i class="<?php echo $menu->icon; ?>"></i>
                                    <span><?php echo $menu->title; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>

        <div class="main-menu">
            <ul>
                <?php foreach ($list_menu as $key => $menu) { ?>
                    <?php if ($menu->id > 60) { ?>
                        <!-- Print parent menu -->
                        <?php if ($menu->parent_id == 0 && $menu->is_have_child != 0) { ?>
                            <li class="<?php echo $active_menu == $menu->id && $menu_style != 'compact-nav' ? 'active' : ''; ?>">
                                <a href="">
                                    <i class="<?php echo $menu->icon; ?>"></i>
                                    <span><?php echo $menu->title; ?></span>
                                    <?php if ($key == 121) { ?>
                                        <div class="badge badge-red pull-right">21</div>
                                    <?php } elseif ($key == 6) { ?>
                                        <div class="badge badge-grey pull-right">121</div>
                                    <?php } ?>
                                </a>
                                <ul>
                                    <!-- Print submenu -->
                                    <?php foreach ($list_menu as $submenu) { ?>
                                        <?php if ($submenu->parent_id == $menu->id) { ?>
                                            <li>
                                                <a href="<?php echo base_url() . $submenu->link; ?>"><?php echo $submenu->title; ?></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } elseif ($menu->parent_id == 0 && $menu->is_have_child == 0) { ?>
                            <li class="<?php echo $active_menu == $menu->id ? 'active' : ''; ?>">
                                <a href="<?php echo base_url($menu->link); ?>">
                                    <i class="<?php echo $menu->icon; ?>"></i>
                                    <span><?php echo $menu->title; ?></span>
                                    <?php if ($menu->id == 121) { ?>
                                        <div class="badge badge-red pull-right">21</div>
                                    <?php } ?>
                                    <?php if ($menu->id == 122) { ?>
                                        <div class="badge badge-red pull-right">2</div>
                                    <?php } ?>
                                    <?php if ($menu->id == 123) { ?>
                                        <div class="badge badge-red pull-right">10</div>
                                    <?php } ?>
                                    <?php if ($menu->id == 124) { ?>
                                        <div class="badge badge-red pull-right">8</div>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>

    </aside>




    <!-- Sidebar Holder -->


    <!-- Page Content Holder -->
    <div class="main">
        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn btn_side_btn">
            <i class="glyphicon glyphicon-align-left"></i>
            <span>Side Menu</span>
        </button>



        <div class="breadcrumb">
            <a href="">Home</a>
            <a href="">Request Form</a>
        </div>
        <div class="content">
            <div class="panel">
                <div class="content-header no-mg-top">
                    <i class="fa fa-newspaper-o"></i>
                    <div class="content-header-title">Access Request Form</div>
                </div>
                <div class="row">


                    <div class="col-md-12">
                        <div class="content-box">
                            <div class="content-box-header">
                                <div class="box-title">Gulf Data Hub</div>

                                <form id="form-validate">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Document Number</label>
                                                <input class="form-control" data-error="Please input your First Name"
                                                       placeholder="12344JKL" required="required" type="text" name="refrence_number">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input class="form-control" data-error="Please input your Last Name"
                                                       placeholder="12-03-2017" required="required" type="text" name="form_date">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-section">
                                        <span>Requester Information</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="ABC User" required="required" type="text" name="requester_name">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Email address</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="abc@gmail.com" required="required" type="email">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Contact Detail</label>
                                                <input class="form-control" placeholder="009711234567" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Designation</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="Manager" required="required" type="text" name="requester_designation">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                                       type="text" name="requester_company">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Access Start Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" name="request_start_date" value="04-12-2017">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" name="request_start_time" value="01:00 pm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Access End Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text"  name="request_end_date" value="04-12-2017">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text"  name="request_end_time"  value="03:00 pm">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-section">
                                        <span>Type of Access</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> New User <input type="checkbox" name="access_type" class="check_access_type"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Pre Approved User <input type="checkbox" name="access_type" class="check_access_type"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Emergency Access <input type="checkbox" name="access_type" class="check_access_type"> </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-section">
                                        <span>Specify Business Requests For Access Request</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Site Visit <input type="checkbox" name="business_requirements" class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Mobilization <input type="checkbox" name="business_requirements"  class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Fit-Out Work <input type="checkbox" name="business_requirements"  class="business_requirements"> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Material Movement <input type="checkbox" name="business_requirements"  class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> FM Services <input type="checkbox" name="business_requirements"  class="business_requirements"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Equipment Inspection <input type="checkbox" name="business_requirements"  class="business_requirements"> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Meeting <input type="checkbox" name="business_requirements"  class="business_requirements"> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Courier/Food <input type="checkbox" name="business_requirements"  class="business_requirements"> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">

                                            <label style=" display: inline;">Other</label>
                                            <input style=" display: inline; width: 65%" class="form-control"
                                                   data-error="Your email address is invalid"
                                                   placeholder="Manager" name="business_requirements_other"  type="text">
                                        </div>
                                        <div class="help-block form-text with-errors form-control-feedback"></div>

                                    </div>


                                    <div class="form-section">
                                        <span>Zone Access Required</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Data Hall </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> 1 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> 2 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> 3 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">

                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="check-label"> Office Area <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="clearifix"></div>


                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> POE Room </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> Du1 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> Du2 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> ET1 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> ET2 <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="check-label"> Receiving Area <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="clearifix"></div>


                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> UPS Room </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> A <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> B <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> C <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> D <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="check-label"> Storage Area <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="clearifix"></div>


                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Battery Room </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> A <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> B <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> C <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> D <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="check-label"> LAB Area <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="clearifix"></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> UPS Output Room </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> A <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> B <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> C <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> D <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="check-label"> Output Room <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="clearifix"></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Main Switch Room </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> A <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> B <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> C <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> D <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>


                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="check-label"> Preaction Room <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="clearifix"></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Fuel Main Tank Area <input type="checkbox"> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Fuel Pump Room <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> AHU Room <input type="checkbox"></label>
                                            </div>
                                        </div>

                                        <div class="clearifix"></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Chilled Water Pump Room <input type="checkbox">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Chilled Area Backyard <input
                                                            type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> External Area <input type="checkbox"></label>
                                            </div>
                                        </div>

                                        <div class="clearifix"></div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Generator Room <input type="checkbox"> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> First Floor Corridor <input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="check-label"> Ground Floor Corridor <input
                                                            type="checkbox"></label>
                                            </div>
                                        </div>

                                        <div class="clearifix"></div>

                                    </div>


                                    <div class="form-section">
                                        <span>Material Movement Details</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Material </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> IN <input type="checkbox" class="check_material"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="check-label"> OUT <input type="checkbox" class="check_material"></label>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row materials materialhtml hidden" id="material_div">
                                        <div class="col-sm-3">
                                            <label>Item Description </label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="An electronic product" required="required" type="text">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Serial Number </label>
                                                <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Brand Name & Model </label>
                                                <input class="form-control" placeholder="Samsung Note8" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Power Capacity </label>
                                                <input class="form-control" placeholder="Samsung Note8" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Heat Dissipation</label>
                                                <input class="form-control" placeholder="" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label> Storage Requirements/ Allocated Location</label>
                                                <input class="form-control" placeholder="" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label> Comliance check name/signature</label>
                                                <input class="form-control" placeholder="" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label> Status/Remarks</label>

                                                <input class="form-control" placeholder="" required="required"
                                                       type="text">

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row hidden" id="material_add_div_btn">
                                        <div class="col-sm-3 marginleft10">

                                            <button class="btn btn-success " type="button" id="addmatrial"> <i class="fa fa-plus"></i> </button>
                                        </div>
                                    </div>










                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Reference For Approval</label>
                                                <input class="form-control" placeholder="" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <input class="form-control" placeholder="" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-section">
                                        <span>General Notice For Data Center Access</span>
                                    </div>

                                    <div class="row">
                                        <div class="bullets col-sm-12">

                                            <ul>
                                                <li>This Permit is not applicable for specific task such as Confined Space
                                                    Entry, work at Height, Hot work, Spray painting, Structural
                                                    Penetration, Film Shooting, Excavation Works, Handling of Chemicals,
                                                    Isolation of services and integration with existing systems
                                                    etc. separate Work Permit will be required along with access
                                                </li>
                                                <li>All devices are to be housed in Racks. Racks can be procured and installed
                                                    through the DC Operational Teams coordination.
                                                </li>
                                                <li>Prior approval from DC Operational Team is required to identify where to
                                                    place the customer racks.
                                                </li>
                                                <li>Arrangements for delivery of equipment shall be made in coordination with
                                                    the DC Operational Teams. The Operation Team will
                                                    determine how to receive the order and where to store the equipment prior to
                                                    installation on Separate Material Movement Form
                                                </li>
                                                <li>It is Customers responsibility to work with the DC Operations team to
                                                    arrange for their equipment to be located in the proper
                                                    location in the data center.
                                                </li>

                                                <li>Disposing of all refuse (cardboard, Styrofoam, plastic, pallets, etc.) is
                                                    the responsibility of the customer to the Collection or
                                                    Refuse Point.
                                                </li>
                                                <li>No supplies, boxes or unused equipment will be stored in or on top of server
                                                    cabinets.
                                                </li>
                                                <li>No combustible material should be left in the data center at all. This
                                                    includes cardboard, wood, plastic, tools, etc...
                                                </li>
                                                <li>Eating, Drinking, Smoking, packing & unpacking is strictly prohibited inside
                                                    the Data Center except the designated areas
                                                </li>
                                                <li>The DC Operations Team will review the Access and Requester will be
                                                    contacted if additional information is required.
                                                </li>
                                                <li>No Access will be granted without handing or giving a valid photo ID card at
                                                    the security.
                                                </li>
                                                <li>Allocated access cards should always be worn during the time inside the DC
                                                    Facility and returned at the security gate at the time
                                                    of leaving.
                                                </li>
                                                <li>Any Noisy or malodourous activities such as drilling, coring, painting etc.
                                                    are permitted only after working hours and the applicant
                                                    to coordinate with the building security and DC Operations team for the same
                                                    to avoid any stoppage of works.
                                                </li>
                                                <li>In case of loss of the given access card the customer should pay 100 AED for
                                                    replacement
                                                </li>

                                            </ul>


                                        </div>
                                    </div>

                                    <div class="form-section">
                                        <span>Accompanying Visitors Detail</span>
                                    </div>
                                    <div class="row" id="visitor_fields">

                                        <div class="col-sm-4 paddingtop8">
                                            <label>Visotor Name</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="XYZ Visitor" required="required" type="text">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-4 paddingtop8">
                                            <label>Company Name</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="Wisdom IT" required="required" type="text">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Contact Details</label>
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="052 1234567" required="required"
                                                           type="text">

                                                    <div class="input-group-btn">
                                                        <button class="btn btn-success" id="addScnt" type="button"><i
                                                                    class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>

                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>


                                        </div>

                                    </div>


                                    <div class="form-section">
                                        <span>Requester Acknowledgement</span>
                                    </div>


                                    <div class="row">
                                        <div class="bullets col-sm-12">

                                            <ul>
                                                <li>I fully understand and will abide by all policies and procedures as
                                                    described in the GDH Data Center Policies and Procedures
                                                    document.
                                                </li>
                                                <li>I fully understand and agree to these rules and that the facility is
                                                    monitored by CCTV. I also agree to provide my full
                                                    cooperation during any investigation concerning a security matter, which
                                                    might have occurred in the Data Center during a
                                                    time when my presence in the facility has been recorded.
                                                </li>
                                                <li>GDH reserves their right to terminate the access of the Data Center for its
                                                    personnel, clients, subcontractors and visitors
                                                    found violating these rules and in case of emergency
                                                </li>
                                            </ul>


                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input class="form-control" data-error="Please input your Last Name"
                                                       placeholder="12-03-2017" required="required" type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-section">
                                        <span> Manager / Pre Approved Managers Details</span>
                                    </div>

                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" data-error="Your email address is invalid"
                                               placeholder="ABC User" required="required" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Email address</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="abc@gmail.com" required="required" type="email">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Contact Detail</label>
                                                <input class="form-control" placeholder="009711234567" required="required"
                                                       type="text">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Designation</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="Manager" required="required" type="text" name="requester_designation">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input class="form-control" placeholder="Wisdom IT Solution" required="required"
                                                       type="text" name="requester_company">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Status(Signature)</label>
                                                <select name="" class="form-control">
                                                    <option>Pending</option>
                                                    <option>Approved</option>
                                                    <option>Cancel</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" value="04-12-2017">
                                            </div>
                                        </div>


                                    </div>


                                    <div class="form-section">
                                        <span> For Data Centre Operations Team</span>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Access Number</label>
                                            <input class="form-control" data-error="Your email address is invalid"
                                                   placeholder="AERT-711-34567" required="required" type="text">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row paddingtop8">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Esscort Service </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Optional<input type="checkbox"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="check-label"> Mandatory <input type="checkbox"></label>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Access Start Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" value="04-12-2017">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" value="01:00 pm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Access End Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" value="04-12-2017">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Time</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" value="03:00 pm">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Status(Signature)</label>
                                                <select name="" class="form-control">
                                                    <option>Pending</option>
                                                    <option>Approved</option>
                                                    <option>Cancel</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input class="single-daterange form-control" placeholder="Date of birth"
                                                       type="text" value="04-12-2017">
                                            </div>
                                        </div>


                                    </div>


                                    <div class="content-box-footer">
                                        <button class="btn btn-primary">Save</button>
                                        <a href="<?php echo site_url('auth/login') ?>" class="btn btn-warning">Back To
                                            Requests</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">


                $(function () {
                    //visitors form add more
                    var scntDiv = $('#visitor_fields');

                    var html_vistor = '';
                    html_vistor += '<div class="row addvisitor">';
                    html_vistor += '<div class="col-sm-4 paddingtop8">';
                    html_vistor += '<label>Visotor Name</label>';
                    html_vistor += '<input class="form-control" data-error="Your email address is invalid"placeholder="XYZ Visitor" required="required" type="text">';
                    html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_vistor += '</div>';
                    html_vistor += '<div class="col-sm-4 paddingtop8">';
                    html_vistor += '<label>Company Name</label>';
                    html_vistor += '<input class="form-control" data-error="Your email address is invalid" placeholder="Wisdom IT" required="required" type="text">';
                    html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_vistor += '</div>';
                    html_vistor += '<div class="col-sm-4">';
                    html_vistor += '<div class="form-group">';
                    html_vistor += '<label>Contact Details</label>';
                    html_vistor += '<div class="input-group">';
                    html_vistor += '<input class="form-control" placeholder="052 1234567" required="required" type="text">';
                    html_vistor += '<div class="input-group-btn">';
                    html_vistor += '<button class="btn btn-danger" type="button" id="remScnt"> <i class="fa fa-times"></i> </button>';
                    html_vistor += '</div>';
                    html_vistor += '</div>';
                    html_vistor += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_vistor += '</div>';
                    html_vistor += '</div>';
                    html_vistor += '</div>';


                    $('#addScnt').on('click', function () {
                        $(html_vistor).insertAfter(scntDiv);

                        $('#remScnt').on('click', function () {
                            $(this).parents('.addvisitor').remove();
                            return false;
                        });

                        return false;
                    });

                    //accesstype checkboxes

                    //material check boxes
                    $('.check_access_type').click(function() {
                        $('.check_access_type').not(this).prop('checked', false);
                    });
                    //material check boxes
                    $('.check_material').click(function() {
                        $('.check_material').not(this).prop('checked', false);
                    });
                    // Mariral addmore



                    var matDiv = $('#material_div');

                    var html_material = '';

                    html_material += '<div class="row materials materialhtml" id="" >';
                    html_material += '<div class="col-sm-3">';
                    html_material += '<label>Item Description </label>';
                    html_material += '<input class="form-control" data-error="Your email address is invalid"placeholder="An electronic product" required="required" type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '<div class="col-sm-3">';
                    html_material += '<div class="form-group">';
                    html_material += '<label> Serial Number </label>';
                    html_material += '<input class="form-control" placeholder="Wisdom IT Solution" required="required"type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '<div class="col-sm-3">';
                    html_material += '<div class="form-group">';
                    html_material += '<label> Brand Name & Model </label>';
                    html_material += '<input class="form-control" placeholder="Samsung Note8" required="required" type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '<div class="col-sm-3">';
                    html_material += '<div class="form-group">';
                    html_material += '<label> Power Capacity </label>';
                    html_material += '<input class="form-control" placeholder="Samsung Note8" required="required" type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '<div class="col-sm-3">';
                    html_material += '<div class="form-group">';
                    html_material += '<label> Heat Dissipation</label>';
                    html_material += '<input class="form-control" placeholder="" required="required"type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '<div class="col-sm-4">';
                    html_material += '<div class="form-group">';
                    html_material += '<label> Storage Requirements/ Allocated Location</label>';
                    html_material += '<input class="form-control" placeholder="" required="required" type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += ' </div>';
                    html_material += '</div>';
                    html_material += ' <div class="col-sm-3">';
                    html_material += ' <div class="form-group">';
                    html_material += '<label> Comliance check name/signature</label>';
                    html_material += '<input class="form-control" placeholder="" required="required" type="text">';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '<div class="col-sm-2">';
                    html_material += '<div class="form-group">';
                    html_material += '<label> Status/Remarks</label>';
                    html_material += '<div class="input-group">';
                    html_material += ' <input class="form-control" placeholder="" required="required" type="text">';
                    html_material += '<div class="input-group-btn">';
                    html_material += '<button class="btn btn-danger" type="button" id="remMatrial"> <i class="fa fa-times"></i> </button>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '<div class="help-block form-text with-errors form-control-feedback"></div>';
                    html_material += '</div>';
                    html_material += '</div>';
                    html_material += '</div>';




                    $(".check_material").change(function() {
                        if(this.checked) {

                            $('#material_div').removeClass('hidden');
                            $('#material_add_div_btn').show('500');

                        }else{
                            $('#material_div').addClass('hidden');
                            $('#material_add_div_btn').hide('500');

                        }
                    });


                    $('#addmatrial').on('click', function () {
                        $(html_material).insertAfter(matDiv);

                        $('#remMatrial').on('click', function () {
                            $(this).parents('.materials').remove();
                            return false;
                        });

                        return false;
                    });

                    $('#remMatrial').on('click', function () {
                        $(this).parents('.materials').remove();
                        return false;
                    });






                });


            </script>





    </div>



<div class="overlay"></div>


<?php $this->load->view('components/foot'); ?>
    </div>
</div>

