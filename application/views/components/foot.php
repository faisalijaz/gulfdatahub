<!-- jQuery CDN -->

<!-- Bootstrap Js CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script type="text/javascript">


    $(document).ready(function () {

        $('#datetimepicker6').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            lang: 'en',
            minDate: 'today',
        });

        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            lang: 'en',
            minDate: 'today',
        });

        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });

        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        $(".general_date").datetimepicker({
            format: 'DD/MM/YYYY',
            lang: 'en',
            minDate: 'today'
        });

        // $('.datepicker').datepicker();

        $('.timepicker').clockpicker({donetext: 'Select',twelvehour: true});
    });

    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
           // alert('sd1');
            $('.overlay').fadeOut();
            $('.mask').hide();
        });


        $('.side-nav-mobile').on('click',function(){
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.mask').hide();
            // alert('sd');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });


        var menu = $('#sidebar');
        var menuTimeout = null;

        $(window).on('mousemove', mouseMoveHandler);

        function mouseMoveHandler(e) {
            if (e.pageX < 30 || menu.is(':hover')) {
                // Show the menu if mouse is within 20 pixels
                // from the left or we are hovering over it
                clearTimeout(menuTimeout);
                menuTimeout = null;
                $('#sidebar').addClass('active');
                $('.overlay').fadeIn();
            } else if (menuTimeout === null) {
                // Hide the menu if the mouse is further than 20 pixels
                // from the left and it is not hovering over the menu
                // and we aren't already scheduled to hide it
                $('#sidebar').removeClass('active');
                // alert('sd1');
                $('.overlay').fadeOut();
                $('.mask').hide();
            }
        }



        $('#sidebarCollapse,#sidebarCollapse1').hover(function(){
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
           // alert('sd');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

    </body>
</html>