<?php

if (!defined('APPPATH'))
    exit('No direct script access allowed');

class Common {

    function Common() {
        $ci = $this->CI = & get_instance();
//        $this->CI->output->enable_profiler(TRUE);
    }

    public function encode($value) {

        if (!$value) {
            return false;
        }
        $text = $value;

        return trim(base64_encode($text));
    }

    public function decode($value) {

        if (!$value) {
            return false;
        }

        return trim(base64_decode($value));
    }

    public function sendEmail($to,$subject,$message) {
        $ci = $this->CI = & get_instance();
        $ci->load->library('email');
        $ci->email->from(ADMIN_EMAIL, 'Gulf Data Hub');
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($message);
        $ci->email->send();
    }
    public function sendEmailBulk($toArray,$subject,$message) {
        $ci = $this->CI = & get_instance();
        $ci->load->library('email');
        $ci->email->from(ADMIN_EMAIL, 'Gulf Data Hub');

        if(!empty($toArray)){
            foreach($toArray as $key => $to ) {

                $ci->email->to($to['email']);
                $ci->email->subject($subject);
                $ci->email->message($message);
                $ci->email->send();
            }
        }
    }




}

?>
