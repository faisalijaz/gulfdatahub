<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Base_Controller {

	/**
     * Update Profile Form
     *
     * @access 	public
     * @param 	
     * @return 	view
     */
	
	public function index()
	{	
		$this->load->model('group_m');



		//print_r($this->session->userdata['active_user']->group_id);


		$this->data['title'] = 'Profile';
		$this->data['subview'] = 'profile/main';
		$this->data['groups'] = $this->group_m->all();

		$this->load->view('components/main', $this->data);
	}

	/**
     * Validate Input
     *
     * @access 	public
     * @param 	
     * @return 	json(array)
     */

	public function validate()
	{
		$rules = [
			[
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'required'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required'
			],
			[
				'field' => 'group_id',
				'label' => 'Group Id',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run()) {
			header("Content-type:application/json");
			echo json_encode('success');
		} else {
			header("Content-type:application/json");
			echo json_encode($this->form_validation->get_all_errors());
		}
	}

	/**
     * Save Profile Changes
     *
     * @access 	public
     * @param 	
     * @return 	json('string')
     */





	public function save()
    {

        $post = $this->input->post();

        if (count($post) > 0) {

            /*echo "<pre>";
            print_r($post);
            exit('sdsd');*/
            $this->load->library('upload');

            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path" => "./uploads/"
            ));

            if ($_FILES['photo']['name'] != '') {
                if ($this->upload->do_upload("photo")) {
                    $uploaded = $this->upload->data();
                    $post['photo'] = $uploaded['file_name'];
                }
            }



            if ($_FILES['image_id']['name'] != '') {
                if ($this->upload->do_upload("image_id")) {
                    $uploaded1 = $this->upload->data();
                    $post['image_id'] = $uploaded1['file_name'];
                }

            }
            $id = $post['id'];
            unset($post['id']);
            $post['id_expiry_date'] = date('Y-m-d h:i:s',strtotime( $post['id_expiry_date']));
            update('users', $post, 'id='.$id);
            $this->load->model('user_m');

            $user = $this->user_m->get_user($id);

            $this->session->set_userdata('active_user', $user);

            $this->session->set_flashdata('success_message', 'Your Profile data has been updated successfully');
redirect(base_url('profile'));


        }
    }

}
