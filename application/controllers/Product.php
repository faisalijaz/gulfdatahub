<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller
{

    /**
     * List of Products
     *
     * @access    public
     * @param
     * @return    view
     */

    public function index()
    {
        $this->data['title'] = 'Product';
        $this->data['subview'] = 'product/main';
        $this->load->view('components/main', $this->data);
    }

    /**
     * Product Form
     *
     * @access    public
     * @param
     * @return    view
     */
    public function preapprovers()
    {

        $user_id = $this->session->userdata['active_user']->id;
        if ($this->session->userdata['active_user']->group_id == 3) {
            $data['requests'] = getValArray('*', ' preapprove_requests', array('created_by' => $user_id));
        } else if ($this->session->userdata['active_user']->group_id == 5) {
            $data['requests'] = getValArray('*', ' preapprove_requests', array('manager_signature' => 'Approved'));
        }else{
            $data['requests'] = getValArray('*', ' preapprove_requests');
        }
        $this->data['title'] = 'Pre-Approver Requests';
        $this->data['subview'] = 'product/pre-approvever-request';
        $this->data['requests'] = $data['requests'];
        $this->load->view('components/theme', $this->data);

    }

    public function pdftest()
    {

        $request_id = $this->input->get('id');
        $request_id = $this->common->decode($request_id);
        $personnels = getValArray("*", 'personnels', 'request_id', $request_id);
        $request = getRowArray('*', 'preapprove_requests', $where = 'id=' . $request_id);

        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('My Title');

        $image_file = "http://127.0.0.1/assets/images/logo1.jpg";

        $pdf->SetHeaderData($image_file, PDF_HEADER_LOGO_WIDTH, 'sads', 'sdfs');
        $pdf->setHtmlHeader("GDH-PAL-FRM-001");

        $pdf->SetHeaderMargin(30);
        // set image scale factor
        $pdf->SetTopMargin(50);
        $pdf->setFooterMargin(20);
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // $pdf->AddPage();
        $pdf->AddPage('L', 'A4');


        $subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';

        $html = '
<table border=".5" cellspacing="1" cellpadding="4">
    <tr>
        <th colspan="6" align="center" bgcolor="#6495ed" ><h3>Pre-Approved List for GDH Data Center</h3></th>
    </tr>
     <tr>
        <th colspan="6" align="center" bgcolor="#a9a9a9" ><h3>Main Contact Person (Approved/ Requestor)</h3></th>
    </tr>
    <tr>
       <th align="center" bgcolor="#dcdcdc">Name</th>
       <th  align="center" bgcolor="#dcdcdc">Title</th>
       <th  align="center" bgcolor="#dcdcdc">Mobile Number</th>
       <th  align="center" bgcolor="#dcdcdc">Email Address</th>
       <th  align="center" bgcolor="#dcdcdc">ID Expiry</th>
       <th  align="center" bgcolor="#dcdcdc">Signaturedsd </th>
    </tr>
    <tr>
        <td align="center">' . $request["pre_manager_name"] . '</td>
        <td align="center">' . $request["pre_manager_title"] . '</td>
        <td align="center">' . $request["pre_manager_phone"] . '</td>
        <td align="center">' . $request["pre_manager_email"] . '</td>
        <td align="center">' . $request["pre_manager_card_expiry"] . '</td>
        <td align="center">' . $request["pre_manager_signature"] . '</td>
    </tr>
    <tr>
        <th colspan="6" align="center" bgcolor="#a9a9a9" ><h3>Backup Contact Person (Approved/ Requestor)</h3></th>
    </tr>
    <tr>
       <th align="center" bgcolor="#dcdcdc">Name</th>
       <th  align="center" bgcolor="#dcdcdc">Title</th>
       <th  align="center" bgcolor="#dcdcdc">Mobile Number</th>
       <th  align="center" bgcolor="#dcdcdc">Email Address</th>
       <th  align="center" bgcolor="#dcdcdc">ID Details</th>
       <th  align="center" bgcolor="#dcdcdc">Signature</th>
    </tr>
    <tr>
        <td align="center">' . $request["contact_person_name"] . '</td>
        <td align="center">' . $request["contact_person_title"] . '</td>
        <td align="center">' . $request["contact_person_number"] . '</td>
        <td align="center">' . $request["contact_person_email"] . '</td>
        <td align="center">' . $request["contact_person_card_expiry"] . '</td>
        <td align="center">' . $request["contact_person_signature"] . '</td>
    </tr>
    
    <tr>
        <th colspan="6" align="center" bgcolor="#a9a9a9" ><h3>Pre Approved Personnel</h3></th>
    </tr>
    <tr>
       <th align="center" bgcolor="#dcdcdc">Name</th>
       <th  align="center" bgcolor="#dcdcdc">Title</th>
       <th  align="center" bgcolor="#dcdcdc">Mobile Number</th>
       <th  align="center" bgcolor="#dcdcdc">Email Address</th>
       <th  align="center" bgcolor="#dcdcdc">ID Details</th>
       <th  align="center" bgcolor="#dcdcdc">Signature</th>
    </tr>';

        if (!empty($personnels)) {

            foreach ($personnels as $row) {

                $html .= '<tr><td align="center">' . $row["personnel_name"] . '</td>
        <td align="center">' . $row["personnel_title"] . '</td>
        <td align="center">' . $row["personnel_phone"] . '</td>
        <td align="center">' . $row["personnel_email"] . '</td>
        <td align="center">' . $row["personnel_id_expiry"] . '</td>
        <td align="center">' . $row["personnel_signature"] . '</td></tr>';
            }
        }

        $html .= '<tr>
        <th colspan="6" align="center" bgcolor="#a9a9a9" ><h3>Authorized Approver</h3></th>
    </tr>
    <tr>
       <th align="center" bgcolor="#dcdcdc" colspan="2">Name</th>
       <th  align="center" bgcolor="#dcdcdc" colspan="2">Signature</th>
       <th  align="center" bgcolor="#dcdcdc" colspan="2">Date</th>
       
    </tr>
    <tr>
       <td align="center"  colspan="2">' . $request["manager_name"] . '</td>
        <td align="center"  colspan="2">' . $request["team_signature"] . '</td>
        <td align="center" colspan="2">' . $request["team_signature_date"] . '</td>
       
    </tr>

</table>';

// print a block of text using Write()
        // $pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        $pdf->writeHTML($html, true, false, true, false, '');

//Close and output PDF document
        $pdf->Output('example_023064.pdf', 'I');
    }

    public
    function securityformPreApprovers()
    {
        $post = $this->input->post();

        if (count($post) > 0) {

                if ($post['is_active'] != 1) {
                    $data['check_in_time'] = date('Y-m-d h:i:s');
                    $data['signature_checkin'] = $post['sigpattern'];
                   // $data['security_member_id'] = $this->session->userdata['active_user']->id;
                    $data['is_active'] = 1;
                    update('preapprove_requests', $data, "id=" . $post['request_id']);

                    $this->session->set_flashdata('success_message', 'You are check in successfully');
                    redirect(base_url('product/preapprovers/securityformPreApprovers?id=' . $this->common->encode($post['request_id'])));

                } else {
                    $data['check_out_time'] = date('Y-m-d h:i:s');
                    $data['signature_checkout'] = $post['sigpattern'];
                   // $data['security_member_id'] = $this->session->userdata['active_user']->id;
                    $data['is_active'] = 2;
                    update('preapprove_requests', $data, "id=" . $post['request_id']);
                    $this->session->set_flashdata('success_message', 'You are check out successfully');
                    redirect(base_url('product/securityformPreApprovers?id=' . $this->common->encode($post['request_id'])));

                }



        }

        $request_id = $this->input->get('id');
        $request_id = $this->common->decode($request_id);
        $personnels = getValArray("*", 'personnels', 'request_id', $request_id);
        $request = getRowArray('*', 'preapprove_requests', $where = 'id=' . $request_id);
        $this->data['request'] = $request;
        $this->data['personnels'] = $personnels;
        $this->data['title'] = 'Security Form';
        $this->data['subview'] = 'product/security-form-preApprover';
        $this->load->view('components/theme', $this->data);
    }

    public
    function securityform()
    {
        $post = $this->input->post();

        if (count($post) > 0) {

            if ($post['is_active'] != 1) {
                $data['check_in_time'] = date('Y-m-d h:i:s');
                $data['signature_checkin'] = $post['sigpattern'];
                // $data['security_member_id'] = $this->session->userdata['active_user']->id;
                $data['is_active'] = 1;
                update('requests', $data, "id=" . $post['request_id']);

                $this->session->set_flashdata('success_message', 'You are check in successfully');
                redirect(base_url('product/securityform?id=' . $this->common->encode($post['request_id'])));

            } else {
                $data['check_out_time'] = date('Y-m-d h:i:s');
                $data['signature_checkout'] = $post['sigpattern'];
                // $data['security_member_id'] = $this->session->userdata['active_user']->id;
                $data['is_active'] = 2;
                update('requests', $data, "id=" . $post['request_id']);
                $this->session->set_flashdata('success_message', 'You are check out successfully');
                redirect(base_url('product/securityform?id=' . $this->common->encode($post['request_id'])));

            }



        }

      /*  $request_id = $this->input->get('id');
        $request_id = $this->common->decode($request_id);*/
        $request_id = 35;
        $personnels = getValArray("*", 'visiters', 'request_id', $request_id);
        $request = getRowArray('*', 'requests', $where = 'id=' . $request_id);
        $this->data['request'] = $request;
        $this->data['personnels'] = $personnels;
        $this->data['title'] = 'Security Form';
        $this->data['subview'] = 'product/security-form-all';
        $this->load->view('components/theme', $this->data);
    }

    public function editPreRequest()
    {
        $post = $this->input->post();

        if (count($post) > 0) {

            $count_for_new = 0;
            $request_id = $post['id'];
            unset($post['id']);
            $this->load->library('upload');
            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path" => "./uploads/"
            ));
            $user = $this->session->userdata();
            $post['start_date'] = date('Y-m-d h:i', strtotime($post['start_date']));
            $post['end_date'] = date('Y-m-d h:i', strtotime($post['end_date']));
            $post['card_expiry'] = date('Y-m-d', strtotime($post['card_expiry']));

            if ($this->upload->do_upload("profile_image")) {
                $uploaded = $this->upload->data();
                $post['profile_image'] = $uploaded['file_name'];
                update('users', array('photo' => $uploaded['file_name']), 'id=' . $this->session->userdata['active_user']->id);
            }

            if ($this->upload->do_upload("manager_id_image")) {
                $uploaded1 = $this->upload->data();
                $post['manager_id_image'] = $uploaded1['file_name'];
                update('users', array('image_id' => $uploaded1['file_name']), 'id=' . $this->session->userdata['active_user']->id);

            }
            if ($this->upload->do_upload("pre_manager_id_image")) {
                $uploaded = $this->upload->data();
                $post['pre_manager_id_image'] = $uploaded['file_name'];
            }

            if ($this->upload->do_upload("contact_person_image")) {
                $uploaded1 = $this->upload->data();
                $post['contact_person_image'] = $uploaded1['file_name'];
            }


            $visiter_info = array();
            $visiter_info['personnel_name'] = $post['personnel_name'];
            $visiter_info['personnel_title'] = $post['personnel_title'];
            $visiter_info['personnel_phone'] = $post['personnel_phone'];
            $visiter_info['personnel_email'] = $post['personnel_email'];
            $visiter_info['personnel_id_expiry'] = $post['personnel_id_expiry'];
            $visiter_info['personnel_signature'] = $post['personnel_signature'];
            if (isset($post['personnel_image'])) {
                $visiter_info['personnel_image'] = $post['personnel_image'];
                unset($post['personnel_image']);
            }


            unset($post['personnel_name']);
            unset($post['personnel_title']);
            unset($post['personnel_phone']);
            unset($post['personnel_email']);
            unset($post['personnel_id_expiry']);
            unset($post['personnel_signature']);

            $single_image = 0;
            if ($post['access_type'] != 'preapprover') {


            } else {

                $uploadedALL = array();
                if (!empty($_FILES['personnel_image'])) {
                    if ($this->upload->do_upload("personnel_image")) {
                        $uploadedALL = $this->upload->data();
                    }
                }
                if (count($_FILES['personnel_image']['name']) == 1) {
                    $single_image = 1;
                }


                $count_for_new = count($visiter_info['personnel_image']);
                // $count_for_new = $count_for_new -1;
                $this->db->delete('personnels', array('request_id' => $request_id));
                for ($i = 0; $i < count($visiter_info['personnel_name']); $i++) {
                    if ($visiter_info['personnel_name'][$i] != '') {
                        if (isset($visiter_info['personnel_image'][$i])) {
                            $visiters_values = array(
                                'personnel_name' => $visiter_info['personnel_name'][$i],
                                'personnel_title' => $visiter_info['personnel_title'][$i],
                                'personnel_phone' => $visiter_info['personnel_phone'][$i],
                                'personnel_email' => $visiter_info['personnel_email'][$i],
                                'personnel_image' => $visiter_info['personnel_image'][$i],
                                'personnel_id_expiry' => $visiter_info['personnel_id_expiry'][$i],
                                'personnel_signature' => $visiter_info['personnel_signature'][$i],
                                'request_id' => $request_id
                            );
                        } else {
                            if ($single_image == 1) {
                                $visiters_values = array(
                                    'personnel_name' => $visiter_info['personnel_name'][$i],
                                    'personnel_title' => $visiter_info['personnel_title'][$i],
                                    'personnel_phone' => $visiter_info['personnel_phone'][$i],
                                    'personnel_email' => $visiter_info['personnel_email'][$i],
                                    'personnel_image' => $uploadedALL['file_name'],
                                    'personnel_id_expiry' => $visiter_info['personnel_id_expiry'][$i],
                                    'personnel_signature' => $visiter_info['personnel_signature'][$i],
                                    'request_id' => $request_id
                                );
                            } else {
                                $visiters_values = array(
                                    'personnel_name' => $visiter_info['personnel_name'][$i],
                                    'personnel_title' => $visiter_info['personnel_title'][$i],
                                    'personnel_phone' => $visiter_info['personnel_phone'][$i],
                                    'personnel_email' => $visiter_info['personnel_email'][$i],
                                    'personnel_image' => $uploadedALL[$i - $count_for_new]['file_name'],
                                    'personnel_id_expiry' => $visiter_info['personnel_id_expiry'][$i],
                                    'personnel_signature' => $visiter_info['personnel_signature'][$i],
                                    'request_id' => $request_id
                                );
                            }

                        }

                        $this->db->insert('personnels', $visiters_values);

                    }
                }
                update('preapprove_requests', $post, 'id=' . $request_id);
/*
                if ($post['manager_signature'] == 'Approved') {

                    $toarray = get_datateam();
                    $subject = 'Access Request  : Pre-approver';
                    $message = 'Dear Data Operation Team Member you have request to Approve/Reject.Please wait for response.';
                    $this->common->sendEmailBulk($toarray, $subject, $message);

                } else if ($post['manager_signature'] == 'Reject') {
                    $to = $post['email'];
                    $subject = 'Access Request Reject : Pre-approver';
                    if (isset($post['manager_reject_reason'])) {

                        $message = 'Dear requester your request has been rejected due to some reason: ' . $post['manager_reject_reason'];
                    } else {
                        $message = 'Dear requester your request has been rejected due to some reason.';
                    }
                    $this->common->sendEmail($to, $subject, $message);
                }*/

               /* if ($post['team_signature'] == 'Approved') {

                    $to = $post['email'];
                    $subject = 'Access Request Approved : Pre-approver';
                        $message = 'Dear requester your request has been Approved .';

                    $this->common->sendEmail($to, $subject, $message);

                } else if ($post['team_signature'] == 'Reject') {
                    $to = $post['email'];
                    $subject = 'Access Request Reject : Pre-approver';
                    if (isset($post['manager_reject_reason'])) {

                        $message = 'Dear requester your request has been rejected due to some reason: ' . $post['manager_reject_reason'];
                    } else {
                        $message = 'Dear requester your request has been rejected due to some reason.';
                    }
                    $this->common->sendEmail($to, $subject, $message);
                }*/

                $this->session->set_flashdata('success_message', 'Your request has been updated successfully');
                redirect(base_url('product/preapprovers'));

            }

        } else {
            $request_id = $this->input->get('id');
            $request_id = $this->common->decode($request_id);
            $personnels = getValArray("*", 'personnels', 'request_id', $request_id);
            $request = getRowArray('*', 'preapprove_requests', $where = 'id=' . $request_id);
            $this->data['title'] = 'Pre-Approver Edit Requests';
            $this->data['subview'] = 'product/preapprover-form-edit';
            $this->data['request'] = $request;
            $this->data['personnels'] = $personnels;
            $this->load->view('components/theme', $this->data);
        }
    }


    public function preApproverRequest()
    {


        $alldata = $this->session->userdata();

        $post = $this->input->post();

        if (count($post) > 0) {
            $this->load->library('upload');
            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path" => "./uploads/"
            ));
            $user = $this->session->userdata();
            $post['created_by'] = $this->session->userdata['active_user']->id;
            $post['start_date'] = date('Y-m-d h:i', strtotime($post['start_date']));
            $post['end_date'] = date('Y-m-d h:i', strtotime($post['end_date']));

            if ($this->upload->do_upload("profile_image")) {
                $uploaded = $this->upload->data();
                $post['profile_image'] = $uploaded['file_name'];
                update('users', array('photo' => $uploaded['file_name']), 'id=' . $this->session->userdata['active_user']->id);
            }

            if ($this->upload->do_upload("manager_id_image")) {
                $uploaded1 = $this->upload->data();
                $post['manager_id_image'] = $uploaded1['file_name'];
                update('users', array('image_id' => $uploaded1['file_name']), 'id=' . $this->session->userdata['active_user']->id);

            }
            if ($this->upload->do_upload("pre_manager_id_image")) {
                $uploaded = $this->upload->data();
                $post['pre_manager_id_image'] = $uploaded['file_name'];
            }

            if ($this->upload->do_upload("contact_person_image")) {
                $uploaded1 = $this->upload->data();
                $post['contact_person_image'] = $uploaded1['file_name'];
            }


            $visiter_info = array();
            $visiter_info['personnel_name'] = $post['personnel_name'];
            $visiter_info['personnel_title'] = $post['personnel_title'];
            $visiter_info['personnel_phone'] = $post['personnel_phone'];
            $visiter_info['personnel_email'] = $post['personnel_email'];
            $visiter_info['personnel_id_expiry'] = $post['personnel_id_expiry'];
            $visiter_info['personnel_signature'] = $post['personnel_signature'];


            unset($post['personnel_name']);
            unset($post['personnel_title']);
            unset($post['personnel_phone']);
            unset($post['personnel_email']);
            unset($post['personnel_id_expiry']);
            unset($post['personnel_signature']);


            if ($post['access_type'] != 'preapprover') {


            } else {

                $this->db->insert('preapprove_requests', $post);
                $inserted_id = $this->db->insert_id();

                if ($inserted_id) {

                    //  if(!empty($_FILES['personnel_image'])) {
                    if ($this->upload->do_upload("personnel_image")) {
                        $uploadedALL = $this->upload->data();
                    }
                    //}

                    // $this->db->delete('', array('request_id' => $request_id));
                    for ($i = 0; $i < count($visiter_info['personnel_name']); $i++) {
                        if ($visiter_info['personnel_name'][$i] != '') {

                            $visiters_values = array(
                                'personnel_name' => $visiter_info['personnel_name'][$i],
                                'personnel_title' => $visiter_info['personnel_title'][$i],
                                'personnel_phone' => $visiter_info['personnel_phone'][$i],
                                'personnel_email' => $visiter_info['personnel_email'][$i],
                                'personnel_image' => $uploadedALL[$i]['file_name'],
                                'personnel_id_expiry' => $visiter_info['personnel_id_expiry'][$i],
                                'personnel_signature' => $visiter_info['personnel_signature'][$i],
                                'request_id' => $inserted_id
                            );
                            $this->db->insert('personnels', $visiters_values);

                        }
                    }
                }
                if ($inserted_id) {

                    /* $to =$post['email'];
                     $subject ='Access Request : Pre-approver';
                     $message = 'Dear requester your request has been submitted successfully.Please wait for response. We will update you soon.';
                     $this->common->sendEmail($to,$subject,$message);
                     $toarray =  get_managers();
                     $message = 'Dear Manager you request to Approve/Reject.Please wait for response.';
                     $this->common->sendEmailBulk($toarray,$subject,$message);*/


                    $this->session->set_flashdata('success_message', 'Your request has been submitted successfully');
                    redirect(base_url('product/preapprovers'));

                } else {

                    $this->session->set_flashdata('error_message', 'Your request not processed. Something going wrong');
                    redirect(base_url('product/preapprovers'));

                }
            }
        } else {
            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'product/preapprover-form';
            $this->load->view('components/theme', $this->data);
        }
    }

    public function form()
    {
        $data['index'] = $this->input->post('index');
        $this->load->view('product/form', $data);
    }

    /**
     * Datagrid Data
     *
     * @access    public
     * @param
     * @return    json(array)
     */

    public function data()
    {
        header('Content-Type: application/json');
        $this->load->model('product_m');
        echo json_encode($this->product_m->getJson($this->input->post()));
    }

    /**
     * Validate Input
     *
     * @access    public
     * @param
     * @return    json(array)
     */

    public function validate()
    {
        $rules = [
            [
                'field' => 'product_name',
                'label' => 'Product Name',
                'rules' => 'required'
            ],
            [
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'required'
            ],
            [
                'field' => 'stock',
                'label' => 'Stock',
                'rules' => 'required'
            ],
            [
                'field' => 'images',
                'label' => 'Images',
                'rules' => 'required'
            ],
            [
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'required'
            ]
        ];

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
            header("Content-type:application/json");
            echo json_encode('success');
        } else {
            header("Content-type:application/json");
            echo json_encode($this->form_validation->get_all_errors());
        }
    }

    /**
     * Create Update Action
     *
     * @access    public
     * @param
     * @return    method
     */

    public function action()
    {
        if (!$this->input->post('id')) {
            $this->create();
        } else {
            $this->update();
        }
    }

    /**
     * Create a New Product
     *
     * @access    public
     * @param
     * @return    json(string)
     */

    public function create()
    {
        $data['product_name'] = $this->input->post('product_name');
        $data['price'] = $this->input->post('price');
        $data['stock'] = $this->input->post('stock');
        $data['images'] = $this->input->post('images');
        $data['description'] = $this->input->post('description');
        $this->db->insert('products', $data);

        header('Content-Type: application/json');
        echo json_encode('success');
    }

    /**
     * Update Existing Product
     *
     * @access    public
     * @param
     * @return    json(string)
     */

    public function update()
    {
        $data['product_name'] = $this->input->post('product_name');
        $data['price'] = $this->input->post('price');
        $data['stock'] = $this->input->post('stock');
        $data['images'] = $this->input->post('images');
        $data['description'] = $this->input->post('description');
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('products', $data);
        header('Content-Type: application/json');
        echo json_encode('success');
    }

    /**
     * Delete a Product
     *
     * @access    public
     * @param
     * @return    redirect
     */

    public function delete()
    {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('products');
    }

}
