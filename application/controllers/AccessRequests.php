<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccessRequests extends Base_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    /**
     * List of Requests
     *
     * @access    public
     * @param
     * @return    view
     */

    public function index()
    {
        $user_id = $this->session->userdata['active_user']->id;
        $data['requests'] = getValArray('*', 'requests', array('created_by' => $user_id));
        $this->data['title'] = 'Access Requests';
        $this->data['subview'] = 'requests/main';
        $this->data['requests'] = $data['requests'];
        $this->load->view('components/theme', $this->data);
    }

    public function preapprovers()
    {
        $this->data['title'] = 'Pre-Approver Requests';
        $this->data['subview'] = 'requests/pre-approvever-request';
        $this->load->view('components/theme', $this->data);
    }

    /**
     * requests Form
     *
     * @access    public
     * @param
     * @return    view
     */

    public function form()
    {
        $data['index'] = $this->input->post('index');
        $this->load->view('requests/form', $data);
    }

    /**
     * Datagrid Data
     *
     * @access    public
     * @param
     * @return    json(array)
     */

    public function data()
    {
        header('Content-Type: application/json');
        $this->load->model('requests_m');
        echo json_encode($this->requests_m->getJson($this->input->post()));
    }

    /**
     * Validate Input
     *
     * @access    public
     * @param
     * @return    json(array)
     */

    public function validate()
    {
        $rules = [
            [
                'field' => 'requests_name',
                'label' => 'requests Name',
                'rules' => 'required'
            ],
            [
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'required'
            ],
            [
                'field' => 'stock',
                'label' => 'Stock',
                'rules' => 'required'
            ],
            [
                'field' => 'images',
                'label' => 'Images',
                'rules' => 'required'
            ],
            [
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'required'
            ]
        ];

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
            header("Content-type:application/json");
            echo json_encode('success');
        } else {
            header("Content-type:application/json");
            echo json_encode($this->form_validation->get_all_errors());
        }
    }

    /**
     * Create Update Action
     *
     * @access    public
     * @param
     * @return    method
     */

    public function action()
    {
        if (!$this->input->post('id')) {
            $this->create();
        } else {
            $this->update($this->input->post('id'));
        }
    }

    /**
     * Create a New requests
     *
     * @access    public
     * @param
     * @return    json(string)
     */

    public function create()
    {
        $post = $this->input->post();


        if (count($post) > 0) {

            $this->load->library('upload');
            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path" => "./uploads/"
            ));
            $user = $this->session->userdata();
            $post['created_by'] = $this->session->userdata['active_user']->id;

            if ($this->upload->do_upload("profile_image")) {
                $uploaded = $this->upload->data();
                $post['profile_image'] = $uploaded['file_name'];
            }

            if ($this->upload->do_upload("manager_id_image")) {
                $uploaded1 = $this->upload->data();
                $post['manager_id_image'] = $uploaded1['file_name'];
            }



            $visiter_info = array();
            $visiter_info['visiter_name'] = $post['visiter_name'];
            $visiter_info['visiter_company'] = $post['visiter_company'];
            $visiter_info['visiter_phone'] = $post['visiter_phone'];


            unset($post['visiter_name']);
            unset($post['visiter_company']);
            unset($post['visiter_phone']);


            $workPermitType = false;
            if (isset($post['work_permit_type'])) {
                $workPermitType = true;
                unset($post['work_permit_type']);
            }


            if(isset($post['card_expiry'])){
                $post['card_expiry'] = changeDateFormat($post['card_expiry'] );
            }

            if(isset($post['requester_signature_date'])){
                $post['requester_signature_date'] = changeDateFormat($post['requester_signature_date'] );
            }


            if (isset($post['access_type']) && $post['access_type'] == 'preapprover') {
            } else {

                $this->db->insert('requests', $post);
                $inserted_id = $this->db->insert_id();

                if ($inserted_id) {

                    for ($i = 0; $i < count($visiter_info['visiter_name']); $i++) {
                        if ($visiter_info['visiter_name'][$i] != '') {

                            $visiters_values = array(
                                'name' => $visiter_info['visiter_name'][$i],
                                'company' => $visiter_info['visiter_company'][$i],
                                'phone' => $visiter_info['visiter_phone'][$i],
                                'request_id' => $inserted_id
                            );
                            $this->db->insert('visiters', $visiters_values);

                        }
                    }

                    /**
                     *
                     */
                    if ($workPermitType) {
                        if ($this->session->has_userdata('work_permit_data')) {
                            if ($this->saveWorkPermits($this->session->userdata('work_permit_data'), $inserted_id)) {
                                $this->session->set_userdata('work_permit_data', null);
                            }
                        }
                    }

                    /**
                     *
                     */
                    if (isset($post['material_in'])) {
                        if ($this->session->has_userdata('material_in_data')) {
                            if ($this->saveMaterialInOutForm($this->session->userdata('material_in_data'), $inserted_id)) {
                                $this->session->set_userdata('material_in_data', null);
                            }
                        }
                    }

                    /**
                     *
                     */
                    if (isset($post['material_out'])) {
                        if ($this->session->has_userdata('material_out_data')) {
                            if ($this->saveMaterialInOutForm($this->session->userdata('material_out_data'), $inserted_id)) {
                                $this->session->set_userdata('material_out_data', null);
                            }
                        }
                    }

                }
                if ($inserted_id) {


                    $this->session->set_flashdata('success_message', 'Your request has been submitted successfully');
                    redirect(base_url('accessRequests'));

                } else {
                    $this->session->set_flashdata('error_message', 'Your request not processed. Something going wrong');
                    redirect(base_url('accessRequests'));
                }
            }
        } else {

            $this->session->set_userdata('material_in_data', null);
            $this->session->set_userdata('work_permit_data', null);
            $this->session->set_userdata('material_out_data', null);


            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/form';
            $this->load->view('components/theme', $this->data);


        }
    }

    /**
     * Update Existing requests
     *
     * @access    public
     * @param
     * @return    json(string)
     */

    public function update($id)
    {
        $inserted_id = $id;

        if ($this->input->post('name')) {

            $post = $this->input->post();

            if (count($post) > 0) {

                $this->load->library('upload');

                //Configure upload.
                $this->upload->initialize(array(
                    "allowed_types" => "gif|jpg|png|jpeg",
                    "upload_path" => "./uploads/"
                ));

                $user = $this->session->userdata();
                $post['created_by'] = $this->session->userdata['active_user']->id;

                if (isset($post['old_profile_image']) && $post['old_profile_image'] <> "") {

                    if ($this->upload->do_upload("profile_image")) {
                        $uploaded = $this->upload->data();
                        $post['profile_image'] = $uploaded['file_name'];
                    }
                    unset($post['old_profile_image']);
                } else {

                    if ($this->upload->do_upload("profile_image")) {
                        $uploaded = $this->upload->data();
                        $post['profile_image'] = $uploaded['file_name'];
                    }
                }

                if (isset($post['old_manager_id_image']) && $post['old_manager_id_image'] <> "") {

                    if ($this->upload->do_upload("manager_id_image")) {
                        $uploaded1 = $this->upload->data();
                        $post['manager_id_image'] = $uploaded1['file_name'];
                    }

                    unset($post['old_manager_id_image']);
                } else {
                    if ($this->upload->do_upload("manager_id_image")) {
                        $uploaded1 = $this->upload->data();
                        $post['manager_id_image'] = $uploaded1['file_name'];
                    }
                }

                if(isset($post['start_date'])){
                    if(strpos($post['start_date'],'-') !== false){
                        $dates = explode('-',$post['start_date']);
                        $post['start_date'] = date('Y-m-d H:i A', strtotime($dates[0]));
                        $post['end_date'] = date('Y-m-d H:i A', strtotime($dates[1]));
                    }
                }

                if(isset($post['card_expiry'])){
                    $post['card_expiry'] = changeDateFormat($post['card_expiry'] );
                }

                if(isset($post['requester_signature_date'])){
                    $post['requester_signature_date'] = changeDateFormat($post['requester_signature_date'] );
                }

                $visiter_info = array();
                $visiter_info['visiter_name'] = $post['visiter_name'];
                $visiter_info['visiter_company'] = $post['visiter_company'];
                $visiter_info['visiter_phone'] = $post['visiter_phone'];

                unset($post['visiter_name']);
                unset($post['visiter_company']);
                unset($post['visiter_phone']);
                unset($post['vistor_id']);

                if (isset($post['id'])) {
                    $id= $post['id'];
                    unset($post['id']);
                }


                $workPermitType = false;
                if (isset($post['work_permit_type'])) {
                    $workPermitType = true;
                    unset($post['work_permit_type']);
                }

                if (isset($post['access_type']) && $post['access_type'] == 'preapprover') {
                } else {

                    $this->db->where(['id' => $id]);
                    $updated = $this->db->update('requests', $post);

                    if ($updated) {

                        $this->db->where('request_id', $id);
                        $this->db->delete('visiters');

                        for ($i = 0; $i < count($visiter_info['visiter_name']); $i++) {
                            if ($visiter_info['visiter_name'][$i] != '') {

                                $visiters_values = array(
                                    'name' => $visiter_info['visiter_name'][$i],
                                    'company' => $visiter_info['visiter_company'][$i],
                                    'phone' => $visiter_info['visiter_phone'][$i],
                                    'request_id' => $inserted_id
                                );
                                $this->db->insert('visiters', $visiters_values);

                            }
                        }

                        /**
                         *
                         */

                        if ($workPermitType) {
                            if ($this->session->has_userdata('work_permit_data')) {
                                if ($this->saveWorkPermits($this->session->userdata('work_permit_data'), $id)) {
                                    $this->session->set_userdata('work_permit_data', null);
                                }
                            }
                        }

                        /**
                         *
                         */

                        if (isset($post['material_in'])) {
                            if ($this->session->has_userdata('material_in_data')) {
                                if ($this->saveMaterialInOutForm($this->session->userdata('material_in_data'), $id)) {
                                    $this->session->set_userdata('material_in_data', null);
                                }
                            }
                        }
                        /**
                         *
                         */
                        if (isset($post['material_out'])) {
                            if ($this->session->has_userdata('material_out_data')) {
                                if ($this->saveMaterialInOutForm($this->session->userdata('material_out_data'), $id)) {
                                    $this->session->set_userdata('material_out_data', null);
                                }
                            }
                        }
                       // printr($this->session->userdata());
                    }



                    if ($post['manager_signature'] == 'Approved') {

                        $toarray = get_datateam();
                        $subject = 'Access Request';
                        $message = 'Dear Data Operation Team Member you have request to Approve/Reject.Please wait for response.';
                        $this->common->sendEmailBulk($toarray, $subject, $message);

                    } else if ($post['manager_signature'] == 'Reject') {
                        $to = $post['email'];
                        $subject = 'Access Request Reject';
                        if (isset($post['manager_reject_reason'])) {

                            $message = 'Dear requester your request has been rejected due to some reason: ' . $post['manager_reject_reason'];
                        } else {
                            $message = 'Dear requester your request has been rejected due to some reason.';
                        }
                        $this->common->sendEmail($to, $subject, $message);
                    }

                    if ($post['team_signature'] == 'Approved') {
                        $to = $post['email'];
                        $subject = 'Access Request Approved';
                        $message = 'Dear requester your request has been Approved .';
                        $this->common->sendEmail($to, $subject, $message);
                    } else if ($post['team_signature'] == 'Reject') {
                        $to = $post['email'];
                        $subject = 'Access Request Reject';
                        if (isset($post['manager_reject_reason'])) {
                            $message = 'Dear requester your request has been rejected due to some reason: ' . $post['manager_reject_reason'];
                        } else {
                            $message = 'Dear requester your request has been rejected due to some reason.';
                        }
                        $this->common->sendEmail($to, $subject, $message);
                    }



                    if ($inserted_id) {
                        $this->session->set_flashdata('success_message', 'Your request has been submitted successfully');
                        redirect(base_url('accessRequests'));
                    } else {
                        $this->session->set_flashdata('error_message', 'Your request not processed. Something going wrong');
                        redirect(base_url('accessRequests'));
                    }
                }
            }
        } else {

            $this->data['request_data'] = getDataByColumn('requests', 'id', $id);
            /*echo "<pre>";
                    print_r($this->data['request_data']);
                    exit('fsdf');*/

            $this->data['material_in_data'] = getRequestMaterialData($id, 'in');
            $this->data['material_out_data'] = getRequestMaterialData($id, 'out');
            $this->data['work_permit'] = getRequestWorkPermit($id);


            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/form';


            $this->load->view('components/theme', $this->data);

        }

        /*
        $data['requests_name'] = $this->input->post('requests_name');
        $data['price'] = $this->input->post('price');
        $data['stock'] = $this->input->post('stock');
        $data['images'] = $this->input->post('images');
        $data['description'] = $this->input->post('description');
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('requests', $data);

        header('Content-Type: application/json');
        echo json_encode('success');
        */
    }

    public function preApproverRequest()
    {
        $post = $this->input->post();
        if (count($post) > 0) {
            $data['requests_name'] = $this->input->post('requests_name');
            $data['price'] = $this->input->post('price');
            $data['stock'] = $this->input->post('stock');
            $data['images'] = $this->input->post('images');
            $data['description'] = $this->input->post('description');
            $this->db->insert('requestss', $data);

            header('Content-Type: application/json');
            echo json_encode('success');
        } else {
            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/preapprover-form';
            $this->load->view('components/theme', $this->data);


        }
    }


    public function viewManager()
    {
        $post = $this->input->post();
        if (count($post) > 0) {
            $data['requests_name'] = $this->input->post('requests_name');
            $data['price'] = $this->input->post('price');
            $data['stock'] = $this->input->post('stock');
            $data['images'] = $this->input->post('images');
            $data['description'] = $this->input->post('description');
            $this->db->insert('requestss', $data);

            header('Content-Type: application/json');
            echo json_encode('success');
        } else {
            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/manager-form';
            $this->load->view('components/theme', $this->data);


        }
    }

    public function viewSecurity()
    {
        $post = $this->input->post();
        if (count($post) > 0) {
            $data['requests_name'] = $this->input->post('requests_name');
            $data['price'] = $this->input->post('price');
            $data['stock'] = $this->input->post('stock');
            $data['images'] = $this->input->post('images');
            $data['description'] = $this->input->post('description');
            $this->db->insert('requestss', $data);

            header('Content-Type: application/json');
            echo json_encode('success');
        } else {
            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/security-view';
            $this->load->view('components/theme', $this->data);


        }
    }

    public function viewTeam()
    {
        $post = $this->input->post();
        if (count($post) > 0) {
            $data['requests_name'] = $this->input->post('requests_name');
            $data['price'] = $this->input->post('price');
            $data['stock'] = $this->input->post('stock');
            $data['images'] = $this->input->post('images');
            $data['description'] = $this->input->post('description');
            $this->db->insert('requestss', $data);

            header('Content-Type: application/json');
            echo json_encode('success');
        } else {
            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/team-form';
            $this->load->view('components/theme', $this->data);


        }
    }

    public function viewWorkTeam()
    {
        $post = $this->input->post();
        if (count($post) > 0) {
            $data['requests_name'] = $this->input->post('requests_name');
            $data['price'] = $this->input->post('price');
            $data['stock'] = $this->input->post('stock');
            $data['images'] = $this->input->post('images');
            $data['description'] = $this->input->post('description');
            $this->db->insert('requestss', $data);

            header('Content-Type: application/json');
            echo json_encode('success');
        } else {
            $this->data['title'] = 'Access Requests';
            $this->data['subview'] = 'requests/team-work-form';
            $this->load->view('components/theme', $this->data);


        }
    }

    public function viewWorkPermits()
    {
        $this->data['title'] = 'Work Permits Requests';
        $this->data['subview'] = 'requests/workpermits';
        $this->load->view('components/theme', $this->data);
    }

    public function editFormTeam()
    {
        $this->data['title'] = 'Edit Form';
        $this->data['subview'] = 'requests/edit-form-team';
        $this->load->view('components/theme', $this->data);
    }

    public function editFormManager()
    {
        $this->data['title'] = 'Edit Form';
        $this->data['subview'] = 'requests/edit-form-manager';
        $this->load->view('components/theme', $this->data);
    }

    public function securityform()
    {
        $this->data['title'] = 'Edit Form';
        $this->data['subview'] = 'requests/security-form';
        $this->load->view('components/theme', $this->data);
    }

    /**
     * Delete a Product
     *
     * @access    public
     * @param
     * @return    redirect
     */

    public function delete()
    {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('requests');
    }

    /**
     *
     */
    public function submitRequestInOutForm()
    {

        if (count($this->input->post()) > 0) {

            $post = $this->input->post();

            if ($post['type'] == 'in') {
                $this->session->set_userdata('material_in_data', $this->input->post());
            }

            if ($post['type'] == 'out') {
                $this->session->set_userdata('material_out_data', $this->input->post());
            }

            if ($post['type'] == 'work_permit') {

                if (isset($post['type'])) {
                    unset($post['type']);
                }

                $this->session->set_userdata('work_permit_data', $post);
            }
            // echo 1;
            // printr($post);
            printr($this->session->userdata());
        }
    }

    /**
     * @param $data
     */
    public function saveMaterialInOutForm($data, $request_id)
    {
        $formData = $data;
        $id = "";
        $loggedIn = getLoggedInUser();

        $materialItems = $formData['Itmes'];

        unset($formData['Itmes']);
        unset($formData['saveForm']);

        if (isset($formData['id'])) {
            $id = $formData['id'];
            unset($formData['id']);
        }

        $formData['request_id'] = $request_id;
        $formData['created_by'] = $loggedIn->id;
        $formData['m_date'] = changeDateFormat($formData['m_date']);

        if ($id != "") {
            $this->db->where('id', $id);
            if ($this->db->update('material_info', $formData)) {
                $inserted_id = $id;
            }

        } else {
            $this->db->insert('material_info', $formData);
            $inserted_id = $this->db->insert_id();
        }

        if ($inserted_id) {

            if($id){
                $this->db->where('id', $id);
                $this->db->delete('material_items');
            }

            if (count($materialItems) > 0) {
                for ($i = 0; $i < count($materialItems['material_item']); $i++) {
                    if ($materialItems['material_item'][$i] != '') {
                        $visiters_values = array(
                            'material_item' => $materialItems['material_item'][$i],
                            'material_quntity' => $materialItems['material_quntity'][$i],
                            'material_serial' => $materialItems['material_serial'][$i],
                            'material_model' => $materialItems['material_model'][$i],
                            'material_power_capacity' => $materialItems['material_power_capacity'][$i],
                            'material_power_heat' => $materialItems['material_power_heat'][$i],
                            'material_power_storage' => $materialItems['material_power_storage'][$i],
                            'material_power_check_name' => $materialItems['material_power_check_name'][$i],
                            'material_status' => $materialItems['material_status'][$i],
                            'material_id' => $inserted_id
                        );
                        $this->db->insert('material_items', $visiters_values);
                    }
                }
            }
        }
    }

    /**
     * @param $workPermit
     * @return bool
     */
    public function saveWorkPermits($workPermit, $request_id)
    {
        if ($workPermit <> null && count($workPermit) > 0) {

            $workers = [];
            $id = "";

            $loggedIn = getLoggedInUser();

            if (isset($workPermit['worker_name'])) {
                $workers = $workPermit['worker_name'];
            }

            unset($workPermit['worker_name']);

            if (isset($workPermit['Array'])) {
                unset($workPermit['Array']);
            }

            if (isset($workPermit['id'])) {
                $id = $workPermit['id'];
                unset($workPermit['id']);
            }

            if (isset($workPermit['work_permit_type'])) {
                unset($workPermit['work_permit_type']);
            }

            if (isset($workPermit['Itmes'])) {
                unset($workPermit['Itmes']);
            }

            if (isset($workPermit['type'])) {
                unset($workPermit['type']);
            }

            if(isset($workPermit['other_special_reqs'])){
                unset($workPermit['other_special_reqs']);
            }

            if(isset($workPermit['special_requirement_other_checkbox'])){
                unset($workPermit['special_requirement_other_checkbox']);
            }


            $workPermit['start_date'] = changeDateFormat($workPermit['start_date']);
            $workPermit['end_date'] = changeDateFormat($workPermit['end_date']);
            $workPermit['signature_date'] = changeDateFormat($workPermit['signature_date']);
            $workPermit['request_id'] = $request_id;
            $workPermit['created_by'] = $loggedIn->id;

            if ($id != "") {
                $this->db->where('id', $id);
                if ($this->db->update('work_permits', $workPermit)) {
                    $inserted_id = $id;
                }
            } else {
                $this->db->insert('work_permits', $workPermit);
                $inserted_id = $this->db->insert_id();
            }

            if ($inserted_id && count($workers) > 0 && $workers <> null) {

                if ($id) {
                    $this->db->where('work_permit_id', $inserted_id);
                    $this->db->delete('work_permit_workers');
                }

                foreach ($workers as $worker) {
                    if ($worker && $worker <> "") {
                        $workerData = [
                            'worker_name' => $worker,
                            'work_permit_id' => $inserted_id,
                        ];

                        $this->db->insert('work_permit_workers', $workerData);
                    }
                }
            }

            return true;
        }
    }

    /**
     *
     */
    public function validateForms()
    {
        $validateForms = ['work_permit' => 0, 'material_in' => 0, 'material_out' => 0];

        if ($this->session->has_userdata('work_permit_data')) {
            $validateForms['work_permit'] = 1;
        }

        if ($this->session->has_userdata('material_in_data')) {
            $validateForms['material_in'] = 1;
        }

        if ($this->session->has_userdata('material_out_data')) {
            $validateForms['material_out'] = 1;
        }
        echo json_encode($validateForms);
    }
}
