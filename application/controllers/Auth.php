<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    /**
     * Login Form
     *
     * @access    public
     * @param
     * @return    view
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->helper(array('form', 'url'));

    }

    public function login()
    {
        $data['title'] = 'Login';
        $data['subview'] = 'login/main';
        $this->load->view('components/mainlayout', $data);
    }

    public function emailCheck()
    {
        return false;
    }


    public function signup()
    {
        $post = $this->input->post();
        if (count($post) > 0) {

            $users = $this->user_m->checkEmail($post['email']);

            if ($users > 0) {
                $data['heading'] = 'Login';
                $this->session->set_flashdata('error_message', 'This email address is already exist.Please try with other email');

                $data['title'] = 'Signup';
                $data['subview'] = 'login/form';
                $this->load->view('components/layout', $data);
            }else {


                $this->load->library('upload');
                //Configure upload.
                $this->upload->initialize(array(
                    "allowed_types" => "gif|jpg|png|jpeg",
                    "upload_path" => "./uploads/"
                ));

                if ($this->upload->do_upload("photo")) {
                    $uploaded = $this->upload->data();
                    $data['photo'] = $uploaded['file_name'];
                }

                if ($this->upload->do_upload("image_id")) {
                    $uploaded1 = $this->upload->data();
                    $data['image_id'] = $uploaded1['file_name'];
                }
             


                $name = $this->input->post('first_name') . ' ' . $this->input->post('last_name');

                $data['name'] = $name;
                $data['email'] = $this->input->post('email');
                $data['password'] = $this->input->post('password');
                $data['group_id'] = 3;
                $data['phone'] = $this->input->post('phone');
                $data['gender'] = $this->input->post('gender');
                $data['company_name'] = $this->input->post('company_name');
                $data['designation'] = $this->input->post('designation');
                $data['id_expiry_date'] = date('Y-m-d',strtotime($this->input->post('id_expiry_date')));
                $data['active_status'] = 1;
                $this->db->insert('users', $data);
                $insert_id = $this->db->insert_id();

                if ($insert_id) {

                    $inserted_user = $this->user_m->get_user_array($insert_id);
                    $attempt = $this->user_m->attempt($inserted_user);
                    if ($attempt === null) {

                        $this->session->set_flashdata('error_message', 'You are not registered successfully. Please try again');
                        redirect(base_url(''));
                    } else {
                        $this->session->set_userdata('active_user', $attempt);
                        $this->session->set_flashdata('success_message', 'You are registered successfully');
                        redirect(base_url(''));
                    }

                }
            }

        }else {

            $data['title'] = 'Signup';
            $data['subview'] = 'login/form';
            $this->load->view('components/mainlayout', $data);
        }
    }

    public function update()
    {
        $post = $this->input->post();
        if (count($post) > 0) {
            $this->load->library('upload');
            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path" => "./uploads/"
            ));

            if ($_FILES['photo']['name'] != '') {
                if ($this->upload->do_upload("photo")) {
                    $uploaded = $this->upload->data();
                    $post['photo'] = $uploaded['file_name'];
                }
            }


            if ($_FILES['image_id']['name'] != '') {
                if ($this->upload->do_upload("image_id")) {
                    $uploaded1 = $this->upload->data();
                    $post['image_id'] = $uploaded1['file_name'];
                }

            }

            $this->load->model('user_m');
            $user = $this->user_m->get_user($this->input->post('id'));
            $this->session->set_userdata('active_user', $user);
            $this->session->set_flashdata('success_message', 'Your Profile data has been updated successfully');
            redirect(base_url('profile'));


        }
    }


    public function create()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $data['group_id'] = $this->input->post('group_id');
        $this->db->insert('users', $data);

        header('Content-Type: application/json');
        echo json_encode('success');
    }


    /**
     * Validate and Login User
     *
     * @access    public
     * @param
     * @return    json(array)
     */

    public function login_attempt()
    {


        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ]
        ];

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
            $this->load->model('user_m');
            $attempt = $this->user_m->attempt($this->input->post());
            if ($attempt === null) {
                header("Content-type:application/json");
                echo json_encode(['password' => 'Wrong email or password']);
            } else {
                $this->session->set_userdata('active_user', $attempt);
                header("Content-type:application/json");
                echo json_encode(['status' => 'success']);
            }
        } else {
            header("Content-type:application/json");
            echo json_encode($this->form_validation->get_all_errors());
        }
    }

    /**
     * Logout User
     *
     * @access    public
     * @param
     * @return    redirect
     */

    public function logout()
    {
        $this->session->unset_userdata('active_user');
        redirect('auth/login');
    }
}
